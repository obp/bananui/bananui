Bananui is a minimal graphical user interface library for Linux feature phones.
It uses cairo as the rendering backend and displays windows using Wayland.

Bananui supports keypad navigation out of the box and has its own layout
engine, which still needs some optimization but already works well enough.

## Building

Use the Meson build system to build bananui:

```
meson setup _build
ninja -C _build
ninja -C _build install
```

## Testing

You can use [phoc](https://gitlab.gnome.org/World/Phosh/phoc) as a wayland
compositor for testing apps.

```
phoc -E _build/examples/bananui-uitest
phoc -E _build/examples/bananui-menutest
```

## Examples

 - [uitest](./examples/uitest.c): General demo app
 - [menu](./examples/menu.c): Typical menu-based settings-like app (uses the bMenu API and GApplication)

## API warning
This library is only "version 2" because a previous abandoned version 1
exists at <https://git.abscue.de/bananui/wbananui>. If the current API turns
out to be inconvenient enough to require incompatible changes, a development
version of this library will be created with a new (unstable) API. Version 2
will remain in that case, but may get deprecated before the new API becomes
stable.

## Documentation
The (WIP) documentation is available at <https://obp.abscue.de/bananui/bananui>.

## Contributing
Contributions are welcome. For a list of contributors, see <https://git.abscue.de/obp/bananui/bananui/-/graphs/main>.
