/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_SOFTKEY_H_
#define _BANANUI_SOFTKEY_H_
#include <bananui/widget.h>
#include <bananui/card.h>

/**
 * SECTION:softkey
 * @title: bSoftkeyPanel
 * @short_description: Card for displaying software key labels
 *
 * The #bSoftkeyPanel displays three labels at the bottom of the screen,
 * designed to match the general-purpose buttons of a phone.
 */

/**
 * bSoftkeyPanel:
 * @card: the card (for use with #bRenderCard).
 *
 * This structure contains the card used for displaying softkey labels.
 */
typedef struct bsSoftkeyPanel {
	bCard *card;
	/*< private >*/
	bContent *lcont, *ccont, *rcont;
} bSoftkeyPanel;

/**
 * bCreateSoftkeyPanel:
 * 
 * Returns: a newly created #bSoftkeyPanel, or %NULL if there was an error
 */
bSoftkeyPanel *bCreateSoftkeyPanel(void);

/**
 * bDestroySoftkeyPanel:
 * @sk: a #bSoftkeyPanel.
 *
 * Free all resources associated with a #bSoftkeyPanel.
 */
void bDestroySoftkeyPanel(bSoftkeyPanel *sk);

/**
 * bSetSoftkeyText:
 * @sk: a #bSoftkeyPanel.
 * @l: label for the left software key, or %NULL to leave it unchanged.
 * @c: label for the center/OK key, or %NULL to leave it unchanged.
 * @r: label for the right software key, or %NULL to leave it unchanged.
 *
 * Change the labels displayed on the panel.
 */
void bSetSoftkeyText(bSoftkeyPanel *sk,
	const char *l, const char *c, const char *r);

#endif
