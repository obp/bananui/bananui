/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_CARD_H_
#define _BANANUI_CARD_H_

#include <time.h>
#include <bananui/widget.h>
#include <bananui/window.h>

/**
 * SECTION:card
 * @title: bCard
 * @short_description: Group of widgets with transformations and navigation
 *
 * A #bCard is a wrapper around a box which simplifies rendering and
 * navigation. It lets the user scroll and move between widgets and also
 * provides an API for animated transformations.
 */

typedef struct bsCardAnmationXY {
	/*< private >*/
	double from[2], to[2], duration;
	struct timespec time;
} bCardAnimationXY;

typedef struct bsCardAnmation {
	/*< private >*/
	double from, to, duration;
	struct timespec time;
} bCardAnimation;

/**
 * bCard:
 * @box: the card's root box. Add widgets here.
 * @focus: currently focused box, or %NULL if there is none.
 * @style: custom style for the card's root box, with dimensions automatically set to match the window size.
 * @keydown: triggered when a key is pressed on a focused card. Parameter type: #bKeyEvent.
 * @keyup: triggered when a key is released on a focused card. Parameter type: #bKeyEvent.
 * @prerender: triggered after applying transformations but before rendering widgets. Parameter type: #bRenderParams.
 * @postrender: triggered after rendering widgets with transformations still applied. Parameter type: #bRenderParams.
 *
 * Contains the widgets of the card, event handlers and other data.
 */
typedef struct bsCard {
	bBox *box;
	bBox *focus;
	bStyleSpec style;
	bEventHandler *keydown;
	bEventHandler *keyup;
	bEventHandler *prerender;
	bEventHandler *postrender;
	/*< private >*/
	bBox *saved_focus;
	int has_focus, scrolled_to_focus;
	bCardAnimationXY scale, translate, scroll;
	bCardAnimation rotate, opacity;
	double maxsx, maxsy;
} bCard;

/**
 * bRenderParams:
 * @wnd: the window on which the card is being rendered.
 * @card: the card currently being rendered.
 * @renderer: the window's cairo instance (shortcut for wnd->renderer).
 *
 * Parameters passed to the `prerender` and `postrender` events of a #bCard.
 */
typedef struct bsRenderParams {
	bWindow *wnd;
	bCard *card;
	cairo_t *renderer;
} bRenderParams;

/**
 * bFocusEvent:
 * @wnd: the window on which the box was focused.
 * @card: the card containing the box.
 * @box: the box that was focused.
 *
 * Parameters passed to the `focusin` and `focusout` events of a #bBox
 * when the user navigates between widgets in a focused #bCard.
 */
typedef struct bsFocusEvent {
	bWindow *wnd;
	bCard *card;
	bBox *box;
} bFocusEvent;

/**
 * bCreateCard:
 * @style_name: style to use for the card's root box.
 *
 * Cards that contain normal pages with content usually use the "top" style.
 * The "top" style defines reasonable background and foreground colors and
 * leaves space at the bottom for a #bSoftkeyPanel.
 *
 * Returns: a newly allocated #bCard with the specified style, or %NULL on error
 */
bCard *bCreateCard(const char *style_name);

/**
 * bBuildCard:
 * @script: a #bScript.
 * @style_name: style to use for the card's root box.
 * @n_refs: length of @refs.
 * @refs: pointer to array where sub-boxes will be stored.
 *
 * Like #bBuildBox but builds a card instead.
 *
 * Returns: a newly allocated #bCard filled by the script on success, %NULL on error
 */
bCard *bBuildCard(bScript *script,
		  const char *style_name,
		  uint16_t n_refs,
		  bBox **refs);

/**
 * bScaleCard:
 * @card: a #bCard.
 * @x: horizontal scaling factor.
 * @y: vertical scaling factor.
 * @time: animation time in seconds, 0 to apply setting instantly.
 *
 * Change the scaling factor of the card.
 */
void bScaleCard(bCard *card, double x, double y, double time);

/**
 * bRotateCard:
 * @card: a #bCard.
 * @angle: rotation angle in degrees.
 * @time: animation time in seconds, 0 to apply setting instantly.
 *
 * Change the rotation of the card.
 */
void bRotateCard(bCard *card, double angle, double time);

/**
 * bTranslateCard:
 * @card: a #bCard.
 * @x: horizontal offset from left.
 * @y: vertical offset from top.
 * @time: animation time in seconds, 0 to apply setting instantly.
 *
 * Change the position of the card on the screen. Unlike #bScrollCard,
 * this does not affect the internal scrolling mechanism.
 */
void bTranslateCard(bCard *card, double x, double y, double time);

/**
 * bScrollCard:
 * @card: a #bCard.
 * @x: horizontal offset from right.
 * @y: vertical offset from bottom.
 * @time: animation time in seconds, 0 to apply setting instantly.
 *
 * Change the scroll position of the card. The transformation caused by
 * scrolling is applied separately from the translation set using
 * #bTranslateCard.
 */
void bScrollCard(bCard *card, double x, double y, double time);

/**
 * bFadeCard:
 * @card: a #bCard.
 * @opacity: alpha multiplier.
 * @time: animation time in seconds, 0 to apply setting instantly.
 *
 * Change the opacity/transparency of a card.
 */
void bFadeCard(bCard *card, double opacity, double time);

/**
 * bAnimateCardWith:
 * @card: the #bCard to be animated.
 * @master: another #bCard that is already animated.
 *
 * Copy transformations and ongoing animations from @master to @card
 * without causing any delays.
 */
void bAnimateCardWith(bCard *card, bCard *master);

/**
 * bAnimationComplete:
 * @card: a #bCard.
 *
 * Returns: 0 if an animation is in progress, else 1
 */
int bAnimationComplete(bCard *card);

/**
 * bFocusBox:
 * @wnd: a #bWindow.
 * @card: a #bCard.
 * @box: a #bBox which belongs to @card.
 *
 * Move @card's focus to @box. If @card is focused, the @box receives
 * a focusin event.
 */
void bFocusBox(bWindow *wnd, bCard *card, bBox *box);

/**
 * bFocusCard:
 * @wnd: a #bWindow.
 * @card: #bCard to be focused.
 *
 * Focus @card on @wnd and register keyboard handlers.
 * The card may not be focused on another window.
 * A focusin event is triggered on @card's currently focused box.
 */
void bFocusCard(bWindow *wnd, bCard *card);

/**
 * bUnfocusCard:
 * @wnd: #bWindow that the card was focused on.
 * @card: a focused #bCard.
 *
 * Unfocus @card and remove its keyboard handlers from @wnd.
 * A focusout event is triggered on @card's currently focused box.
 */
void bUnfocusCard(bWindow *wnd, bCard *card);

/**
 * bRenderCard:
 * @wnd: a #bWindow.
 * @card: #bCard to be rendered.
 *
 * Render @card's widgets on @wnd. Should be called from @wnd's `redraw` handler.
 * The @card does not have to be focused on @wnd.
 */
void bRenderCard(bWindow *wnd, bCard *card);

/**
 * bDestroyCard:
 * @card: a #bCard.
 *
 * Destroy all resources associated with @card.
 * If the card is focused and #bDestroyWindow is not immediately called
 * after this, the caller should unfocus the card first.
 */
void bDestroyCard(bCard *card);

#endif
