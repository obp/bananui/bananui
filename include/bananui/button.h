/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_BUTTON_H_
#define _BANANUI_BUTTON_H_

#include <bananui/widget.h>
#include <bananui/utils.h>

/**
 * SECTION:button
 * @title: Button
 * @short_description: Focusable and clickable widget with text and icons
 *
 * The #bButtonWidget can be used to show simple pushbuttons, menu items,
 * checkboxes, radio buttons and other clickable button-like items. The
 * look of the button is determined by the style.
 */

/**
 * bButtonFunc:
 * @B_BUTTON_FUNC_PUSHBUTTON: normal button, state ignored
 * @B_BUTTON_FUNC_CHECKBOX: button state controls checkbox icon
 * @B_BUTTON_FUNC_RADIO: button state controls radio button icon
 *
 * Specifies the function of the button's state.
 */
typedef enum beButtonFunc {
	B_BUTTON_FUNC_PUSHBUTTON,
	B_BUTTON_FUNC_CHECKBOX,
	B_BUTTON_FUNC_RADIO
} bButtonFunc;


/**
 * bButtonWidget:
 * @box: the box widget containing the button. When destroyed, this structure is also destroyed.
 *
 * Contains the button's data and sub-widgets.
 */
typedef struct bsButtonWidget {
	bBox *box;

	/*< private >*/
	bContent *icon;
	bContent *text;
	bContent *subtitle;

	bButtonFunc func;
	int state, focused;

	char *style_name;
	char *focused_style_name;

	bBox *subbox, *subbox2, *iconbox;
} bButtonWidget;

/**
 * bCreateButtonWidget:
 * @text: text to be shown on the button.
 * @style_name: style to be used for the button.
 * @func: type of the button, describes how its state affects the icon.
 *
 * Typical styles are:
 *  - "pushbutton" for %B_BUTTON_FUNC_PUSHBUTTON as a standalone button
 *  - "menubutton" for %B_BUTTON_FUNC_PUSHBUTTON in a list of options
 *  - "checkbox" for %B_BUTTON_FUNC_CHECKBOX
 *  - "radiobutton" for %B_BUTTON_FUNC_RADIO
 *
 * Returns: a newly allocated #bButtonWidget or %NULL on error
 */
bButtonWidget *bCreateButtonWidget(const char *text,
				   const char *style_name,
				   bButtonFunc func);

/**
 * bSetButtonState:
 * @btn: a #bButtonWidget.
 * @state: 1 -> "checked", 0 -> "unchecked"
 *
 * Change the state of the button. This has no effect for
 * %B_BUTTON_FUNC_PUSHBUTTON buttons.
 */
void bSetButtonState(bButtonWidget *btn, int state);

/**
 * bSetButtonText:
 * @btn: a #bButtonWidget.
 * @text: text to be shown on the button.
 *
 * Change the text displayed on the button.
 */
void bSetButtonText(bButtonWidget *btn, const char *text);

/**
 * bSetButtonSubtitle:
 * @btn: a #bButtonWidget.
 * @text: additional text to be shown on the button.
 *
 * Change the subtitle displayed on the button. This usually describes
 * what the button does or the indicates the state of the setting controlled
 * by the button.
 *
 * Not all button styles display the subtitle.
 */
void bSetButtonSubtitle(bButtonWidget *btn, const char *subtitle);

/**
 * bSetButtonIcon:
 * @btn: a #bButtonWidget.
 * @icon: system icon to be shown on the button.
 * @size: size of the icon in pixels.
 *
 * Change the icon displayed on the button.
 *
 * Not all button styles display an icon.
 */
void bSetButtonIcon(bButtonWidget *btn, const char *icon, int size);

#endif
