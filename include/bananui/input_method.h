/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_INPUT_METHOD_H_
#define _BANANUI_INPUT_METHOD_H_

#include <bananui/utils.h>

/**
 * SECTION:input_method
 * @title: bInputMethod
 * @short_description: Direct access to the system input method
 *
 * #bInputMethod is a wrapper around the system input method.
 * The current default implementation uses Wayland's zwp_text_input_v3
 * protocol. This API is only useful for applications that intend to
 * use a different input method protocol or implement their own text
 * input widgets.
 */


struct wl_display;
struct zwp_text_input_v3;

typedef enum beInputFlags {
	B_INPUT_FLAG_NONE = 0,
	B_INPUT_FLAG_MULTILINE = 1,
	B_INPUT_FLAG_MULTILINE_WRAP = 2
} bInputFlags;

typedef enum beInputMode {
	B_INPUT_MODE_NORMAL,
	B_INPUT_MODE_ALPHA,
	B_INPUT_MODE_DIGITS,
	B_INPUT_MODE_NUMBER,
	B_INPUT_MODE_PHONE,
	B_INPUT_MODE_URL,
	B_INPUT_MODE_EMAIL,
	B_INPUT_MODE_NAME,
	B_INPUT_MODE_PASSWORD,
	B_INPUT_MODE_PIN
} bInputMode;

typedef struct bsInputMethod {
	const struct bsInputMethodOps *ops;
	void *private;

	bEventHandler *update, *leave, *enter, *destroy_guard;
	unsigned int enabled : 1;
} bInputMethod;

typedef struct bsInputMethodOps {
	void (*report)(bInputMethod *im, bInputMode mode, bInputFlags flags,
			const char *text, int cursor, int anchor);
	void (*enable)(bInputMethod *im);
	void (*disable)(bInputMethod *im);
	void (*destroy)(bInputMethod *im);
} bInputMethodOps;

typedef struct bsInputMethodEvent {
	bInputMethod *im;
	char *preedit, *commit_string;
	unsigned int delete_before, delete_after;
} bInputMethodEvent;

bInputMethod *bCreateWaylandInputMethod(struct wl_display *disp,
		struct zwp_text_input_v3 *text_input);
void bReportInputMethodState(bInputMethod *im, bInputMode mode, bInputFlags flags,
		const char *text, int cursor, int anchor);
void bEnableInputMethod(bInputMethod *im);
void bDisableInputMethod(bInputMethod *im);
void bDestroyInputMethod(bInputMethod *im);

#endif
