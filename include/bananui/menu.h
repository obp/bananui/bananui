/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_MENU_H_
#define _BANANUI_MENU_H_

#include <bananui/button.h>
#include <bananui/card.h>
#include <bananui/softkey.h>

/**
 * SECTION:menu
 * @title: bMenu
 * @short_description: API for creating menu-based UIs
 *
 * This is a high-level API for creating a menu hierarchy with a stack
 * of views. A menu is defined by using the #B_MENU_ITEMS macro followed
 * by the #B_DEFINE_MENU macro.
 *
 * A #bMenu object can be created with #bCreateMenuView and represents
 * the entire menu-based UI of an application.
 *
 * For an example that uses this API, see
 * <https://git.abscue.de/obp/bananui/bananui/-/blob/main/examples/menu.c>.
 */


typedef enum beMenuTransition {
	B_MENU_TRANSITION_NONE = 0,
	B_MENU_TRANSITION_FADE = 1,
	B_MENU_TRANSITION_SLIDE = 2,
	B_MENU_TRANSITION_FADE_SLIDE = 3,
} bMenuTransition;

typedef struct bsMenuView {
	bCard *card;
	bCard *header;
	bContent *header_text;
	bSoftkeyPanel *sk;
	struct bsMenuView *prev;
} bMenuView;

typedef struct bsMenu {
	bWindow *wnd;
	bMenuView *current, *prev, *disappearing;
	void *userdata;
} bMenu;

bMenu *bCreateMenuView(bWindow *wnd);
int bPopMenu(bMenu *m, bMenuTransition transition);
void bSetMenuTitle(bMenu *m, const char *title);
void bRenderMenu(bMenu *m);

typedef void (*bMenuInitFunc)(bMenu *m);
typedef int (*bMenuClickFunc)(bMenu *m, void *arg);
typedef int (*bMenuButtonFunc)(bMenu *m, bButtonWidget *btn, void *arg);

typedef struct bsMenuEntry {
	const char *name, *icon, *style;
	bButtonFunc func;
	bMenuClickFunc click_handler;
	bMenuButtonFunc button_handler;
	void *arg;
	short height, iconsize;
} bMenuEntry;

typedef struct bsMenuDef {
	bMenuEntry *entries;
	size_t num_entries;
	bMenuInitFunc init;
	const char *title;
} bMenuDef;

int _bOpenMenuPrivate(bMenu *m, bMenuDef *def, bMenuTransition transition);

static inline int bOpenMenuHandlerWrapper(bMenu *m, void *arg) {
	_bOpenMenuPrivate(m, arg, B_MENU_TRANSITION_FADE_SLIDE);
	/*
	 * This is an event handler. bOpenMenuPrivate manipulates events,
	 * so event processing has to be stopped here.
	 */
	return 0;
}

#define B_MENU_ITEMS(_shortname) \
	static bMenuEntry _shortname ## __bMenu_entries__[]

#define B_SUBMENU(_shortname, _name) \
		{ \
			.name = _name, \
			.icon = "go-next-symbolic", \
			.style = "menubutton", \
			.func = B_BUTTON_FUNC_PUSHBUTTON, \
			.click_handler = bOpenMenuHandlerWrapper, \
			.arg = &_shortname ## __bMenu__, \
		}

#define B_DEFINE_MENU(_shortname, _title) \
	static void _shortname ## __bMenu_init__(bMenu *m); \
	static bMenuDef _shortname ## __bMenu__ = { \
		.entries = _shortname ## __bMenu_entries__, \
		.num_entries = sizeof(_shortname ## __bMenu_entries__) / \
			sizeof(bMenuEntry), \
		.init = _shortname ## __bMenu_init__, \
		.title = _title, \
	}; \
	static void _shortname ## __bMenu_init__(bMenu *m) \

#define B_OPEN_MENU(_m, _shortname, _transition) \
	_bOpenMenuPrivate((_m), &_shortname ## __bMenu__, (_transition))

#endif
