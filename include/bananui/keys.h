/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_KEYS_H_
#define _BANANUI_KEYS_H_

#include <xkbcommon/xkbcommon.h>

/**
 * SECTION:keys
 * @title: Keys
 * @short_description: Keysym definitions for feature phones
 *
 * Bananui defines some useful keysym aliases for keys found on feature
 * phones. Some of them are named more appropriately than their XKB_KEY_
 * counterparts.
 */

#ifndef XKB_KEY_XF86PickupPhone
#define XKB_KEY_XF86PickupPhone 0x100811bd
#endif

#ifndef XKB_KEY_XF86HangupPhone
#define XKB_KEY_XF86HangupPhone 0x100811be
#endif

#ifndef XKB_KEY_XF86KbdLcdMenu1
#define XKB_KEY_XF86KbdLcdMenu1 0x100812b8
#endif

#ifndef XKB_KEY_XF86KbdLcdMenu2
#define XKB_KEY_XF86KbdLcdMenu2 0x100812b9
#endif

#define BANANUI_KEY_SoftLeft XKB_KEY_XF86KbdLcdMenu1
#define BANANUI_KEY_SoftRight XKB_KEY_XF86KbdLcdMenu2
#define BANANUI_KEY_Star XKB_KEY_XF86NumericStar
#define BANANUI_KEY_Pound XKB_KEY_XF86NumericPound
#define BANANUI_KEY_Back XKB_KEY_BackSpace
#define BANANUI_KEY_EndCall XKB_KEY_XF86HangupPhone
#define BANANUI_KEY_Call XKB_KEY_XF86PickupPhone
#define BANANUI_KEY_Assistant XKB_KEY_XF86Assistant
#define BANANUI_KEY_Shortcut XKB_KEY_XF86MenuKB

#endif
