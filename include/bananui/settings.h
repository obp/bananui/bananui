/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_SETTINGS_H_
#define _BANANUI_SETTINGS_H_

typedef struct bsSettings bSettings;

bSettings *bCreateSettingsManager(void);
char *bGetSettingString(bSettings *mgr, const char *name, const char *defval);
int bGetSettingInt(bSettings *mgr, const char *name, int defval);
int bSetSettingString(bSettings *mgr, const char *name, const char *val);
int bSetSettingInt(bSettings *mgr, const char *name, int val);
void bDestroySettingsManager(bSettings *mgr);

#endif
