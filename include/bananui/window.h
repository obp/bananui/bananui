/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_WINDOW_H_
#define _BANANUI_WINDOW_H_

#include <cairo.h>
#include <bananui/input_method.h>
#include <bananui/display.h>
#include <bananui/utils.h>
#include <bananui/keys.h>
#include <gio/gio.h>

/**
 * SECTION:window
 * @title: bWindow
 * @short_description: Window object for interacting with the user
 *
 * A #bWindow provides an area for rendering content and various events for
 * receiving user input. Windows are created using #bCreateWindow,
 * #bCreateFullscreenWindow, #bCreateSizedWindow or #bCreateWindow2.
 */

/**
 * bWindow:
 * @ops: pointer to the window implementation.
 * @disp: compositor connection used to display this window.
 * @private: pointer to implementation-specific data.
 * @keydown: triggered when a key is pressed. Parameter type: #bKeyEvent
 * @keyup: triggered when a key is released. Parameter type: #bKeyEvent
 * @fullscreenchange: unused
 * @focusin: triggered when the window is activated by the user.
 * @focusout: triggered when the window is deactivated.
 * @redraw: triggered for each frame. Rendering code should go here.
 * @renderer: cairo renderer for showing visual content during the render callback.
 *
 * The #bWindow structure contains data associated with the window,
 * including event handler lists and private data.
 */
typedef struct bsWindow {
	const struct bsWindowOps *ops;
	bDisplay *disp;
	void *private;

	bEventHandler *keydown;
	bEventHandler *keyup;
	bEventHandler *fullscreenchange;
	bEventHandler *focusin;
	bEventHandler *focusout;
	bEventHandler *redraw;

	cairo_t *renderer;

	/*< private >*/
	bEventHandler *destroy_guard;
	struct xkb_state *xkbstate;

	int width, height, key_repeat_rate, key_repeat_delay;
	xkb_keycode_t keycode;
	unsigned int closing : 1;
	unsigned int focused : 1;

	unsigned int timeout_id;
} bWindow;

/**
 * bWindowOps:
 * @init: implements #bCreateWindow2.
 * @destroy: implements #bDestroyWindow.
 * @request_frame: implements #bRequestFrame.
 * @get_input_method: implements #bGetInputMethod.
 *
 * Implementation of a window.
 */
typedef struct bsWindowOps {
	int (*init)(bWindow *wnd, const char *title, const char *app_id,
			int fullscreen, int width, int height);
	void (*destroy)(bWindow *wnd);
	void (*request_frame)(bWindow *wnd);
	bInputMethod* (*get_input_method)(bWindow *wnd);
} bWindowOps;

/**
 * bKeyEvent:
 * @wnd: the window that this event was triggered on.
 * @keycode: the XKB keycode, equal to the Linux keycode + 8.
 * @sym: the XKB keysym, see #bananui-Keys and <https://github.com/xkbcommon/libxkbcommon/blob/master/include/xkbcommon/xkbcommon-keysyms.h>. 
 * @is_repeat: whether this is a key repeat event.
 * @seqnum: the index of the keysym, needed because a keycode may generate multiple keysyms.
 */
typedef struct bsKeyEvent {
	bWindow *wnd;
	xkb_keycode_t keycode;
	xkb_keysym_t sym;
	int is_repeat, seqnum;
} bKeyEvent;

/**
 * bCreateWindow2:
 * @title: the user-visible title of the window.
 * @app_id: a string sent to the compositor to identify the app. This is usually the same as the base name of the app's desktop file.
 * @fullscreen: 1 if the window should be fullscreen, else 0.
 * @width: width of the window, or -1 to use the default width.
 * @height: height of the window, or -1 to use the default height.
 * @disp: optional #bDisplay to show the window on, %NULL to use the default.
 * @ops: optional #bWindowOps for custom implementations, %NULL to use the default.
 *
 * This is the advanced function for creating a window and provides
 * the highest flexibility. Most users will use #bCreateWindow or the
 * other variants.
 *
 * Returns: A newly created #bWindow, or %NULL if there was an error.
 */
bWindow *bCreateWindow2(const char *title, const char *app_id,
		int fullscreen,
		int width, int height, bDisplay *disp,
		const bWindowOps *ops);

/**
 * bCreateWindow:
 * @title: the user-visible title of the window.
 * @app_id: a string sent to the compositor to identify the app. This is usually the same as the base name of the app's desktop file.
 *
 * Create a simple, ordinary window.
 *
 * Returns: A newly created #bWindow, or %NULL if there was an error.
 */
bWindow *bCreateWindow(const char *title, const char *app_id);

/**
 * bCreateFullscreenWindow:
 * @title: the user-visible title of the window.
 * @app_id: a string sent to the compositor to identify the app. This is usually the same as the base name of the app's desktop file.
 *
 * Create a fullscreen window.
 *
 * Returns: A newly created #bWindow, or %NULL if there was an error.
 */
bWindow *bCreateFullscreenWindow(const char *title, const char *app_id);

/**
 * bCreateSizedWindow:
 * @title: the user-visible title of the window.
 * @app_id: a string sent to the compositor to identify the app. This is usually the same as the base name of the app's desktop file.
 * @width: width of the window, or -1 to use the default width.
 * @height: height of the window, or -1 to use the default height.
 *
 * Create a window with a specific size.
 *
 * Returns: A newly created #bWindow, or %NULL if there was an error.
 */
bWindow *bCreateSizedWindow(const char *title, const char *app_id,
		int width, int height);

/**
 * bWindowKey:
 * @wnd: a #bWindow.
 * @kc: the XKB keycode, not to be confused with a keysym, equal to the Linux keycode + 8
 * @dir: XKB_KEY_UP for releasing a key or XKB_KEY_DOWN for pressing a key
 *
 * Simulate a key event on the window.
 */
void bWindowKey(bWindow *wnd, xkb_keycode_t kc, enum xkb_key_direction dir);

/**
 * bStopKeyRepeat:
 * @wnd: a #bWindow.
 *
 * If a key is currently repeating, stop further repeat events for this key
 * until it is released.
 */
void bStopKeyRepeat(bWindow *wnd);

/**
 * bRequestFrame:
 * @wnd: a #bWindow.
 *
 * Tell the compositor that the window content has changed. This must be
 * called in order to re-render the window.
 */
void bRequestFrame(bWindow *wnd);

/**
 * bDestroyWindow:
 * @wnd: a #bWindow.
 *
 * Hide the window from the screen and free all resources associated with it.
 */
void bDestroyWindow(bWindow *wnd);

/**
 * bGetInputMethod:
 * @wnd: a #bWindow.
 *
 * Request access to the input method.
 *
 * Returns: a newly allocated #bInputMethod object associated with the window,
 * or %NULL if there is no input method.
 */
bInputMethod *bGetInputMethod(bWindow *wnd);

#endif
