/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_UTILS_H_
#define _BANANUI_UTILS_H_

#include <cairo.h>

/**
 * SECTION:utils
 * @title: Event utilities
 * @short_description: Utilities related to event handling
 *
 * Events are defined as #bEventHandler pointers in a struct.
 * Callbacks are executed in reverse order (register last, run first).
 */

/**
 * bEventCallback:
 * @param: event-specific data.
 * @userdata: the user data passed with #bRegisterEventHandler.
 *
 * Type definition for a function passed to #bRegisterEventHandler.
 * This type of function is called when an event is triggered.
 *
 * Returns: 1 if event processing should continue, 0 to prevent other handlers from executing.
 */
typedef int (*bEventCallback)(void *param, void *userdata);

/**
 * bEventHandler:
 *
 * List of #bEventCallback functions.
 */
typedef struct bsEventHandler {
	/*< private >*/
	bEventCallback cb;
	void *userdata;
	struct bsEventHandler *next;
} bEventHandler;

/**
 * bClearEvent:
 * @list: pointer to an event.
 * 
 * Removes all handlers registered on this event. This is needed to free
 * all associated resources.
 */
void bClearEvent(bEventHandler **list);

/**
 * bRegisterEventHandler:
 * @list: pointer to an event
 * @cb: the callback to register
 * @userdata: data to pass to the callback
 *
 * Adds an event callback to the beginning of the list.
 */
void bRegisterEventHandler(bEventHandler **list,
		bEventCallback cb, void *userdata);

/**
 * bUnregisterEventHandler:
 * @list: pointer to an event
 * @cb: the callback to unregister
 * @userdata: data to pass to the callback
 *
 * Removes an event handler from a list. Both @cb and @userdata must
 * match the values used when calling #bRegisterEventHandler.
 *
 * When called from an event handler, it is **unsafe** to unregister
 * other handlers that are related to the same event but scheduled to
 * run after the current handler. This is because #bTriggerEvent keeps
 * a reference to the next handler to call and unregistering a handler
 * does not update this reference.
 *
 * To avoid problems, users of this library should always register an
 * idle callback with GLib for destroying objects or unregistering
 * handlers other than the currently running handler.
 */
void bUnregisterEventHandler(bEventHandler **list,
		bEventCallback cb, void *userdata);

/**
 * bTriggerEvent:
 * @list: an event. Note that this only has one `*`.
 * @param: the event-specific data to pass to all callbacks.
 *
 * Calls all registered callbacks in reverse order.
 *
 * Returns: 0 if a callback aborted processing by returning 0, else 1
 */
int bTriggerEvent(bEventHandler *list, void *param);

/**
 * bTriggerEventSafe:
 * @list: an event. Note that this only has one `*`.
 * @destroy: event triggered when the target object is destroyed.
 * @param: the event-specific data to pass to all callbacks.
 *
 * Call all registered callbacks in reverse order, aborting with
 * a longjmp() call when @destroy is triggered.
 *
 * This often leads to bugs, so instead of destroying objects directly
 * from an event callback, users of this library should register an idle
 * callback with GLib for destroying objects.
 *
 * Returns: 0 if a aborted by @destroy or a callback returning 0, else 1
 */
int bTriggerEventSafe(bEventHandler *list, bEventHandler **destroy,
		void *param);


#endif
