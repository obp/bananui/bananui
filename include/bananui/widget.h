/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_WIDGET_H_
#define _BANANUI_WIDGET_H_

#include <pango/pangocairo.h>
#include <bananui/utils.h>
#include <bananui/script.h>

/**
 * SECTION:widget
 * @title: Widgets
 * @short_description: Base widget types
 *
 * The most important widget type is #bBox. Boxes can be nested and can
 * also contain #bContent widgets (which can represent text, icons or images).
 * A box that contains a #bContent object usually doesn't contain anything else
 * except that one object.
 */

/**
 * bAlignment:
 * @B_ALIGN_START: top or left alignment.
 * @B_ALIGN_CENTER: center alignment.
 * @B_ALIGN_END: bottom or right alignment.
 *
 * Defines the alignment of a box or its text content. Text is always
 * aligned horizontally. Boxes are aligned horizontally in vertical
 * containers and vertically in horizontal containers.
 */
typedef enum beAlignment {
	B_ALIGN_START,
	B_ALIGN_CENTER,
	B_ALIGN_END
} bAlignment;

/**
 * bWrapMode:
 * @B_WRAP_ELLIPSIZE: text is ellipsized, only beginning visible.
 * @B_WRAP_BREAK: text is cut off at character boundaries.
 * @B_WRAP_WORD: text is cut off at word boundaries.
 * @B_WRAP_ELLIPSIZE_START: text is ellipsized, only end visible.
 *
 * Defines what happens to text when it is too long to fit a box.
 */
typedef enum beWrapMode {
	B_WRAP_ELLIPSIZE,
	B_WRAP_BREAK,
	B_WRAP_WORD,
	B_WRAP_ELLIPSIZE_START,
} bWrapMode;

/**
 * bFlowDirection:
 * @B_DOWN: vertical box, top to bottom.
 * @B_LTR: horizontal box, left to right.
 * @B_UP: vertical box, bottom to top.
 * @B_RTL: horizontal box, right to left.
 *
 * Defines the graphical ordering of the children of a box.
 */
typedef enum beFlowDirection {
	B_DOWN,
	B_LTR,
	B_UP,
	B_RTL,
} bFlowDirection;

/**
 * bContentType:
 * @B_CONTENT_TYPE_EMPTY: contains nothing.
 * @B_CONTENT_TYPE_TEXT: contains text.
 * @B_CONTENT_TYPE_IMAGE: contains a raster image.
 * @B_CONTENT_TYPE_IMAGE_SVG: contains an vector image.
 *
 * Defines the type of content in a #bContent object.
 */
typedef enum beContentType {
	B_CONTENT_TYPE_EMPTY,
	B_CONTENT_TYPE_TEXT,
	B_CONTENT_TYPE_IMAGE,
	B_CONTENT_TYPE_IMAGE_SVG
} bContentType;

/**
 * bWidgetType:
 * @B_WIDGET_TYPE_BOX: contains a #bBox object.
 * @B_WIDGET_TYPE_CONTENT: contains a #bContent object.
 *
 * Defines the type of object in a #bWidgetListItem.
 */
typedef enum beWidgetType {
	B_WIDGET_TYPE_BOX,
	B_WIDGET_TYPE_CONTENT
} bWidgetType;

/**
 * bFontStyle:
 * @B_FONT_NORMAL: font is not slanted.
 * @B_FONT_OBLIQUE: font is slanted in a roman style.
 * @B_FONT_ITALIC: font is slanted in an italic style.
 *
 * See <https://docs.gtk.org/Pango/enum.Style.html>.
 */
typedef enum beFontStyle {
	B_FONT_NORMAL = PANGO_STYLE_NORMAL,
	B_FONT_OBLIQUE = PANGO_STYLE_OBLIQUE,
	B_FONT_ITALIC = PANGO_STYLE_ITALIC,
} bFontStyle;

typedef struct bsRsvgHandleWrapper bRsvgHandleWrapper;

/**
 * bWidgetListItem:
 * @owner: the parent of this item.
 * @type: the type of the object contained in this item.
 * @box: if @type is %B_WIDGET_TYPE_BOX, a pointer to the box.
 * @cont: if @type is %B_WIDGET_TYPE_CONTENT, a pointer to the content.
 * @prev: the previous item, or %NULL if this is the first item.
 * @next: the next item, or %NULL if this is the last item.
 *
 * The fields in this structure are not meant to be modified by the user.
 */
typedef struct bsWidgetListItem {
	struct bsBox *owner;
	bWidgetType type;
	union {
		struct bsBox *box;
		struct bsContent *cont;
	};
	struct bsWidgetListItem *prev;
	struct bsWidgetListItem *next;
} bWidgetListItem;

/**
 * bWidgetList:
 * @first: the first item in the list, or %NULL for empty lists.
 * @last: the last item in the list, or %NULL for empty lists.
 *
 * The fields in this structure are not meant to be modified by the user.
 */
typedef struct bsWidgetList {
	bWidgetListItem *first;
	bWidgetListItem *last;
} bWidgetList;

/**
 * bStyle:
 * @background_color: RGBA of color rendered below all content but above containing boxes.
 * @content_color: RGBA of the color used for text and symbolic icons.
 * @warn_color: RGBA of the "warning" color used for symbolic icons.
 * @error_color: RGBA of the "error" color used for symbolic icons.
 * @success_color: RGBA of the "success" color used for symbolic icons.
 * @mgn_t: top margin in pixels.
 * @mgn_l: left margin in pixels.
 * @mgn_b: bottom margin in pixels.
 * @mgn_r: right margin in pixels.
 * @pad_t: top padding in pixels.
 * @pad_l: left padding in pixels.
 * @pad_b: bottom padding in pixels.
 * @pad_r: right padding in pixels.
 * @clip: 1 if content outside the boundaries of this box is clipped, else 0.
 * @alignment: alignment of the box in the containing box.
 * @content_alignment: alignment of the text in this box.
 * @flow_direction: direction in which the children are rendered.
 * @font_style: slant style of the font used for text in this box.
 * @wrap_mode: wrap mode for text that does not fit into the box.
 * @font_weight: weight of the font used for text in this box.
 * @font_size: size of the font used for text in this box.
 * @font_name: name of the font used for text in this box.
 * @min_width: minimum width of the box, or -1 to stretch the box to the maximum available space.
 * @max_width: maximum width of the box, or -1 to limit the width of the box to the parent width.
 * @min_height: minimum height of the box, or -1 to stretch the box to the maximum available space.
 * @max_height: maximum height of the box, or -1 to limit the height of the box to the parent height.
 * @rounding_radius: radius used for rounding the corners. 0 disables rounding.
 * @render_func: function for rendering a special background.
 * @render_class: argument passed to @render_func.
 * @render_userdata: argument passed to @render_func.
 * 
 * The style defines the look of a box.
 */
typedef struct bsStyle {
	double background_color[4];
	double content_color[4];
	double warn_color[4];
	double error_color[4];
	double success_color[4];

	double mgn_t, mgn_l, mgn_b, mgn_r;
	double pad_t, pad_l, pad_b, pad_r;

	int clip;

	bAlignment alignment, content_alignment;
	bFlowDirection flow_direction;
	bFontStyle font_style;
	bWrapMode wrap_mode;
	int font_weight, font_size;

	const char *font_name;

	double min_width, min_height, max_width, max_height, rounding_radius;

	void (*render_func)(cairo_t *cr,
			    double x, double y,
			    double width, double height,
			    void *rclass,
			    void *userdata);
	void *render_class, *render_userdata;
} bStyle;

/**
 * bComputedLayout:
 * @style: the final style of the widget with inheritance rules applied.
 * @width: final width of the box.
 * @height: final height of the box.
 * @top: y-offset of the box.
 * @left: x-offset of the box.
 *
 * Various details about the layout of the box are stored here during
 * and after rendering. The fields in this structure are not meant to
 * be modified by the user and are only valid after the box has been
 * rendered at least once.
 */
typedef struct bsComputedLayout {
	bStyle style;
	double width, height;
	double top, left;

	/*< private >*/
	double used_width, used_height;
	unsigned int width_stretches, height_stretches;
	int horizontal;
	unsigned int token;
} bComputedLayout;

/**
 * bStyleSpec:
 * @override_background_color: uses default (transparent) if 0.
 * @override_content_color: uses inherited value if 0, default is black.
 * @override_warn_color: uses inherited value if 0, default is black.
 * @override_error_color: uses inherited value if 0, default is black.
 * @override_success_color: uses inherited value if 0, default is black.
 * @override_mgn_l: uses default (0) if 0.
 * @override_mgn_t: uses default (0) if 0.
 * @override_mgn_b: uses default (0) if 0.
 * @override_mgn_r: uses default (0) if 0.
 * @override_pad_l: uses default (0) if 0.
 * @override_pad_t: uses default (0) if 0.
 * @override_pad_b: uses default (0) if 0.
 * @override_pad_r: uses default (0) if 0.
 * @override_rounding_radius: uses default (0) if 0.
 * @override_min_width: uses default (0) if 0.
 * @override_max_width: uses default (0) if 0.
 * @override_min_height: uses default (0) if 0.
 * @override_max_height: uses default (0) if 0.
 * @override_clip: uses default (0) if 0.
 * @override_alignment: uses inherited value if 0, default is %B_ALIGN_START.
 * @override_content_alignment: uses inherited value if 0, default is %B_ALIGN_START.
 * @override_wrap_mode: uses inherited value if 0, default is %B_WRAP_WORD.
 * @override_flow_direction: uses inherited value if 0, default is %B_DOWN.
 * @override_font_size: uses inherited value if 0, default is 16.
 * @override_font_style: uses inherited value if 0, default is %B_FONT_NORMAL.
 * @override_font_weight: uses inherited value if 0, default is 500.
 * @items: the style values which this specification overrides.
 *
 * Defines the style of a single box in a box hierarchy. Defaults or
 * inherited values are used for a style parameter if the corresponding
 * override bit is not set. If there is nothing to inherit from, the
 * default is always used when the bit is not set.
 *
 * The font name does not have an override bit because it can be set to
 * %NULL, in which case it is inherited or defaults to "sans-serif".
 */
typedef struct bsStyleSpec {
	unsigned int override_background_color : 1;
	unsigned int override_content_color : 1;
	unsigned int override_warn_color : 1;
	unsigned int override_error_color : 1;
	unsigned int override_success_color : 1;
	unsigned int override_mgn_l : 1;
	unsigned int override_mgn_r : 1;
	unsigned int override_mgn_t : 1;
	unsigned int override_mgn_b : 1;
	unsigned int override_pad_l : 1;
	unsigned int override_pad_r : 1;
	unsigned int override_pad_t : 1;
	unsigned int override_pad_b : 1;
	unsigned int override_rounding_radius : 1;
	unsigned int override_min_width : 1;
	unsigned int override_min_height : 1;
	unsigned int override_max_width : 1;
	unsigned int override_max_height : 1;
	unsigned int override_clip : 1;
	unsigned int override_alignment : 1;
	unsigned int override_content_alignment : 1;
	unsigned int override_flow_direction : 1;
	unsigned int override_font_style : 1;
	unsigned int override_wrap_mode : 1;
	unsigned int override_font_size : 1;
	unsigned int override_font_weight : 1;

	bStyle items;
} bStyleSpec;

/**
 * bBox:
 * @children: list of sub-boxes.
 * @computed: computed layout information.
 * @style: pointer to the style specification (do not modify).
 * @custom_style: optional pointer to a custom user-defined style specification.
 * @focusable: 1 if the box is focusable, else 0.
 * @focusin: triggered when focus moves to this box.
 * @focusout: triggered when focus moves away from this box.
 * @click: triggered when the user clicks the box.
 * @skright: triggered when the user clicks the box with the right softkey.
 * @skleft: triggered when the user clicks the box with the left softkey.
 * @destroy: triggered when the box is destroyed.
 * @self: points to the list item of this box if it has a parent, else %NULL.
 *
 * #bBox is a generic widget structure with a customizable look which
 * can contain #bContent objects or other instances of #bBox.
 */
typedef struct bsBox {
	bWidgetList children;
	bComputedLayout computed;

	bStyleSpec *style;
	bStyleSpec *custom_style;

	unsigned int focusable : 1;

	bEventHandler *focusin;
	bEventHandler *focusout;
	bEventHandler *click;
	bEventHandler *skright;
	bEventHandler *skleft;
	bEventHandler *destroy;
	struct bsWidgetListItem *self;
} bBox;

/**
 * bContent:
 * @type: the type of content represented by this object.
 *
 * An object representing variable content.
 */
typedef struct bsContent {
	bContentType type;
	/*< private >*/
	double imgscale, svgwidth, svgheight, xoffset, yoffset;
	size_t cursor;
	unsigned int cursor_hscroll : 1;
	unsigned int cursor_visible : 1;
	int svgrecolor;
	PangoFontDescription *font;
	PangoLayout *lt;
	union {
		char *text;
		cairo_surface_t *imgsurf;
		bRsvgHandleWrapper *rsvghandle;
		void *data;
	};
	double width, height;
	struct bsWidgetListItem *self;
} bContent;

/**
 * bDefaultTheme:
 * @theme: a script to use as the default theme, or %NULL to leave it unchanged.
 *
 * Set and/or get the default theme.
 *
 * Returns: the theme that was default before this function was called.
 */
bScript *bDefaultTheme(bScript *theme);

/**
 * bRenderBox:
 * @renderer: cairo instance to render the box with.
 * @box: the box to render.
 * @top: y-offset of the box.
 * @left: x-offset of the box.
 *
 * Render the box and all of its children using cairo. The layout is
 * recomputed if needed.
 */
void bRenderBox(cairo_t *renderer, bBox *box, double top, double left);

/**
 * bInsertBox:
 * @box: the box to insert.
 * @before: a box before which @box is inserted.
 *
 * Insert the @box as @before's previous sibling.
 *
 * Returns: -1 on error, 0 on success
 */
int bInsertBox(bBox *box, bBox *before);

/**
 * bAddBox:
 * @box: the parent box.
 * @child: the box to insert.
 *
 * Append @child to the end of @box.
 *
 * Returns: -1 on error, 0 on success
 */
int bAddBox(bBox *box, bBox *child);

/**
 * bAddContent:
 * @box: the parent box.
 * @cont: the content to insert.
 *
 * Append @content to the end of @box.
 *
 * Returns: -1 on error, 0 on success
 */
int bAddContent(bBox *box, bContent *cont);

/**
 * bAddFill:
 * @box: the parent box.
 *
 * Append empty space that expands dynamically to the end of @box.
 * This is equivalent to appending #bCreateBox("fill").
 *
 * Returns: -1 on error, 0 on success
 */
int bAddFill(bBox *box);

/**
 * bCreateBox:
 * @style_name: style to use for the box.
 *
 * Creates a box with the style loaded from the default theme.
 *
 * <ulink url="https://git.abscue.de/obp/bananui/bananui/-/blob/main/themes/common.ipt">themes/common.ipt</ulink>
 * defines some very simple styles:
 *  - "fill" - a box that expands in all directions
 *  - "ltr" - a box with the %B_LTR flow direction
 *  - "limit-width" - a box with the width limited to the maximum available space
 *
 * The default theme also provides a "top" style for a top-level box containing
 * a page with content. The "top" style usually defines reasonable background
 * and foreground colors and leaves space at the bottom for a #bSoftkeyPanel.
 *
 * Returns: an empty, newly allocated box with the specified style.
 */
bBox *bCreateBox(const char *style_name);

/**
 * bBindStyle:
 * @box: a #bBox.
 * @style_name: style to use for the box.
 *
 * Changes the style of the box.
 */
void bBindStyle(bBox *box, const char *style_name);

/**
 * bInvalidate:
 * @box: a #bBox.
 *
 * Force the next #bRenderBox call to recompute the layout of this box and
 * all of its parents.
 */
void bInvalidate(bBox *box);

/**
 * bInvalidateWithChildren:
 * @box: a #bBox.
 *
 * Like #bInvalidate, but also recursively invalidates all children.
 */
void bInvalidateWithChildren(bBox *box);

/**
 * bCreateEmpty:
 *
 * Returns: an empty, newly allocated #bContent object.
 */
bContent *bCreateEmpty(void);

/**
 * bCreateText:
 * @txt: text content.
 *
 * Returns: a newly allocated #bContent object with the specified text, or %NULL on error.
 */
bContent *bCreateText(const char *txt);

/**
 * bCreateImage:
 * @filename: path to a PNG image.
 *
 * Returns: a newly allocated #bContent object with the specified image, or %NULL on error.
 */
bContent *bCreateImage(const char *filename);

/**
 * bCreateImage:
 * @name: system icon name.
 * @size: icon size in pixels.
 *
 * Returns: a newly allocated #bContent object with the specified icon, or %NULL on error.
 */
bContent *bCreateIcon(const char *name, int size);

/**
 * bClearContent:
 * @cont: a #bContent object.
 *
 * Makes @cont an empty content object.
 */
void bClearContent(bContent *cont);

/**
 * bGetLineXAtOffset:
 * @cont: #bContent containing text.
 * @cursor: text offset in bytes.
 * @line: location to store the line index.
 * @x: location to store the x-offset in pixels.
 *
 * Converts a text offset into a line index and a graphical x-offset.
 * Only works with widgets that have already been rendered.
 *
 * Returns: -1 on error, 0 on success
 */
int bGetLineXAtOffset(bContent *cont, int cursor, int *line, int *x);

/**
 * bGetOffsetAtLineX:
 * @cont: #bContent containing text.
 * @line: the line index.
 * @x: the x-offset in pixels.
 *
 * Converts a line index and a graphical x-offset into a text offset.
 * Only works with widgets that have already been rendered.
 *
 * Returns: the text offset in bytes or -1 on error
 */
int bGetOffsetAtLineX(bContent *cont, int line, int x);

/**
 * bGetLastLine:
 * @cont: #bContent containing text.
 *
 * Only works with widgets that have already been rendered.
 *
 * Returns: the index of the last line or -1 on error
 */
int bGetLastLine(bContent *cont);

/**
 * bSetText:
 * @cont: a #bContent object.
 * @txt: text content.
 *
 * Makes @cont contain the specified text.
 *
 * Returns: -1 on error, 0 on success
 */
int bSetText(bContent *cont, const char *txt);

/**
 * bSetImagePath:
 * @cont: a #bContent object.
 * @filename: path to a PNG image.
 *
 * Makes @cont contain the specified image.
 *
 * Returns: -1 on error, 0 on success
 */
int bSetImagePath(bContent *cont, const char *filename);

/**
 * bSetIcon:
 * @cont: a #bContent object.
 * @name: system icon name.
 * @size: icon size in pixels.
 *
 * Makes @cont contain the specified icon.
 *
 * Returns: -1 on error, 0 on success
 */
int bSetIcon(bContent *cont, const char *name, int size);

/**
 * bDestroyBoxRecursive:
 * @box: a #bBox.
 *
 * Free all the resources associated with @box and all of its children,
 * including #bContent items.
 */
void bDestroyBoxRecursive(bBox *box);

/**
 * bDestroyBoxChildren:
 * @box: a #bBox.
 *
 * Recursively free all the children of #bBox, including #bContent items.
 */
void bDestroyBoxChildren(bBox *box);

static inline int _bBoxRefNull(void *param, void *data)
{
	bBox **dest = data;
	*dest = NULL;
	return 1;
}

/**
 * bBoxRef:
 * @dest: location to store a reference to @box.
 * @box: a #bBox.
 *
 * Stores a reference to @box in @dest and registers an event handler
 * that sets @dest to %NULL when @box is destroyed.
 */
static inline void bBoxRef(bBox **dest, bBox *box)
{
	*dest = box;
	bRegisterEventHandler(&box->destroy, _bBoxRefNull, dest);
}


/**
 * bBoxUnref:
 * @dest: location where a reference to a box is stored.
 *
 * Unregisters the event handler that sets @dest to %NULL when the box
 * is destroyed and sets @dest to %NULL.
 */
static inline void bBoxUnref(bBox **dest)
{
	if (*dest) {
		bUnregisterEventHandler(&(*dest)->destroy, _bBoxRefNull, dest);
		*dest = NULL;
	}
}

#endif
