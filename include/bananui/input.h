/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_INPUT_H_
#define _BANANUI_INPUT_H_

#include <bananui/widget.h>
#include <bananui/window.h>

/**
 * SECTION:input
 * @title: Input
 * @short_description: Widget for entering text
 *
 * A #bInputWidget lets the user enter text using the system input method.
 * The input method is automatically obtained from the window when the
 * input widget is focused.
 *
 * The keyboard is currently only used for navigation and can't be used
 * to enter text directly. The bananui-shell implements an input method
 * usable on feature phones.
 *
 * Usage of #bInputWidget is designed to be similar to #bButtonWidget.
 */


/**
 * bInputWidget:
 * @box: the box widget containing the input. When destroyed, this structure is also destroyed.
 *
 * Contains the input's data and sub-widgets.
 */
typedef struct bsInputWidget {
	bBox *box;

	/*< private >*/
	char *text, *preedit;
	int bufsize;
	bEventHandler *change;

	bInputMode mode;
	bInputFlags flags;
	unsigned int focused : 1;
	unsigned int active : 1;
	int cursor, curx;
	bWindow *wnd;
	bInputMethod *im;

	char *style_name, *focused_style_name;

	bContent *cont;
} bInputWidget;

/**
 * bCreateInputWidget:
 * @style_name: style to be used for the input.
 * @mode: #bInputMode indicating the type of data to be entered.
 * @flags: #bInputFlags modifying the behavior of the input.
 *
 * Typical styles are:
 *  - "input" for a single-line text input
 *  - "multiline-input" for %B_INPUT_FLAG_MULTILINE
 *  - "wrap-input" for %B_INPUT_FLAG_MULTILINE_WRAP
 *
 * Returns: a newly allocated #bInputWidget or %NULL on error
 */
bInputWidget *bCreateInputWidget(const char *style_name,
				 bInputMode mode,
				 bInputFlags flags);

/**
 * bGetInputText:
 * @inp: a #bInputWidget.
 *
 * Returns: a copy of the text entered by the user, to be freed with g_free()
 */
char *bGetInputText(bInputWidget *inp);

/**
 * bSetInputText:
 * @inp: a #bInputWidget.
 * @text: text to be set.
 *
 * Replaces the text entered by the user in @inp with @text.
 *
 * Returns: -1 on error, 0 on success
 */
int bSetInputText(bInputWidget *inp, const char *text);

#endif
