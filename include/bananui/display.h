/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_DISPLAY_H_
#define _BANANUI_DISPLAY_H_

/**
 * SECTION:display
 * @title: bDisplay
 * @short_description: Display server connection management
 *
 * A #bDisplay represents a connection to a display server, usually
 * a Wayland compositor.
 */

struct wl_display;

typedef struct bsDisplay {
	const struct bsDisplayOps *ops;
	void *private;

	int fd;
	unsigned int watch;
} bDisplay;

typedef struct bsDisplayOps {
	void (*destroy)(bDisplay *display);
} bDisplayOps;

bDisplay *bCreateWaylandDisplay(const char *name);
struct wl_display *bGetWaylandDisplay(bDisplay *disp);

bDisplay *bGetDefaultDisplay(void);
void bSetDefaultDisplay(bDisplay *disp);

void bDestroyDisplay(bDisplay *disp);

#endif
