/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_SCRIPT_H_
#define _BANANUI_SCRIPT_H_

#include <stdint.h>

/**
 * SECTION:script
 * @title: bScript
 * @short_description: Igel scripts in bananui
 *
 * Bananui provides a scripting interface based on
 * <https://git.abscue.de/stachel-os/igel-interpreter>.
 *
 * A #bScript object represents a dynamically loadable script. Scripts
 * are mainly used for creating themes. The script for the default theme
 * is automatically loaded and can be obtained using #bDefaultTheme.
 */

#ifndef __GTK_DOC_IGNORE__
typedef struct bsStyleSpec bStyleSpec;
typedef struct bsBox bBox;
#endif

/**
 * bScript:
 *
 * Contains the private data associated with a script.
 */
typedef struct bsScript bScript;

/**
 * bLoadScript:
 * @name: the path to the script file relative to the script directory.
 *
 * Load a script from one of the system script directories. Scripts are
 * searched in /usr/share/bananui/scripts, ~/.local/share/bananui/scripts
 * and bananui/scripts in other $XDG_DATA_DIRS.
 *
 * Returns: a #bScript object used to reference this script, or %NULL if
 * the script could not be found.
 */
bScript *bLoadScript(const char *name);

/**
 * bExecuteScript:
 * @script: a #bScript.
 * @entrypoint: the name of the entry point function.
 *
 * Execute a function named [@entrypoint] in a script. Only used
 * internally. If this is called directly, the function will not
 * be able to do anything.
 *
 * Returns: -1 on error, 0 on success
 */
int bExecuteScript(bScript *script, const char *entrypoint);

/**
 * bBuildStyle:
 * @script: a #bScript.
 * @style: #bStyleSpec structure to be filled by the script.
 * @style_name: the name of the style.
 *
 * Load a #bStyleSpec from a theme script. The script must contain a
 * function named [style @style_name].
 *
 * Returns: -1 on error, 0 on success
 */
int bBuildStyle(bScript *script, bStyleSpec *style, const char *style_name);

/**
 * bBuildBox:
 * @script: a #bScript.
 * @style_name: the name of the style.
 * @n_refs: length of @refs.
 * @refs: pointer to array where sub-boxes will be stored.
 *
 * Execute the [build @style_name] function in the script to build a widget.
 * The script may define special sub-boxes for adding content to the widget.
 * These will be stored in @refs. It is recommended to zero out the @refs
 * array before building the box because sub-boxes not defined by the script
 * will not be automatically set to %NULL.
 *
 * Returns: a newly allocated #bBox filled by the script on success, %NULL on error
 */
bBox *bBuildBox(bScript *script, const char *style_name,
		uint16_t n_refs, bBox **refs);

#endif
