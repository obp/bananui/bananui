#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <gio/gio.h>
#include <bananui/window.h>
#include <bananui/menu.h>

#define MAX_MENU_DEPTH 8

static const char about_text[] =
"\n"
"Bananui is a user interface for smart feature phones.\n"
"Copyright (C) 2023 The OpenBananaProject contributors.\n"
"See https://git.abscue.de/obp/bananui\n\n"
"Bananui is free software: you can redistribute it and/or modify it under "
"the terms of the GNU Lesser General Public License as published by the "
"Free Software Foundation, either version 3 of the License, or any later "
"version.\n\n"
"The user interface is distributed in the hope that it will be useful, but "
"WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY "
"or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License"
" for more details.";

struct application_state {
	char *section;
	bWindow *wnd;
	bMenu *menu;
	GApplication *gapp;
	bButtonWidget *checkbox;
	int checked;
};

static int handle_checkbox_click(bMenu *m, void *arg)
{
	struct application_state *as = m->userdata;
	as->checked = !as->checked;
	bSetButtonState(as->checkbox, as->checked);
	bRequestFrame(as->wnd);
	return 1;
}

static int handle_checkbox_button(bMenu *m, bButtonWidget *btn, void *arg)
{
	struct application_state *as = m->userdata;
	as->checkbox = btn;
	if(btn) bSetButtonState(btn, as->checked);
	return 1;
}

B_MENU_ITEMS(about) = {};
B_DEFINE_MENU(about, "About wbananui") {
	bBox *box = bCreateBox("limit-width");
	bAddContent(box, bCreateText(about_text));
	bAddBox(m->current->card->box, box);
}
B_MENU_ITEMS(more) = {
	{
		.name = "Checkbox",
		.func = B_BUTTON_FUNC_CHECKBOX,
		.style = "checkbox",
		.click_handler = handle_checkbox_click,
		.button_handler = handle_checkbox_button,
	},
	{
		.name = "Radio button 1",
		.func = B_BUTTON_FUNC_RADIO,
		.style = "radiobutton",
	},
	{
		.name = "Radio button 2",
		.func = B_BUTTON_FUNC_RADIO,
		.style = "radiobutton",
	},
	{
		.name = "Pushbutton",
		.func = B_BUTTON_FUNC_PUSHBUTTON,
		.style = "pushbutton",
	},
};
B_DEFINE_MENU(more, "More tests") {
}
static bMenuDef test__bMenu__; /* HACK to get recursive menus working */
B_MENU_ITEMS(test) = {
	B_SUBMENU(about, "Same about"),
	B_SUBMENU(about, "Same about again"),
	B_SUBMENU(about, "Same about once again"),
	B_SUBMENU(about, "Same about too long to fit into 240 pixels"),
	B_SUBMENU(test, "This menu itself"),
	B_SUBMENU(more, "More menu tests"),
};
B_DEFINE_MENU(test, "Test") {
}
B_MENU_ITEMS(mm) = {
	B_SUBMENU(test, "Menu test"),
	B_SUBMENU(about, "About"),
};
B_DEFINE_MENU(mm, "Settings") {
}

static int handle_keydown(void *param, void *data)
{
	struct application_state *as = data;
	bKeyEvent *ev = param;

	if(ev->sym == BANANUI_KEY_EndCall){
		g_application_quit(as->gapp);
		return 1;
	}

	if(ev->sym == BANANUI_KEY_Back){
		if(bPopMenu(as->menu, B_MENU_TRANSITION_FADE_SLIDE) == 0){
			g_application_quit(as->gapp);
		}
		/*
		 * bPopMenu manipulates events, so event processing
		 * has to be stopped here.
		 */
		return 0;
	}

	return 1;
}

static int handle_render(void *param, void *data)
{
	struct application_state *as = data;
	bRenderMenu(as->menu);
	return 1;
}

static gint app_command_line(GApplication *self, GVariantDict *options,
		struct application_state *as)
{
	char *sect;
	if(g_variant_dict_lookup(options, "section", "s", &sect)){
		as->section = strdup(sect);
	}
	return -1;
}

static void app_activate(GApplication *self, struct application_state *as)
{
	if(as->wnd){
		/* TODO: Bring to foreground */
		return;
	}
	as->wnd = bCreateWindow("Menu example", "de.abscue.obp.Bananui.MenuTest");
	if(!as->wnd){
		g_error("Failed to create window");
		g_application_quit(self);
	}

	g_application_hold(as->gapp);

	bRegisterEventHandler(&as->wnd->keydown, handle_keydown, as);
	bRegisterEventHandler(&as->wnd->redraw, handle_render, as);

	as->menu = bCreateMenuView(as->wnd);
	as->menu->userdata = as;
	if(as->section){
		if(0 == strcmp(as->section, "about"))
			B_OPEN_MENU(as->menu, about, B_MENU_TRANSITION_NONE);
		else if(0 == strcmp(as->section, "test"))
			B_OPEN_MENU(as->menu, test, B_MENU_TRANSITION_NONE);
		else if(0 == strcmp(as->section, "more"))
			B_OPEN_MENU(as->menu, more, B_MENU_TRANSITION_NONE);
		else
			B_OPEN_MENU(as->menu, mm, B_MENU_TRANSITION_NONE);
	}
	else {
		B_OPEN_MENU(as->menu, mm, B_MENU_TRANSITION_NONE);
	}

}

int main(int argc, char **argv)
{
	struct application_state as = {};

	as.gapp = g_application_new("de.abscue.obp.Bananui.MenuExample",
#if GLIB_CHECK_VERSION(2, 74, 0)
			G_APPLICATION_DEFAULT_FLAGS);
#else /* older GLib versions */
			G_APPLICATION_FLAGS_NONE);
#endif
	g_signal_connect(as.gapp, "activate",
			G_CALLBACK(app_activate), &as);
	g_signal_connect(as.gapp, "handle-local-options",
			G_CALLBACK(app_command_line), &as);

	g_application_add_main_option(as.gapp,
			"section",
			's',
			G_OPTION_FLAG_NONE,
			G_OPTION_ARG_STRING,
			"The section to be opened on startup",
			"SECTION");

	return g_application_run(as.gapp, argc, argv);
}
