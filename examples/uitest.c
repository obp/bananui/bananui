#include <sys/select.h>
#include <cairo.h>
#include <pango/pangocairo.h>
#include <unistd.h>
#include <assert.h>
#include <xkbcommon/xkbcommon.h>
#include <bananui/bananui.h>

static const char the_long_text[] =
"You can navigate using up/left/down/right/backspace\n"
"This is a long text\n"
"With newlines\n\n"
"With paragraphs\n\n"
"With emojis 🔎 and long long long long long long long long long long long long long long long long long long long long long long long lines\n"
"And very looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong words";

#define NUM_VIEWS 4

struct application_state {
	bCard *views[NUM_VIEWS], *prev_view, *header, *softkeys;
	bSoftkeyPanel *sk;
	bButtonWidget *btn, *pbtn;
	bContent *text;
	bWindow *wnd;
	int redraw, clicked;
	int scroll, xscroll;
	int view;
};

static int handle_click_0(void *param, void *data)
{
	fprintf(stderr, "[1] Clicked\n");
	return 1;
}

static int handle_click_1(void *param, void *data)
{
	struct application_state *as = data;
	fprintf(stderr, "[2] Clicked\n");
	as->clicked = !as->clicked;
	bSetText(as->text, as->clicked ? "Clicked!" : "");
	bSetButtonState(as->btn, as->clicked);
	bRequestFrame(as->wnd);
	return 0;
}
static int handle_click_2(void *param, void *data)
{
	assert(0 && "This handler should be skipped!");
	return 0;
}
static int handle_change(void *param, void *data)
{
	fprintf(stderr, "Input text changed: %s\n", (const char *)param);
	return 1;
}

static int handle_render(void *param, void *data)
{
	struct application_state *as = data;

	if(as->prev_view){
		bRenderCard(as->wnd, as->prev_view);
	}
	bRenderCard(as->wnd, as->views[as->view]);
	bRenderCard(as->wnd, as->sk->card);
	if(bAnimationComplete(as->views[as->view])){
		as->prev_view = NULL;
	}
	return 0;
}

static int handle_keydown(void *param, void *data)
{
	struct application_state *as = data;
	bKeyEvent *ev = param;
	if(ev->sym >= 0x20 && ev->sym < 0x7f){
		fprintf(stderr, "Key down: %c (code %d)\n",
				(char)ev->sym, ev->keycode);
	}
	else {
		fprintf(stderr, "Key down: 0x%x (code %d)\n",
				ev->sym, ev->keycode);
	}
	switch(ev->sym){
		case XKB_KEY_Left:
			as->prev_view = as->views[as->view];
			bUnfocusCard(as->wnd, as->prev_view);
			as->view--;
			if(as->view < 0) as->view = NUM_VIEWS-1;
			bTranslateCard(as->views[as->view], -240, 0, 0);
			bFadeCard(as->views[as->view], 0, 0);
			bTranslateCard(as->views[as->view], 0, 0, 0.25);
			bFadeCard(as->views[as->view], 1, 0.25);
			bFocusCard(as->wnd, as->views[as->view]);
			bRequestFrame(as->wnd);
			return 0;
		case XKB_KEY_Right:
			as->prev_view = as->views[as->view];
			bUnfocusCard(as->wnd, as->prev_view);
			as->view++;
			if(as->view >= NUM_VIEWS) as->view = 0;
			bTranslateCard(as->views[as->view], 240, 0, 0);
			bFadeCard(as->views[as->view], 0, 0);
			bTranslateCard(as->views[as->view], 0, 0, 0.25);
			bFadeCard(as->views[as->view], 1, 0.25);
			bFocusCard(as->wnd, as->views[as->view]);
			bRequestFrame(as->wnd);
			return 0;
		case BANANUI_KEY_Back:
		case BANANUI_KEY_EndCall:
			exit(0);
			break;
	}
	return 1;
}

int main(int argc, char * const *argv)
{
	GMainLoop *loop;
	bWindow *wnd;
	bBox *min, *max, *fixed, *longtextbox, *hbuttons;
	bContent *bodytext2, *bodytext3, *bodytext4;
	bContent *mintext, *maxtext, *fixedtext, *longtext;
	bButtonWidget *v0btn1;
	bButtonWidget *v1btn1, *v1btn2, *v1btn3, *v1btn4, *v1btn5, *v1btn6;
	bButtonWidget *v3btn1, *v3btn2, *v3btn3;
	bButtonWidget *pbtn, *pbtn2;
	bInputWidget *inp1, *inp2, *inp3;
	struct application_state as = {};

	bDefaultTheme(bLoadScript("themes/uitest.ipt"));

	wnd = bCreateWindow("UI test", "de.abscue.obp.Bananui.UITest");
	if (!wnd)
		return 1;

	as.views[0] = bCreateCard("top");
	as.views[1] = bCreateCard("top");
	as.views[2] = bCreateCard("top");
	as.views[3] = bCreateCard("top");

	min = bCreateBox("uitest:min");
	max = bCreateBox("uitest:max");
	fixed = bCreateBox("uitest:fix");
	longtextbox = bCreateBox("limit-width");
	hbuttons = bCreateBox("ltr");

	v0btn1 = bCreateButtonWidget("Test Button",
				     "checkbox",
				     B_BUTTON_FUNC_CHECKBOX);
	v1btn1 = bCreateButtonWidget("Test test",
				     "radiobutton",
				     B_BUTTON_FUNC_RADIO);
	v1btn2 = bCreateButtonWidget("Test test test",
				     "radiobutton",
				     B_BUTTON_FUNC_RADIO);
	v1btn3 = bCreateButtonWidget("Test test test test",
				     "radiobutton",
				     B_BUTTON_FUNC_RADIO);
	v1btn4 = bCreateButtonWidget("Test test test test test",
				     "radiobutton",
				     B_BUTTON_FUNC_RADIO);
	v1btn5 = bCreateButtonWidget(
		"Test test test test test test test test test test test",
		"radiobutton",
		B_BUTTON_FUNC_RADIO);
	v1btn6 = bCreateButtonWidget("A button with an icon",
				     "iconbutton",
				     B_BUTTON_FUNC_PUSHBUTTON);
	v3btn1 = bCreateButtonWidget("First button",
				     "uitest:magicbutton",
				     B_BUTTON_FUNC_PUSHBUTTON);
	v3btn2 = bCreateButtonWidget("Second button",
				     "uitest:magicbutton2",
				     B_BUTTON_FUNC_PUSHBUTTON);
	v3btn3 = bCreateButtonWidget("Third button",
				     "uitest:magicbutton2",
				     B_BUTTON_FUNC_PUSHBUTTON);
	pbtn = bCreateButtonWidget("Push me",
				   "pushbutton",
				   B_BUTTON_FUNC_PUSHBUTTON);
	pbtn2 = bCreateButtonWidget("Push me too",
				    "pushbutton",
				    B_BUTTON_FUNC_PUSHBUTTON);
	inp1 = bCreateInputWidget("input",
				  B_INPUT_MODE_ALPHA,
				  B_INPUT_FLAG_NONE);
	bSetInputText(inp1, "Enter something please... test...");
	inp2 = bCreateInputWidget("multiline-input",
				  B_INPUT_MODE_ALPHA,
				  B_INPUT_FLAG_MULTILINE);
	bSetInputText(inp2, "This text has multiple long lines and does not wrap\n"
			    "This is the second line of the text");
	inp3 = bCreateInputWidget("wrap-input",
				  B_INPUT_MODE_ALPHA,
				  B_INPUT_FLAG_MULTILINE_WRAP);
	bSetInputText(inp3, "This text has multiple long lines that wrap\n"
			    "This is the second line of the text\n"
			    "The text is long enough to make this text input\n"
			    "(insert words here...)\n"
			    "(3... 2... 1...)\n"
			    "scroll vertically");

	longtext = bCreateText(the_long_text);
	bodytext2 = bCreateText("small text");
	bodytext3 = bCreateText("Test text");
	bodytext4 = bCreateText(the_long_text);

	mintext = bCreateText("");
	maxtext = bCreateText("");
	fixedtext = bCreateText("");

	as.sk = bCreateSoftkeyPanel();
	as.header = bCreateCard("header");

	bAddBox(as.views[0]->box, min);
	bAddContent(min, mintext);
	bAddBox(as.views[0]->box, max);
	bAddContent(max, maxtext);
	bAddBox(as.views[0]->box, fixed);
	bAddContent(fixed, fixedtext);
	bAddBox(as.views[0]->box, v0btn1->box);
	bAddBox(as.views[0]->box, pbtn->box);
	bAddBox(as.views[0]->box, inp1->box);
	bAddBox(as.views[0]->box, inp2->box);
	bAddBox(as.views[0]->box, inp3->box);
	bAddBox(as.views[0]->box, longtextbox);
	bAddContent(longtextbox, longtext);
	bAddBox(as.views[0]->box, pbtn2->box);
	bAddContent(as.views[0]->box, bodytext2);

	bAddBox(as.views[1]->box, v1btn1->box);
	bAddContent(as.views[1]->box, bodytext3);
	bAddBox(as.views[1]->box, v1btn2->box);
	bAddBox(as.views[1]->box, v1btn3->box);
	bAddBox(as.views[1]->box, v1btn4->box);
	bAddBox(as.views[1]->box, v1btn5->box);
	bAddBox(as.views[1]->box, v1btn6->box);

	bAddContent(as.views[2]->box, bodytext4);

	bAddBox(as.views[3]->box, v3btn1->box);
	bAddBox(as.views[3]->box, hbuttons);
	bAddBox(hbuttons, v3btn2->box);
	bAddBox(hbuttons, v3btn3->box);

	bSetSoftkeyText(as.sk, "Hello", "WONDERFUL", "World");

	bSetButtonSubtitle(v0btn1, "Hello Subtitle");
	bSetButtonSubtitle(v3btn1, "With a subtitle");
	bSetButtonIcon(v1btn6, "battery-level-10-symbolic", 32);

	as.scroll = 0;
	as.xscroll = 0;
	as.text = mintext;
	as.btn = v0btn1;
	as.pbtn = pbtn;
	as.clicked = 0;
	as.wnd = wnd;

	bRegisterEventHandler(&v0btn1->box->click, handle_click_2, &as);
	bRegisterEventHandler(&v0btn1->box->click, handle_click_1, &as);
	bRegisterEventHandler(&v0btn1->box->click, handle_click_0, &as);
	bRegisterEventHandler(&inp1->change, handle_change, &as);

	bRegisterEventHandler(&wnd->keydown, handle_keydown, &as);
	bRegisterEventHandler(&wnd->redraw, handle_render, &as);

	bFocusCard(wnd, as.views[0]);

	bRequestFrame(wnd);

	loop = g_main_loop_new(NULL, TRUE);
	g_main_loop_run(loop);

	return 0;
}
