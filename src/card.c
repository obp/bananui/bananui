/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <bananui/card.h>
#include <math.h>

#define timediff(_a, _b) \
	(((_b).tv_nsec - (_a).tv_nsec) / 1000000000.0 + \
	 ((_b).tv_sec - (_a).tv_sec))

bCard *bCreateCard(const char *style_name)
{
	bCard *card = g_new0(bCard, 1);
	card->scale.to[0] = card->scale.to[1] = card->opacity.to = 1;
	card->box = bCreateBox(style_name);
	card->box->custom_style = &card->style;
	card->style.override_min_width = 1;
	card->style.override_min_height = 1;
	return card;
}

bCard *bBuildCard(bScript *script,
		  const char *style_name,
		  uint16_t n_refs,
		  bBox **refs)
{
	bCard *card = g_new0(bCard, 1);
	card->scale.to[0] = card->scale.to[1] = card->opacity.to = 1;
	card->box = bBuildBox(script, style_name, n_refs, refs);
	if (!card->box) {
		g_free(card);
		return NULL;
	}
	card->box->custom_style = &card->style;
	card->style.override_min_width = 1;
	card->style.override_min_height = 1;
	return card;
}

static void getAnimatedXY(bCardAnimationXY *anim, double *x, double *y,
		struct timespec *now)
{
	double td = timediff(anim->time, *now);
	double x0 = anim->from[0], y0 = anim->from[1];
	double x1 = anim->to[0], y1 = anim->to[1];
	if(td >= anim->duration){
		anim->duration = 0;
		*x = x1;
		*y = y1;
	}
	else {
		double progress = td / anim->duration;
		*x = x0 + progress * (x1 - x0);
		*y = y0 + progress * (y1 - y0);
	}
}

static double getAnimated(bCardAnimation *anim, struct timespec *now)
{
	double td = timediff(anim->time, *now);
	if(td >= anim->duration){
		anim->duration = 0;
		return anim->to;
	}
	return anim->from + (td / anim->duration) * (anim->to - anim->from);
}

static void animateCardXY(bCardAnimationXY *anim,
		double x, double y, double duration)
{
	struct timespec new_time;
	clock_gettime(CLOCK_MONOTONIC, &new_time);
	getAnimatedXY(anim, &anim->from[0], &anim->from[1], &new_time);
	anim->to[0] = x;
	anim->to[1] = y;
	anim->time = new_time;
	anim->duration = duration;
}

static void animateCard(bCardAnimation *anim, double val, double duration)
{
	struct timespec new_time;
	clock_gettime(CLOCK_MONOTONIC, &new_time);
	anim->from = getAnimated(anim, &new_time);
	anim->to = val;
	anim->time = new_time;
	anim->duration = duration;
}

void bScaleCard(bCard *card, double x, double y, double time)
{
	animateCardXY(&card->scale, x, y, time);
}
void bTranslateCard(bCard *card, double x, double y, double time)
{
	animateCardXY(&card->translate, x, y, time);
}
void bScrollCard(bCard *card, double x, double y, double time)
{
	animateCardXY(&card->scroll, x, y, time);
}
void bRotateCard(bCard *card, double angle, double time)
{
	animateCard(&card->rotate, angle, time);
}
void bFadeCard(bCard *card, double opacity, double time)
{
	animateCard(&card->opacity, opacity, time);
}

void bAnimateCardWith(bCard *card, bCard *master)
{
	card->opacity = master->opacity;
	card->rotate = master->rotate;
	card->translate = master->translate;
	card->scale = master->scale;
	/* setting scroll here is possibly unexpected behavior */
}

int bAnimationComplete(bCard *card)
{
	return card->opacity.duration == 0 &&
		card->rotate.duration == 0 &&
		card->translate.duration == 0 &&
		card->scroll.duration == 0 &&
		card->scale.duration == 0;
}

static int handle_focus_destroy(void *param, void *data)
{
	bCard *card = data;
	card->focus = NULL;
	return 1;
}

void bFocusBox(bWindow *wnd, bCard *card, bBox *box)
{
	bFocusEvent ev = {
		.wnd = wnd,
		.card = card,
		.box = card->focus,
	};
	if(card->has_focus && card->focus && card->focus->focusable){
		bTriggerEvent(card->focus->focusout, &ev);
		bUnregisterEventHandler(&card->focus->destroy,
			handle_focus_destroy, card);
	}
	card->focus = ev.box = box;
	card->scrolled_to_focus = 0;
	if(card->has_focus && card->focus && card->focus->focusable){
		bTriggerEvent(card->focus->focusin, &ev);
		bRegisterEventHandler(&card->focus->destroy,
			handle_focus_destroy, card);
	}
}

static bBox *navigate(bBox *focus, bFlowDirection dir)
{
	const bWidgetListItem dummy_widget = {
		.type = B_WIDGET_TYPE_CONTENT, /* not a box */
		.owner = focus,
		/* everything else does not matter */
	};
	const bWidgetListItem *init = focus->self ?: &dummy_widget, *tmp = init;
	bFlowDirection flow_dir;
	int opposite;
#if BANANUI_DEBUG_FOCUS
	fprintf(stderr, "nav start -- %p\n", tmp);
#endif
	do {
		if(init == &dummy_widget) init = tmp;
		flow_dir = tmp->owner->computed.style.flow_direction;
		opposite = (flow_dir == B_UP && dir == B_DOWN) ||
			(flow_dir == B_DOWN && dir == B_UP) ||
			(flow_dir == B_RTL && dir == B_LTR) ||
			(flow_dir == B_LTR && dir == B_RTL);
		if(tmp->type == B_WIDGET_TYPE_BOX && (opposite
				? tmp->box->children.last
				: tmp->box->children.first))
		{
			tmp = opposite
				? tmp->box->children.last
				: tmp->box->children.first;
#if BANANUI_DEBUG_FOCUS
			fprintf(stderr, "nav downto%s -> %p\n",
					opposite ? "last" : "first",
					tmp);
#endif
		}
		else if(opposite ? tmp->prev : tmp->next){
			tmp = opposite ? tmp->prev : tmp->next;
#if BANANUI_DEBUG_FOCUS
			fprintf(stderr, "nav %s -> %p\n",
					opposite ? "prev" : "next",
					tmp);
#endif
		}
		else do {
			if(!tmp->owner->self){
				if(!opposite && flow_dir != dir) return focus;
				tmp = opposite
					? tmp->owner->children.last
					: tmp->owner->children.first;
#if BANANUI_DEBUG_FOCUS
				fprintf(stderr, "nav wrapto%s -> %p\n",
						opposite ? "last" : "first",
						tmp);
#endif
				break;
			}
			tmp = tmp->owner->self;
			flow_dir = tmp->owner->computed.style.flow_direction;
			opposite = (flow_dir == B_UP && dir == B_DOWN) ||
				(flow_dir == B_DOWN && dir == B_UP) ||
				(flow_dir == B_RTL && dir == B_LTR) ||
				(flow_dir == B_LTR && dir == B_RTL);
			if((opposite && tmp->prev) ||
					(flow_dir == dir && tmp->next))
			{
				tmp = opposite ? tmp->prev : tmp->next;
#if BANANUI_DEBUG_FOCUS
				fprintf(stderr, "nav upto%s -> %p\n",
						opposite ? "prev" : "next",
						tmp);
#endif
				break;
			}
		} while(1);
	} while(tmp && tmp != init &&
		(tmp->type != B_WIDGET_TYPE_BOX || !tmp->box->focusable));
	if(tmp && tmp->type == B_WIDGET_TYPE_BOX && tmp->box->focusable){
#if BANANUI_DEBUG_FOCUS
		fprintf(stderr, "focusable found\n");
#endif
		return tmp->box;
	}
#if BANANUI_DEBUG_FOCUS
	fprintf(stderr, "no focusable found\n");
#endif
	return focus;
}

static int handle_keydown(void *param, void *data)
{
	bKeyEvent *ev = param;
	bCard *card = data;
	bBox *next = NULL;
	double sx = card->scroll.to[0], sy = card->scroll.to[1];
	double viewframe_height = ev->wnd->height -
		card->box->computed.style.pad_t -
		card->box->computed.style.pad_b;
	double viewframe_width = ev->wnd->width -
		card->box->computed.style.pad_l -
		card->box->computed.style.pad_r;
	double offset;

	/* This happens if the card hasn't been rendered yet */
	if (!card->focus)
		return 1;

	if (!bTriggerEvent(card->keydown, ev))
		return 0;

	switch(ev->sym){
		case XKB_KEY_Up:
			next = navigate(card->focus, B_UP);
			if(next == card->focus){
				sy -= 20;
				break;
			}
			offset = next->computed.top;
			if((offset - sy) < -0.5*viewframe_height){
				next = card->focus;
			}
			else if((offset + next->computed.height) >
					(sy+viewframe_height))
			{
				if(sy == 0)
					bScrollCard(card, sx, card->maxsy, 0);
				else
					next = card->focus;
			}
			sy -= 20;
			break;
		case XKB_KEY_Down:
			next = navigate(card->focus, B_DOWN);
			if(next == card->focus){
				sy += 20;
				break;
			}
			offset = next->computed.top;
			if((offset - sy) > 1.5*viewframe_height){
				next = card->focus;
			}
			if((offset + next->computed.height) < sy){
				if(sy == card->maxsy)
					bScrollCard(card, sx, 0, 0);
				else
					next = card->focus;
			}
			sy += 20;
			break;
		case XKB_KEY_Left:
			next = navigate(card->focus, B_RTL);
			if(next == card->focus){
				sx -= 20;
				break;
			}
			offset = next->computed.left;
			if((offset - sx) < -0.5*viewframe_width){
				next = card->focus;
			}
			else if((offset + next->computed.width) >
					(sx+viewframe_width))
			{
				if(sx == 0)
					bScrollCard(card, card->maxsx, sy, 0);
				else
					next = card->focus;
			}
			sx -= 20;
			break;
		case XKB_KEY_Right:
			next = navigate(card->focus, B_LTR);
			if(next == card->focus){
				sx += 20;
				break;
			}
			offset = next->computed.left;
			if((offset - sx) > 1.5*viewframe_width){
				next = card->focus;
			}
			if((offset + next->computed.width) < sx){
				if(sx == card->maxsx)
					bScrollCard(card, 0, sy, 0);
				else
					next = card->focus;
			}
			sx += 20;
			break;
		case XKB_KEY_Return:
			if(card->focus && card->focus->focusable){
				return bTriggerEvent(card->focus->click,
						card->focus);
			}
			return 1;
		case BANANUI_KEY_SoftLeft:
			if(card->focus && card->focus->focusable){
				return bTriggerEvent(card->focus->skleft,
						card->focus);
			}
			return 1;
		case BANANUI_KEY_SoftRight:
			if(card->focus && card->focus->focusable){
				return bTriggerEvent(card->focus->skright,
						card->focus);
			}
			return 1;
		default:
			return 1;
	}
	bRequestFrame(ev->wnd);
	if(card->focus == next){
		if(sx > card->maxsx) sx = card->maxsx;
		if(sy > card->maxsy) sy = card->maxsy;
		if(sx < 0) sx = 0;
		if(sy < 0) sy = 0;
		if(card->scroll.to[0] == sx && card->scroll.to[1] == sy){
			return 1;
		}
		else {
			bScrollCard(card, sx, sy, 0.1);
			return 0;
		}
	}
	bFocusBox(ev->wnd, card, next);
	return 0;
}

static int handle_keyup(void *param, void *data)
{
	bCard *card = data;

	return bTriggerEvent(card->keyup, param);
}

void bFocusCard(bWindow *wnd, bCard *card)
{
	bRegisterEventHandler(&wnd->keydown, handle_keydown, card);
	bRegisterEventHandler(&wnd->keyup, handle_keyup, card);
	card->has_focus = 1;
	if (card->focus && card->focus->focusable) {
		bFocusEvent ev = {
			.wnd = wnd,
			.card = card,
			.box = card->focus,
		};
		card->scrolled_to_focus = 0;
		bTriggerEvent(card->focus->focusin, &ev);
	}
}

void bUnfocusCard(bWindow *wnd, bCard *card)
{
	bUnregisterEventHandler(&wnd->keydown, handle_keydown, card);
	bUnregisterEventHandler(&wnd->keyup, handle_keyup, card);
	if (card->focus && card->focus->focusable) {
		bFocusEvent ev = {
			.wnd = wnd,
			.card = card,
			.box = card->focus,
		};
		bTriggerEvent(card->focus->focusout, &ev);
	}
	card->has_focus = 0;
}

void bRenderCard(bWindow *wnd, bCard *card)
{
	double sx, sy, tx, ty, scx, scy, rotation, opacity;
	struct timespec now;
	bRenderParams rp = {
		.wnd = wnd,
		.renderer = wnd->renderer,
	};

	if(card->has_focus && !card->focus){
		bFocusBox(wnd, card, navigate(card->box, B_DOWN));
	}

	card->style.items.min_width = wnd->width;
	card->style.items.min_height = wnd->height;

	cairo_save(wnd->renderer);

	clock_gettime(CLOCK_MONOTONIC, &now);
	getAnimatedXY(&card->scale, &sx, &sy, &now);
	cairo_scale(wnd->renderer, sx, sy);
	getAnimatedXY(&card->translate, &tx, &ty, &now);
	cairo_translate(wnd->renderer, tx, ty);
	getAnimatedXY(&card->scroll, &scx, &scy, &now);
	cairo_translate(wnd->renderer, -scx, -scy);
	rotation = getAnimated(&card->rotate, &now);
	cairo_rotate(wnd->renderer, rotation);
	opacity = getAnimated(&card->opacity, &now);

#if BANANUI_DEBUG_ANIMATION
	fprintf(stderr, "O:%f R:%+f T:(%+f%+f|%+f%+f) S:(%f|%f)\n",
			opacity, rotation, tx, -scx, ty, -scy, sx, sy);
#endif

	cairo_push_group(wnd->renderer);
	bTriggerEvent(card->prerender, &rp);
	bRenderBox(wnd->renderer, card->box, 0, 0);
	bTriggerEvent(card->postrender, &rp);
	cairo_pop_group_to_source(wnd->renderer);
	cairo_paint_with_alpha(wnd->renderer, opacity);

	cairo_restore(wnd->renderer);

	card->maxsx = card->box->computed.width - wnd->width;
	card->maxsy = card->box->computed.height - wnd->height;

	if(!bAnimationComplete(card)){
		bRequestFrame(wnd);
	}
	else if(card->has_focus && card->focus && card->focus != card->box &&
			!card->scrolled_to_focus)
	{
		double viewframe_top = card->box->computed.style.pad_t + scy;
		double viewframe_left = card->box->computed.style.pad_l + scx;
		double viewframe_bottom = wnd->height + scy -
			card->box->computed.style.pad_b;
		double viewframe_right = wnd->width + scx -
			card->box->computed.style.pad_r;
		double focus_top = card->focus->computed.top -
			card->focus->computed.style.mgn_t;
		double focus_left = card->focus->computed.left -
			card->focus->computed.style.mgn_l;
		double focus_bottom = card->focus->computed.top +
			card->focus->computed.height +
			card->focus->computed.style.mgn_b;
		double focus_right = card->focus->computed.left +
			card->focus->computed.width +
			card->focus->computed.style.mgn_r;
		double scy_new = scy, scx_new = scx;
		if(focus_bottom > viewframe_bottom ||
				focus_top < viewframe_top)
		{
			scy_new = scy -
				(fabs(viewframe_bottom - focus_bottom) <
				fabs(viewframe_top - focus_top)
				? (viewframe_bottom - focus_bottom)
				: (viewframe_top - focus_top));
		}
		if(focus_right > viewframe_right ||
				focus_left < viewframe_left)
		{
			scx_new = scx -
				(fabs(viewframe_right - focus_right) <
				fabs(viewframe_left - focus_left)
				? (viewframe_right - focus_right)
				: (viewframe_left - focus_left));
		}
#if BANANUI_DEBUG_ANIMATION
		fprintf(stderr, "Bounds %f|%f|%f|%f ; %f|%f|%f|%f\n",
				viewframe_left, viewframe_top,
				viewframe_right, viewframe_bottom,
				focus_left, focus_top,
				focus_right, focus_bottom);
		fprintf(stderr, "Autoscroll %f|%f -> %f|%f\n",
				scx, scy, scx_new, scy_new);
#endif
		if(scy_new != scy || scx_new != scx){
			bScrollCard(card, scx_new, scy_new, 0.1);
			bRequestFrame(wnd);
		}
		card->scrolled_to_focus = 1;
	}
}

void bDestroyCard(bCard *card)
{
	bDestroyBoxRecursive(card->box);
	bClearEvent(&card->keydown);
	bClearEvent(&card->keyup);
	bClearEvent(&card->prerender);
	bClearEvent(&card->postrender);
}
