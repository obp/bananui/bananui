/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bananui-im"

#include <glib.h>
#include <bananui/input_method.h>
#include "text-input-client-protocol.h"

typedef struct bsWaylandInputMethod {
	struct wl_display *disp;
	struct zwp_text_input_v3 *text_input;

	/* set by compositor */
	char *preedit, *commit_string;
	unsigned int delete_before, delete_after;
	/* set by application */
	bInputMode mode;
	bInputFlags flags;
	char *text;
	int cursor, anchor;
} bWaylandInputMethod;

static void text_input_handle_enter(void *data, struct zwp_text_input_v3 *ti,
	struct wl_surface *surface)
{
	bInputMethod *im = data;
	bWaylandInputMethod *wim = im->private;
	g_assert(ti == wim->text_input);
	bTriggerEventSafe(im->enter, &im->destroy_guard, im);
}

static void text_input_handle_leave(void *data, struct zwp_text_input_v3 *ti,
	struct wl_surface *surface)
{
	bInputMethod *im = data;
	bWaylandInputMethod *wim = im->private;
	g_assert(ti == wim->text_input);
	bTriggerEventSafe(im->leave, &im->destroy_guard, im);
}

static void text_input_handle_preedit_string(void *data,
	struct zwp_text_input_v3 *ti,
	const char *text, int32_t cursor_begin, int32_t cursor_end)
{
	bInputMethod *im = data;
	bWaylandInputMethod *wim = im->private;
	g_assert(ti == wim->text_input);
	g_free(wim->preedit);
	wim->preedit = g_strdup(text);
}

static void text_input_handle_commit_string(void *data,
	struct zwp_text_input_v3 *ti,
	const char *text)
{
	bInputMethod *im = data;
	bWaylandInputMethod *wim = im->private;
	g_assert(ti == wim->text_input);
	g_free(wim->commit_string);
	wim->commit_string = g_strdup(text);
}

static void text_input_handle_delete_surrounding_text(void *data,
	struct zwp_text_input_v3 *ti,
	uint32_t before, uint32_t after)
{
	bInputMethod *im = data;
	bWaylandInputMethod *wim = im->private;
	g_assert(ti == wim->text_input);
	wim->delete_before = before;
	wim->delete_after = after;
}

static void text_input_handle_done(void *data,
	struct zwp_text_input_v3 *ti, uint32_t serial)
{
	bInputMethod *im = data;
	bWaylandInputMethod *wim = im->private;
	bInputMethodEvent ev = {
		.preedit = wim->preedit,
		.commit_string = wim->commit_string,
		.delete_before = wim->delete_before,
		.delete_after = wim->delete_after,
		.im = im
	};
	g_assert(ti == wim->text_input);
	bTriggerEventSafe(im->update, &im->destroy_guard, &ev);
	g_clear_pointer(&wim->commit_string, g_free);
	g_clear_pointer(&wim->preedit, g_free);
	wim->delete_before = wim->delete_after = 0;
}

static const struct zwp_text_input_v3_listener text_input_listener = {
	.enter = text_input_handle_enter,
	.leave = text_input_handle_leave,
	.preedit_string = text_input_handle_preedit_string,
	.commit_string = text_input_handle_commit_string,
	.delete_surrounding_text = text_input_handle_delete_surrounding_text,
	.done = text_input_handle_done,
};

static void wayland_im_report_saved(bInputMethod *im)
{
	bWaylandInputMethod *wim = im->private;
	uint32_t hint = 0, purpose = 0;

	switch(wim->mode){
		case B_INPUT_MODE_NORMAL:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NORMAL;
			break;
		case B_INPUT_MODE_ALPHA:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_ALPHA;
			break;
		case B_INPUT_MODE_DIGITS:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DIGITS;
			break;
		case B_INPUT_MODE_NUMBER:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NUMBER;
			break;
		case B_INPUT_MODE_PHONE:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PHONE;
			break;
		case B_INPUT_MODE_URL:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_URL;
			break;
		case B_INPUT_MODE_EMAIL:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_EMAIL;
			break;
		case B_INPUT_MODE_NAME:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NAME;
			break;
		case B_INPUT_MODE_PASSWORD:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PASSWORD;
			hint |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_SENSITIVE_DATA;
			break;
		case B_INPUT_MODE_PIN:
			purpose = ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PIN;
			hint |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_SENSITIVE_DATA;
			break;
		default:
			break;
	}

	if(wim->flags & B_INPUT_FLAG_MULTILINE)
		hint |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_MULTILINE;
	if(wim->flags & B_INPUT_FLAG_MULTILINE_WRAP)
		hint |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_MULTILINE;

	zwp_text_input_v3_set_content_type(wim->text_input, hint, purpose);
	if(wim->text){
		zwp_text_input_v3_set_surrounding_text(wim->text_input,
				wim->text, wim->cursor, wim->anchor);
	}
	zwp_text_input_v3_commit(wim->text_input);
	wl_display_flush(wim->disp);
}

static void wayland_im_report(bInputMethod *im, bInputMode mode,
		bInputFlags flags, const char *text, int cursor, int anchor)
{
	bWaylandInputMethod *wim = im->private;

	g_free(wim->text);
	wim->mode = mode;
	wim->flags = flags;
	if(text && !g_utf8_validate(text, 0, NULL)){
		wim->text = NULL;
		g_warning("Not reporting text because it is invalid UTF-8");
	}
	else if(text){
		/*
		 * Send up to 4000 bytes with the selection in the middle
		 *
		 * .......*********|*******************^*********.......
		 *        |        `---selection-------'        |
		 *        `-----sent bytes----------------------'
		 */
		int sel_begin, sel_end;
		const char *begin, *mid;
		char *end;
		if(wim->cursor < wim->anchor){
			sel_begin = cursor;
			sel_end = anchor;
		}
		else {
			sel_end = cursor;
			sel_begin = anchor;
		}
		if((sel_end - sel_begin) >= 4000){
			sel_end = sel_begin = anchor = cursor;
			g_warning("Not reporting selection because it is too long");
		}
		/* Find where the selection should start */
		mid = text + sel_begin + (sel_end - sel_begin) / 2;
		begin = text;
		while((begin + 2000) < mid){
			begin = g_utf8_next_char(begin);
		}
		/* Copy the selection */
		wim->text = g_strdup(begin);
		wim->cursor = cursor - (begin - text);
		wim->anchor = anchor - (begin - text);
		/* Find where the selection should end */
		end = wim->text;
		while(1){
			char *next = g_utf8_next_char(end);
			if(!*next){
				break;
			}
			else if((next - wim->text) >= 4000){
				*end = '\0';
				break;
			}
			else {
				end = next;
			}
		}
		g_debug("Reporting text: %s (cursor=%d, anchor=%d)", wim->text,
				wim->cursor, wim->anchor);
	}
	else {
		wim->text = NULL;
	}

	/* The state can only be committed when the input is enabled. */
	if(im->enabled) wayland_im_report_saved(im);
}

static void wayland_im_enable(bInputMethod *im)
{
	bWaylandInputMethod *wim = im->private;
	zwp_text_input_v3_enable(wim->text_input);
	im->enabled = 1;
	/* Commit saved state now that the input is enabled */
	wayland_im_report_saved(im);
}

static void wayland_im_disable(bInputMethod *im)
{
	bWaylandInputMethod *wim = im->private;
	zwp_text_input_v3_disable(wim->text_input);
	zwp_text_input_v3_commit(wim->text_input);
	im->enabled = 0;
}

static void wayland_im_destroy(bInputMethod *im)
{
	bWaylandInputMethod *wim = im->private;
	zwp_text_input_v3_destroy(wim->text_input);
	g_free(wim);
}

static const bInputMethodOps wayland_im_ops = {
	.report = wayland_im_report,
	.enable = wayland_im_enable,
	.disable = wayland_im_disable,
	.destroy = wayland_im_destroy
};

bInputMethod *bCreateWaylandInputMethod(struct wl_display *disp,
		struct zwp_text_input_v3 *text_input)
{
	bInputMethod *im = g_malloc0(sizeof(bInputMethod));
	bWaylandInputMethod *wim = g_malloc0(sizeof(bWaylandInputMethod));
	im->ops = &wayland_im_ops;
	im->private = wim;
	wim->disp = disp;
	wim->text_input = text_input;
	zwp_text_input_v3_add_listener(text_input, &text_input_listener, im);
	return im;
}

void bReportInputMethodState(bInputMethod *im, bInputMode mode,
		bInputFlags flags, const char *text, int cursor, int anchor)
{
	im->ops->report(im, mode, flags, text, cursor, anchor);
}

void bEnableInputMethod(bInputMethod *im)
{
	im->ops->enable(im);
}

void bDisableInputMethod(bInputMethod *im)
{
	im->ops->disable(im);
}

void bDestroyInputMethod(bInputMethod *im)
{
	bEventHandler *destroy = im->destroy_guard;
	im->ops->destroy(im);
	bClearEvent(&im->update);
	bClearEvent(&im->leave);
	bClearEvent(&im->enter);
	g_free(im);
	bTriggerEvent(destroy, destroy);
}
