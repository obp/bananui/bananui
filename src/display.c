/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bananui-display"

#include <glib.h>
#include <wayland-client.h>
#include <bananui/display.h>

static bDisplay *default_display = NULL;

static gboolean wayland_disp_io(GIOChannel *source, GIOCondition condition,
		gpointer disp)
{
	if(wl_display_dispatch(disp) < 0){
		g_error("Wayland compositor disconnected");
	}
	return TRUE;
}

void wayland_disp_destroy(bDisplay *disp)
{
	g_source_remove(disp->watch);
	wl_display_disconnect(disp->private);
}

static const bDisplayOps wayland_disp_ops = {
	.destroy = wayland_disp_destroy,
};

bDisplay *bCreateWaylandDisplay(const char *name)
{
	GIOChannel *iochannel;
	bDisplay *disp = g_malloc0(sizeof(bDisplay));
	disp->ops = &wayland_disp_ops;
	disp->private = wl_display_connect(name);
	if(!disp->private){
		g_critical("Failed to connect to Wayland display\n");
		free(disp);
		return NULL;
	}

	iochannel = g_io_channel_unix_new(
			wl_display_get_fd(disp->private));
	disp->watch = g_io_add_watch(iochannel,
			G_IO_IN | G_IO_HUP | G_IO_ERR,
			wayland_disp_io, disp->private);
	g_io_channel_unref(iochannel);

	return disp;
}

struct wl_display *bGetWaylandDisplay(bDisplay *disp)
{
	return disp->ops == &wayland_disp_ops ? disp->private : NULL;
}

void bDestroyDisplay(bDisplay *disp)
{
	disp->ops->destroy(disp);
	free(disp);
}

bDisplay *bGetDefaultDisplay(void)
{
	if(default_display) return default_display;
	return default_display = bCreateWaylandDisplay(NULL);
}

void bSetDefaultDisplay(bDisplay *display)
{
	default_display = display;
}
