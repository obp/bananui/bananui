/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <glib.h>
#include <setjmp.h>
#include <bananui/utils.h>

void bClearEvent(bEventHandler **list)
{
	while(*list){
		bEventHandler *tmp;
		tmp = (*list)->next;
		g_free(*list);
		*list = tmp;
	}
}

void bRegisterEventHandler(bEventHandler **list, bEventCallback cb,
	void *userdata)
{
	bEventHandler *tmp;
	tmp = g_malloc(sizeof(bEventHandler));
	tmp->next = *list;
	tmp->cb = cb;
	tmp->userdata = userdata;
	*list = tmp;
}

void bUnregisterEventHandler(bEventHandler **list, bEventCallback cb,
	void *userdata)
{
	for(; *list; list = &((*list)->next)){
		if((*list)->cb == cb && (*list)->userdata == userdata){
			bEventHandler *tmp;
			tmp = (*list)->next;
			g_free(*list);
			*list = tmp;
			return;
		}
	}
}

int bTriggerEvent(bEventHandler *list, void *param)
{
	bEventHandler *tmp;
	while (list) {
		tmp = list->next;
		if (!list->cb(param, list->userdata)) {
			return 0;
		}
		list = tmp;
	}
	return 1;
}

static int destroy_guard(void *param, void *data)
{
	jmp_buf *jb = data;
	/* only expecting one handler to attach to the guard event */
	g_free(param);
	longjmp(*jb, 1);
	/* never returns */
}

/* used to stop processing after the object triggering the event disappears */
int bTriggerEventSafe(bEventHandler *list_, bEventHandler **destroy,
		void *param)
{
	bEventHandler *tmp, *list;
	int ret;
	jmp_buf jb;

	if(setjmp(jb) != 0) return -1; /* executed when destroyed */

	/*
	 * make sure not to clobber variables even if setjmp returns 0 again
	 * for some reason (silences compiler warning)
	 */
	ret = 1;
	list = list_;

	bRegisterEventHandler(destroy, destroy_guard, &jb);
	while (list) {
		tmp = list->next;
		if(!list->cb(param, list->userdata)){
			ret = 0;
			break;
		}
		list = tmp;
	}
	bUnregisterEventHandler(destroy, destroy_guard, &jb);
	return ret;
}
