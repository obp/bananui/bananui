/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bananui-misc"

#include <math.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <glib.h>
#include <librsvg/rsvg.h>
#include <cairo.h>
#include <pango/pangocairo.h>
#include <assert.h>
#include <bananui/utils.h>
#include <bananui/script.h>
#include <bananui/widget.h>

struct bsRsvgHandleWrapper {
	RsvgHandle *hd;
};

static bScript *default_theme = NULL;
static GHashTable *style_cache = NULL;
static unsigned int global_token = UINT_MAX;

bScript *bDefaultTheme(bScript *theme)
{
	bScript *old_theme = default_theme;
	if (theme) {
		global_token--;
		default_theme = theme;
	} else if (!default_theme) {
		global_token--;
		default_theme = bLoadScript("themes/default.ipt");
	}
	if (!default_theme)
		g_error("Failed to load default theme!!!");
	return theme ? old_theme : default_theme;
}

static void invalidateChildren(bBox *box)
{
	bWidgetListItem *tmp;
	for (tmp = box->children.first; tmp; tmp = tmp->next) {
		if (tmp->type == B_WIDGET_TYPE_BOX) {
			tmp->box->computed.token++;
			invalidateChildren(tmp->box);
		}
	}
}

void bInvalidate(bBox *box)
{
	while (box && box->self) {
		box->computed.token++;
		box = box->self->owner;
	}
	box->computed.token++;
}

void bInvalidateWithChildren(bBox *box)
{
	bInvalidate(box);
	invalidateChildren(box);
}

void bBindStyle(bBox *box, const char *style_name)
{
	bStyleSpec *style;

	g_debug("%p <- style [%s]", box, style_name);

	bInvalidateWithChildren(box);

	if (style_cache) {
		style = g_hash_table_lookup(style_cache, style_name);
		if (style) {
			g_debug("Using cached style %s", style_name);
			box->style = style;
			return;
		}
	} else {
		style_cache = g_hash_table_new_full(g_str_hash,
						    g_str_equal,
						    g_free,
						    g_free);
	}
	if (bDefaultTheme(NULL)) {
		style = g_new(bStyleSpec, 1);
		bBuildStyle(default_theme, style, style_name);
		g_hash_table_insert(style_cache, g_strdup(style_name), style);
		g_debug("Loaded style %s", style_name);
		box->style = style;
	}
}

bBox *bCreateBox(const char *style_name)
{
	bBox *box = g_new0(bBox, 1);
	if (style_name)
		bBindStyle(box, style_name);
	return box;
}

bContent *bCreateEmpty(void)
{
	bContent *widget;
	widget = g_malloc(sizeof(bContent));
	if(!widget) return NULL;
	widget->self = NULL;
	widget->lt = NULL;
	widget->type = B_CONTENT_TYPE_EMPTY;
	widget->imgscale = 1;
	widget->svgwidth = 0;
	widget->svgheight = 0;
	widget->xoffset = 0;
	widget->yoffset = 0;
	widget->cursor = 0;
	widget->cursor_hscroll = 0;
	widget->cursor_visible = 0;
	widget->font = NULL;
	widget->data = NULL;
	return widget;
}

int bAddBox(bBox *box, bBox *child)
{
	bWidgetListItem *tmp;
	if(!child) return -1;
	tmp = g_malloc(sizeof(bWidgetListItem));
	if(!tmp) return -1;
	tmp->type = B_WIDGET_TYPE_BOX;
	tmp->box = child;
	tmp->prev = box->children.last;
	tmp->next = NULL;
	tmp->owner = box;
	if(box->children.last) box->children.last->next = tmp;
	else box->children.first = tmp;
	box->children.last = tmp;
	child->self = tmp;
	bInvalidate(box);
	return 0;
}

int bInsertBox(bBox *box, bBox *before)
{
	bWidgetListItem *tmp;
	if(!box || !before->self) return -1;
	tmp = g_malloc(sizeof(bWidgetListItem));
	if(!tmp) return -1;
	tmp->type = B_WIDGET_TYPE_BOX;
	tmp->box = box;
	tmp->prev = before->self->prev;
	tmp->next = before->self;
	tmp->owner = before->self->owner;
	if(tmp->prev) tmp->prev->next = tmp;
	else tmp->owner->children.first = tmp;
	tmp->next->prev = tmp;
	box->self = tmp;
	bInvalidate(tmp->owner);
	return 0;
}

int bAddContent(bBox *box, bContent *cont)
{
	bWidgetListItem *tmp;
	if(!cont) return -1;
	tmp = g_malloc(sizeof(bWidgetListItem));
	if(!tmp) return -1;
	tmp->type = B_WIDGET_TYPE_CONTENT;
	tmp->cont = cont;
	tmp->prev = box->children.last;
	tmp->next = NULL;
	tmp->owner = box;
	if(box->children.last) box->children.last->next = tmp;
	else box->children.first = tmp;
	box->children.last = tmp;
	cont->self = tmp;
	bInvalidate(box);
	return 0;
}

#ifdef BANANUI_LOG_WIDGETS
static const char *alignstr(bAlignment align)
{
	switch(align){
		case B_ALIGN_START:
			return "Align at Start";
		case B_ALIGN_CENTER:
			return "Align at Center";
		case B_ALIGN_END:
			return "Align at End";
	}
	return "Align unknown";
}
#endif

static void showText(cairo_t *cr, bContent *widget, bBox *box,
	double left, double top)
{
	double width, height;

	if(box->computed.horizontal){
		width = box->computed.width -
			(left - box->computed.left) -
			box->computed.style.pad_r;
		height = box->computed.height -
			box->computed.style.pad_t -
			box->computed.style.pad_b;
	}
	else {
		width = box->computed.width -
			box->computed.style.pad_l -
			box->computed.style.pad_r;
		height = box->computed.height -
			(top - box->computed.top) -
			box->computed.style.pad_b;
	}
	if(!widget->cursor_hscroll){
		pango_layout_set_width(widget->lt, width*PANGO_SCALE);
	}
	if(box->computed.style.clip){
		pango_layout_set_height(widget->lt, height*PANGO_SCALE);
	}
	cairo_set_source_rgba(cr,
		box->computed.style.content_color[0],
		box->computed.style.content_color[1],
		box->computed.style.content_color[2],
		box->computed.style.content_color[3]);
	if(widget->cursor_visible && widget->cursor <= strlen(widget->text)){
		PangoRectangle strong, weak;
		double strong_x, strong_y, strong_h, weak_x, weak_y;
		pango_layout_get_cursor_pos(widget->lt, widget->cursor,
			&strong, &weak);
		strong_x = strong.x / PANGO_SCALE;
		strong_y = strong.y / PANGO_SCALE;
		strong_h = strong.height / PANGO_SCALE;
		weak_x = weak.x / PANGO_SCALE;
		weak_y = weak.y / PANGO_SCALE;
		if(widget->cursor_hscroll){
			if(strong_x > (width - widget->xoffset)){
				widget->xoffset = width - strong_x;
			}
			if(strong_x < (-widget->xoffset)){
				widget->xoffset = -strong_x;
			}
		}
		if((strong_y + strong_h) > (height - widget->yoffset)){
			widget->yoffset = height - (strong_y + strong_h);
		}
		if(strong_y < (-widget->yoffset)){
			widget->yoffset = -strong_y;
		}
#if BANANUI_VERBOSE
		g_debug("Render cursor at %f%+f,%f%+f",
			strong_x, widget->xoffset, strong_y, widget->yoffset);
#endif
		cairo_rectangle(cr,
			left + widget->xoffset + strong_x,
			top + widget->yoffset + strong_y,
			1,
			strong_h);
		cairo_rectangle(cr,
			left + widget->xoffset + weak_x,
			top + widget->yoffset + weak_y,
			1,
			weak.height / PANGO_SCALE);
		cairo_fill(cr);
	}
	cairo_move_to(cr, left + widget->xoffset, top + widget->yoffset);
	pango_cairo_show_layout(cr, widget->lt);
}

static void recolorSVG(bBox *box, char **css)
{
	double r = box->computed.style.content_color[0],
		g = box->computed.style.content_color[1],
		b = box->computed.style.content_color[2],
		a = box->computed.style.content_color[3],
		wr = box->computed.style.warn_color[0],
		wg = box->computed.style.warn_color[1],
		wb = box->computed.style.warn_color[2],
		wa = box->computed.style.warn_color[3],
		er = box->computed.style.error_color[0],
		eg = box->computed.style.error_color[1],
		eb = box->computed.style.error_color[2],
		ea = box->computed.style.error_color[3],
		sr = box->computed.style.success_color[0],
		sg = box->computed.style.success_color[1],
		sb = box->computed.style.success_color[2],
		sa = box->computed.style.success_color[3];

	*css = g_strdup_printf(
		"rect, path {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n"
		".warning {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n"
		".error {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n"
		".success {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n",
		(int)(r * 255), (int)(g * 255), (int)(b * 255),
		(int)(a), (int)(a * 100) % 100,
		(int)(wr * 255), (int)(wg * 255), (int)(wb * 255),
		(int)(wa), (int)(wa * 100) % 100,
		(int)(er * 255), (int)(eg * 255), (int)(eb * 255),
		(int)(ea), (int)(ea * 100) % 100,
		(int)(sr * 255), (int)(sg * 255), (int)(sb * 255),
		(int)(sa), (int)(sa * 100) % 100);
#if BANANUI_LOG_WIDGETS
	fprintf(stderr, "Setting SVG style: %s\n", *css);
#endif
}

static void computeImageLayouts(bContent *img)
{
	if(img->type == B_CONTENT_TYPE_IMAGE){
		img->width = img->imgscale *
			cairo_image_surface_get_width(img->imgsurf);
		img->height = img->imgscale *
			cairo_image_surface_get_height(img->imgsurf);
	}
	else if(img->type == B_CONTENT_TYPE_IMAGE_SVG){
		RsvgRectangle out, vp;
		RsvgLength iw, ih;
		gboolean has_width, has_height, has_vb;

		rsvg_handle_get_intrinsic_dimensions(img->rsvghandle->hd,
			&has_width, &iw, &has_height, &ih,
			&has_vb, &out);
		if(has_vb){
			/* output rectangle is set, nothing to do */
		}
		else if(has_width && has_height && iw.unit == RSVG_UNIT_PX &&
			ih.unit == RSVG_UNIT_PX)
		{
			out.width = iw.length;
			out.height = ih.length;
		}
		else {
			rsvg_handle_set_dpi(img->rsvghandle->hd, 90);
			vp.x = vp.y = 0;
			vp.width = img->svgwidth;
			vp.height = img->svgheight;
			if(!rsvg_handle_get_geometry_for_layer(
				img->rsvghandle->hd,
				NULL, &vp, NULL, &out, NULL))
			{
				g_critical("Failed to get SVG geometry");
				img->width = img->svgwidth;
				img->height = img->svgheight;
				return;
			}
		}
		if(out.width > out.height){
			img->width = img->svgwidth;
			img->height = out.height / out.width * img->svgwidth;
		}
		else {
			img->width = out.width / out.height * img->svgheight;
			img->height = img->svgheight;
		}
#if BANANUI_LOG_WIDGETS
		fprintf(stderr, "SVG Width:desired=%f,actual=%f\n",
			img->svgwidth, img->width);
		fprintf(stderr, "SVG Height:desired=%f,actual=%f\n",
			img->svgheight, img->height);
#endif
	}
}

static void computeTextLayouts(bContent *text, cairo_t *cr)
{
	PangoRectangle extents;
	if(text->lt) g_object_unref(text->lt);
	text->lt = pango_cairo_create_layout(cr);

	assert(text->self && text->self->owner);

	if(text->font) pango_font_description_free(text->font);
	text->font = pango_font_description_new();
	pango_font_description_set_family(text->font,
		text->self->owner->computed.style.font_name);
	pango_font_description_set_weight(text->font,
		text->self->owner->computed.style.font_weight);
	pango_font_description_set_absolute_size(text->font,
		PANGO_SCALE * text->self->owner->computed.style.font_size);
	pango_font_description_set_style(text->font,
		(PangoStyle)text->self->owner->computed.style.font_style);

	pango_layout_set_font_description(text->lt,
		text->font);
	pango_layout_set_text(text->lt, text->text, -1);
	switch(text->self->owner->computed.style.content_alignment){
		case B_ALIGN_START:
			pango_layout_set_alignment(
				text->lt,
				PANGO_ALIGN_LEFT);
			break;
		case B_ALIGN_CENTER:
			pango_layout_set_alignment(
				text->lt,
				PANGO_ALIGN_CENTER);
			break;
		case B_ALIGN_END:
			pango_layout_set_alignment(
				text->lt,
				PANGO_ALIGN_RIGHT);
			break;
	}
	if(text->self->owner->computed.width >= 0 && !text->cursor_hscroll)
	{
		pango_layout_set_width(text->lt, PANGO_SCALE *
			(text->self->owner->computed.width -
			text->self->owner->computed.style.pad_l -
			text->self->owner->computed.style.pad_r));
	}
	if(text->self->owner->computed.style.wrap_mode ==
			B_WRAP_ELLIPSIZE)
	{
		pango_layout_set_ellipsize(text->lt,
			PANGO_ELLIPSIZE_END);
		pango_layout_set_height(text->lt, -1);
	}
	else if(text->self->owner->computed.style.wrap_mode ==
			B_WRAP_ELLIPSIZE_START)
	{
		pango_layout_set_ellipsize(text->lt,
			PANGO_ELLIPSIZE_START);
		pango_layout_set_height(text->lt, -1);
	}
	else if(text->self->owner->computed.height >= 0)
	{
		pango_layout_set_ellipsize(text->lt,
			PANGO_ELLIPSIZE_END);
		pango_layout_set_height(text->lt, PANGO_SCALE *
			(text->self->owner->computed.height -
			text->self->owner->computed.style.pad_t -
			text->self->owner->computed.style.pad_b));
	}

	pango_layout_set_wrap(text->lt,
		text->self->owner->computed.style.wrap_mode == B_WRAP_BREAK
		? PANGO_WRAP_WORD_CHAR : PANGO_WRAP_WORD);

	pango_layout_get_pixel_extents(text->lt, NULL, &extents);
	/*
	 * The width is always set first because text is horizontal
	 * and the height depends on the width due to wrapping
	 */
	if(text->width < 0){
		text->width = extents.width;
#if BANANUI_LOG_WIDGETS
		fprintf(stderr, "Set '%s' width %d\n",
			text->text, extents.width);
#endif
	}
	else {
		text->height = extents.height;

#if BANANUI_LOG_WIDGETS
		fprintf(stderr, "Set '%s' height %d (width is now %d)\n",
			text->text, extents.height, extents.width);
#endif
	}
}

static void cont_compute_width(bContent *cont, cairo_t *cr)
{
	if(cont->width >= 0){
		return;
	}
	if(cont->type == B_CONTENT_TYPE_TEXT){
		computeTextLayouts(cont, cr);
	}
	else if(cont->type == B_CONTENT_TYPE_IMAGE ||
		cont->type == B_CONTENT_TYPE_IMAGE_SVG)
	{
		computeImageLayouts(cont);
	}
	else {
		cont->width = 0;
	}
}

static void cont_compute_height(bContent *cont, cairo_t *cr)
{
	if(cont->height >= 0){
		return;
	}
	if(cont->type == B_CONTENT_TYPE_TEXT){
		computeTextLayouts(cont, cr);
	}
	else if(cont->type == B_CONTENT_TYPE_IMAGE ||
		cont->type == B_CONTENT_TYPE_IMAGE_SVG)
	{
		computeImageLayouts(cont);
	}
	else {
		cont->height = 0;
	}
}

static void box_compute_stretch_width(bBox *box, cairo_t *cr);
static void box_compute_stretch_height(bBox *box, cairo_t *cr);
static double box_compute_width(bBox *box, cairo_t *cr,
		double min, double max, double free);
static double box_compute_height(bBox *box, cairo_t *cr,
		double min, double max, double free);

/*
 * TODO: put width and height into an array and turn these macros into
 * functions to make them easier to read
 */

#define BOX_COMPUTE_STRETCH(dim, horz) \
	bWidgetListItem *tmp; \
	box->computed.used_##dim = 0; \
	box->computed.dim##_stretches = 0; \
	for(tmp = box->children.first; tmp; tmp = tmp->next){ \
		if(tmp->type == B_WIDGET_TYPE_BOX){ \
			double tmp_stretches = 0, tmp_used = 0; \
			if(tmp->box->computed.style.min_##dim < 0 || \
				tmp->box->computed.style.max_##dim < 0) \
			{ \
				tmp_stretches = 1; \
			} \
			else if(tmp->box->computed.style.min_##dim > 0){ \
				tmp_used = tmp->box->computed.style.min_##dim; \
				if(tmp_used > tmp->box->computed.style.max_##dim && \
					tmp->box->computed.style.max_##dim > 0) \
				{ \
					tmp_used = \
						tmp->box->computed.style.max_##dim; \
				} \
			} \
			else { \
				box_compute_stretch_##dim(tmp->box, cr); \
				tmp_used = tmp->box->computed.used_##dim; \
				tmp_stretches = tmp->box->computed.dim##_stretches; \
			} \
			if(horz){ \
				tmp_used += tmp->box->computed.style.mgn_l; \
				tmp_used += tmp->box->computed.style.mgn_r; \
			} \
			else { \
				tmp_used += tmp->box->computed.style.mgn_t; \
				tmp_used += tmp->box->computed.style.mgn_b; \
			} \
			if(horz == box->computed.horizontal){ \
				box->computed.used_##dim += tmp_used; \
				box->computed.dim##_stretches += tmp_stretches; \
			} \
			else { \
				if(tmp_used > box->computed.used_##dim){ \
					box->computed.used_##dim = tmp_used; \
				} \
				if(tmp_stretches > 0){ \
					box->computed.dim##_stretches = 1; \
				} \
			} \
		} \
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){ \
			cont_compute_##dim(tmp->cont, cr); \
			if(horz == box->computed.horizontal){ \
				box->computed.used_##dim += tmp->cont->dim; \
			} \
			else { \
				if(tmp->cont->dim > box->computed.used_##dim){ \
					box->computed.used_##dim = \
						tmp->cont->dim; \
				} \
			} \
		} \
	} \
	if(horz){ \
		box->computed.used_##dim += box->computed.style.pad_l; \
		box->computed.used_##dim += box->computed.style.pad_r; \
	} \
	else { \
		box->computed.used_##dim += box->computed.style.pad_t; \
		box->computed.used_##dim += box->computed.style.pad_b; \
	}

#define BOX_COMPUTE_REAL_STRETCH(dim, horz) \
	unsigned int old_num_stretches; \
	if(box->computed.dim##_stretches) do { \
		stretch = (free - box->computed.used_##dim) / \
			box->computed.dim##_stretches; \
		if(stretch <= 0){ \
			stretch = 0; \
			break; \
		} \
		old_num_stretches = box->computed.dim##_stretches; \
		for(tmp = box->children.first; tmp; tmp = tmp->next){ \
			if(tmp->type != B_WIDGET_TYPE_BOX) continue; \
			if(tmp->box->computed.dim >= 0) continue; \
			if(tmp->box->computed.style.max_##dim < 0 && \
				tmp->box->computed.style.min_##dim >= 0) \
			{ \
				double content_max = box_compute_##dim( \
					tmp->box, cr, \
					tmp->box->computed.style.min_##dim, \
					0, 0); \
				if(content_max < stretch){ \
					tmp->box->computed.dim = content_max; \
					box->computed.used_##dim += content_max; \
					box->computed.dim##_stretches -= 1; \
				} \
			} \
			if(tmp->box->computed.style.min_##dim < 0 && \
				tmp->box->computed.style.max_##dim >= 0) \
			{ \
				double content_min = box_compute_##dim( \
					tmp->box, cr, 0, \
					tmp->box->computed.style.max_##dim, \
					0); \
				if(content_min > stretch){ \
					tmp->box->computed.dim = content_min; \
					box->computed.used_##dim += content_min; \
					box->computed.dim##_stretches -= 1; \
				} \
			} \
		} \
	} while(box->computed.dim##_stretches != old_num_stretches);

#define BOX_COMPUTE_CHILD(dim, horz, dest, tag) \
	if(min2 != 0 && max2 != 0){ \
		box_compute_stretch_##dim(tmp->box, cr); \
	} \
	if(min2 < 0) min2 = stretch; \
	if(max2 < 0) max2 = stretch; \
	g_debug("[%p] " tag "/" #dim ": stretch: %f, free: %f", \
			tmp->box, stretch, free); \
	dest += tmp->box->computed.dim = box_compute_##dim( \
			tmp->box, cr, min2, max2, \
			min2 > 0 ? min2 : stretch > 0 ? stretch : free); \
	if(horz){ \
		dest += tmp->box->computed.style.mgn_l; \
		dest += tmp->box->computed.style.mgn_r; \
	} \
	else { \
		dest += tmp->box->computed.style.mgn_t; \
		dest += tmp->box->computed.style.mgn_b; \
	}

#define BOX_COMPUTE(dim, horz) \
	bWidgetListItem *tmp; \
	double size = 0, stretch = 0; \
	assert(max >= 0); \
	assert(min >= 0); \
	if (box->computed.dim > 0) { \
		return box->computed.dim; \
	} \
	if (horz == box->computed.horizontal) { \
		BOX_COMPUTE_REAL_STRETCH(dim, horz) \
	} \
	if (horz) { \
		free -= box->computed.style.pad_l; \
		free -= box->computed.style.pad_r; \
	} else { \
		free -= box->computed.style.pad_t; \
		free -= box->computed.style.pad_b; \
	} \
	for (tmp = box->children.first; tmp; tmp = tmp->next) { \
		if (tmp->type == B_WIDGET_TYPE_BOX) { \
			double min2 = tmp->box->computed.style.min_##dim; \
			double max2 = tmp->box->computed.style.max_##dim; \
			if (horz == box->computed.horizontal) { \
				BOX_COMPUTE_CHILD(dim, horz, size, "flow") \
			} \
			else { \
				double size2 = 0; \
				stretch = free; \
				if (horz) { \
					stretch -= tmp->box->computed.style.mgn_l; \
					stretch -= tmp->box->computed.style.mgn_r; \
				} \
				else { \
					stretch -= tmp->box->computed.style.mgn_t; \
					stretch -= tmp->box->computed.style.mgn_b; \
				} \
				if (stretch < 0) stretch = 0; \
				BOX_COMPUTE_CHILD(dim, horz, size2, "side") \
				if (size2 > size) size = size2; \
			} \
		} \
		else if (tmp->type == B_WIDGET_TYPE_CONTENT) { \
			cont_compute_##dim(tmp->cont, cr); \
			if (horz == box->computed.horizontal) { \
				size += tmp->cont->dim; \
			} \
			else { \
				if (tmp->cont->dim > size) { \
					size = tmp->cont->dim; \
				} \
			} \
		} \
	} \
	if (horz) { \
		size += box->computed.style.pad_l; \
		size += box->computed.style.pad_r; \
	} \
	else { \
		size += box->computed.style.pad_t; \
		size += box->computed.style.pad_b; \
	} \
	if (size < min) size = min; \
	if (size > max && max > 0) size = max; \
	return size;

static void box_compute_stretch_width(bBox *box, cairo_t *cr)
{
	BOX_COMPUTE_STRETCH(width, 1)
}

static void box_compute_stretch_height(bBox *box, cairo_t *cr)
{
	BOX_COMPUTE_STRETCH(height, 0)
}

static double box_compute_width(bBox *box, cairo_t *cr,
		double min, double max, double free)
{
	BOX_COMPUTE(width, 1)
}

static double box_compute_height(bBox *box, cairo_t *cr,
		double min, double max, double free)
{
	BOX_COMPUTE(height, 0)
}

static void resetLayout(bBox *box)
{
	bWidgetListItem *tmp;
	int i;

	if (box->computed.token == global_token)
		return;

#define SET_STYLE(_prop, _parent_inherit, _default) \
	if (box->custom_style && box->custom_style->override_ ## _prop) { \
		box->computed.style._prop = box->custom_style->items._prop; \
	} else if (box->style && box->style->override_ ## _prop) { \
		box->computed.style._prop = box->style->items._prop; \
	} else if (_parent_inherit && box->self && box->self->owner) { \
		box->computed.style._prop = \
			box->self->owner->computed.style._prop; \
	} else { \
		box->computed.style._prop = (_default); \
	}

#define SET_STYLE_COLOR(_prop, _parent_inherit, _r, _g, _b, _a) \
	if (box->custom_style && box->custom_style->override_ ## _prop) { \
		for (i = 0; i < 4; i++) { \
			box->computed.style._prop[i] = \
				box->custom_style->items._prop[i]; \
		} \
	} else if (box->style && box->style->override_ ## _prop) { \
		for (i = 0; i < 4; i++) { \
			box->computed.style._prop[i] = \
				box->style->items._prop[i]; \
		} \
	} else if (_parent_inherit && box->self && box->self->owner) { \
		for (i = 0; i < 4; i++) { \
			box->computed.style._prop[i] = \
				box->self->owner->computed.style._prop[i]; \
		} \
	} else { \
		box->computed.style._prop[0] = _r; \
		box->computed.style._prop[1] = _g; \
		box->computed.style._prop[2] = _b; \
		box->computed.style._prop[3] = _a; \
	}

	/*
	 * Strings have no override property, non-NULL values always override
	 */
#define SET_STYLE_STRING(_prop, _parent_inherit, _default) \
	if (box->custom_style && box->custom_style->items._prop) { \
		box->computed.style._prop = box->custom_style->items._prop; \
	} else if (box->style && box->style->items._prop) { \
		box->computed.style._prop = box->style->items._prop; \
	} else if (_parent_inherit && box->self && box->self->owner) { \
		box->computed.style._prop = \
			box->self->owner->computed.style._prop; \
	} else { \
		box->computed.style._prop = (_default); \
	}

	SET_STYLE_COLOR(background_color, 0, 0, 0, 0, 0)
	SET_STYLE_COLOR(content_color, 1, 0, 0, 0, 1)
	SET_STYLE_COLOR(warn_color, 1, 0, 0, 0, 1)
	SET_STYLE_COLOR(error_color, 1, 0, 0, 0, 1)
	SET_STYLE_COLOR(success_color, 1, 0, 0, 0, 1)
	SET_STYLE(mgn_l, 0, 0)
	SET_STYLE(mgn_t, 0, 0)
	SET_STYLE(mgn_b, 0, 0)
	SET_STYLE(mgn_r, 0, 0)
	SET_STYLE(pad_l, 0, 0)
	SET_STYLE(pad_t, 0, 0)
	SET_STYLE(pad_b, 0, 0)
	SET_STYLE(pad_r, 0, 0)
	SET_STYLE(rounding_radius, 0, 0)
	SET_STYLE(min_width, 0, 0)
	SET_STYLE(max_width, 0, 0)
	SET_STYLE(min_height, 0, 0)
	SET_STYLE(max_height, 0, 0)
	SET_STYLE(clip, 0, 0)
	SET_STYLE(alignment, 1, B_ALIGN_START)
	SET_STYLE(content_alignment, 1, B_ALIGN_START)
	SET_STYLE(wrap_mode, 1, B_WRAP_WORD)
	SET_STYLE(flow_direction, 1, B_DOWN)
	SET_STYLE(font_size, 1, 16)
	SET_STYLE(font_style, 1, B_FONT_NORMAL)
	SET_STYLE(font_weight, 1, 500)
	SET_STYLE_STRING(font_name, 1, "sans-serif")

#undef SET_STYLE_STRING
#undef SET_STYLE_COLOR
#undef SET_STYLE

	if (box->custom_style && box->custom_style->items.render_func) {
		box->computed.style.render_func =
			box->custom_style->items.render_func;
		box->computed.style.render_class =
			box->custom_style->items.render_class;
		box->computed.style.render_userdata =
			box->custom_style->items.render_userdata;
	} else if (box->style && box->style->items.render_func) {
		box->computed.style.render_func =
			box->style->items.render_func;
		box->computed.style.render_class =
			box->style->items.render_class;
		box->computed.style.render_userdata =
			box->style->items.render_userdata;
	} else {
		box->computed.style.render_func = NULL;
		box->computed.style.render_userdata = NULL;
	}

	box->computed.horizontal =
		box->computed.style.flow_direction == B_LTR ||
		box->computed.style.flow_direction == B_RTL;
	box->computed.width = box->computed.height = -1;
	box->computed.used_width = box->computed.used_height = -1;
	box->computed.width_stretches = box->computed.height_stretches = 0;
	box->computed.top = box->computed.left = 0;
	for(tmp = box->children.first; tmp; tmp = tmp->next){
		if(tmp->type == B_WIDGET_TYPE_BOX){
			resetLayout(tmp->box);
		}
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){
			tmp->cont->width = tmp->cont->height = -1;
		}
	}

	box->computed.token = global_token;
}

static void renderContent(cairo_t *cr, bBox *box, int indent,
		double top, double left, bContent *cont)
{
#if BANANUI_LOG_WIDGETS
	int i;
#endif

#if BANANUI_LOG_WIDGETS
	for(i = 0; i < indent; i++) fputc(' ', stderr);
#endif
	if(cont->type == B_CONTENT_TYPE_TEXT){
#if BANANUI_LOG_WIDGETS
		fprintf(stderr,
			"Text (%s) w=%lf h=%lf \"%s\"\n",
			alignstr(box->computed.style.content_alignment),
			cont->width, cont->height, cont->text);
#endif
		if(cont->lt){
			showText(cr, cont, box, left, top);
		}
	}
	else if(cont->type == B_CONTENT_TYPE_IMAGE){
#if BANANUI_LOG_WIDGETS
		fprintf(stderr,
			"Image (%s) w=%lf h=%lf\n",
			alignstr(box->computed.style.content_alignment),
			cont->width, cont->height);
#endif
		cairo_save(cr);
		cairo_scale(cr,
			cont->imgscale,
			cont->imgscale);
		cairo_set_source_surface(cr, cont->imgsurf,
			left / cont->imgscale,
			top / cont->imgscale);
		cairo_paint(cr);
		cairo_restore(cr);
	}
	else if(cont->type == B_CONTENT_TYPE_IMAGE_SVG){
		RsvgRectangle vp;
		char *style = NULL;
		GError *err = NULL;
#if BANANUI_LOG_WIDGETS
		fprintf(stderr,
			"SVG (%s) w=%lf h=%lf recolor=%d\n",
			alignstr(box->computed.style.content_alignment),
			cont->width, cont->height, cont->svgrecolor);
#endif
		if(cont->svgrecolor)
			recolorSVG(box, &style);
		if(style && !rsvg_handle_set_stylesheet(
			cont->rsvghandle->hd,
			(unsigned char*)style,
			strlen(style), &err))
		{
			if(err) g_critical("SVG set style failed: %s",
					err->message);
		}
		if(style) g_free(style);
		vp.y = top;
		vp.x = left;
		vp.width = cont->width;
		vp.height = cont->height;
		rsvg_handle_render_document(
			cont->rsvghandle->hd, cr,
			&vp, NULL);
	}
#if BANANUI_LOG_WIDGETS
	else {
		fprintf(stderr, "-- empty --\n");
	}
#endif
}

static void rounded_rectangle(cairo_t *cr, double x, double y,
		double width, double height, double radius)
{
	if(radius == 0){
		cairo_rectangle(cr, x, y, width, height);
		return;
	}
	/* See https://www.cairographics.org/samples/rounded_rectangle/ */
	cairo_new_sub_path(cr);
	cairo_arc(cr, x + width - radius, y + radius,
			radius, -0.5*M_PI, 0);
	cairo_arc(cr, x + width - radius, y + height - radius,
			radius, 0, 0.5*M_PI);
	cairo_arc(cr, x + radius, y + height - radius,
			radius, 0.5*M_PI, M_PI);
	cairo_arc(cr, x + radius, y + radius,
			radius, M_PI, 1.5*M_PI);
	cairo_close_path(cr);
}

static void renderBox_recursive(cairo_t *cr, bBox *box, int indent,
		double top, double left)
{
	bWidgetListItem *tmp;
#if BANANUI_LOG_WIDGETS
	int i;
#endif

	box->computed.top = top;
	box->computed.left = left;

#if BANANUI_LOG_WIDGETS
	for(i = 0; i < indent; i++) fputc(' ', stderr);
	fprintf(stderr, "Box (%s) w=%f,%u h=%f,%u at (%f,%f) {\n",
		alignstr(box->computed.style.alignment),
		box->computed.width, box->computed.width_stretches,
		box->computed.height, box->computed.height_stretches,
		top, left);
	indent++;
#endif

	rounded_rectangle(cr, left, top,
		box->computed.width, box->computed.height,
		box->computed.style.rounding_radius);

#if BANANUI_OUTLINE_BOX
	cairo_set_source_rgba(cr, 1, 0, 1, 1);
	cairo_stroke_preserve(cr);
#endif
	if(box->computed.style.clip){
		cairo_save(cr);
		cairo_clip_preserve(cr);
	}

	cairo_set_source_rgba(cr,
			box->computed.style.background_color[0],
			box->computed.style.background_color[1],
			box->computed.style.background_color[2],
			box->computed.style.background_color[3]);
	cairo_fill(cr);

	if (box->computed.style.render_func) {
		box->computed.style.render_func(
			cr, left, top,
			box->computed.width,
			box->computed.height,
			box->computed.style.render_class,
			box->computed.style.render_userdata);
	}

	switch(box->computed.style.flow_direction){
		case B_DOWN:
			top += box->computed.style.pad_t;
			left += box->computed.style.pad_l;
			break;
		case B_UP:
			top += box->computed.height -
				box->computed.style.pad_b;
			left += box->computed.style.pad_l;
			break;
		case B_LTR:
			top += box->computed.style.pad_t;
			left += box->computed.style.pad_l;
			break;
		case B_RTL:
			top += box->computed.style.pad_t;
			left += box->computed.width -
				box->computed.style.pad_r;
			break;
	}

	for(tmp = box->children.first; tmp; tmp = tmp->next)
	{
		if(tmp->type == B_WIDGET_TYPE_BOX){
			double xoffset = 0, yoffset = 0;
			if(tmp->box->computed.style.alignment !=
				B_ALIGN_START)
			{
				if(box->computed.horizontal)
					yoffset = box->computed.height -
						box->computed.style.pad_b -
						box->computed.style.pad_t -
						tmp->box->computed.height -
						tmp->box->computed.style.mgn_b -
						tmp->box->computed.style.mgn_t;
				else
					xoffset = box->computed.width -
						box->computed.style.pad_r -
						box->computed.style.pad_l -
						tmp->box->computed.width -
						tmp->box->computed.style.mgn_r -
						tmp->box->computed.style.mgn_l;
				if(tmp->box->computed.style.alignment ==
						B_ALIGN_CENTER)
				{
					if(box->computed.horizontal)
						yoffset /= 2;
					else
						xoffset /= 2;
				}
			}
			switch(box->computed.style.flow_direction){
				case B_DOWN:
					top += tmp->box->computed.style.mgn_t;
					xoffset += tmp->box->computed.style.mgn_l;
					break;
				case B_UP:
					top -= tmp->box->computed.style.mgn_b;
					top -= tmp->box->computed.height;
					xoffset += tmp->box->computed.style.mgn_l;
					break;
				case B_LTR:
					yoffset += tmp->box->computed.style.mgn_t;
					left += tmp->box->computed.style.mgn_l;
					break;
				case B_RTL:
					yoffset += tmp->box->computed.style.mgn_t;
					left -= tmp->box->computed.style.mgn_r;
					left -= tmp->box->computed.width;
					break;
			}
			renderBox_recursive(cr, tmp->box, indent,
					top+yoffset, left+xoffset);
			switch(box->computed.style.flow_direction){
				case B_DOWN:
					top += tmp->box->computed.height;
					top += tmp->box->computed.style.mgn_b;
					break;
				case B_UP:
					top -= tmp->box->computed.style.mgn_t;
					break;
				case B_LTR:
					left += tmp->box->computed.width;
					left += tmp->box->computed.style.mgn_r;
					break;
				case B_RTL:
					left -= tmp->box->computed.style.mgn_l;
					break;
			}
		}
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){
			cont_compute_width(tmp->cont, cr);
			cont_compute_height(tmp->cont, cr);
			if(box->computed.style.flow_direction == B_UP){
				top -= tmp->cont->height;
			}
			else if(box->computed.style.flow_direction == B_RTL){
				left -= tmp->cont->width;
			}
			renderContent(cr, box, indent, top, left, tmp->cont);
			if(box->computed.style.flow_direction == B_DOWN){
				top += tmp->cont->height;
			}
			else if(box->computed.style.flow_direction == B_LTR){
				left += tmp->cont->width;
			}
		}
#if BANANUI_LOG_WIDGETS
		else {
			for(i = 0; i < indent; i++) fputc(' ', stderr);
			fprintf(stderr, "!! Unknown widget !!\n");
		}
#endif
	}
	indent--;
#if BANANUI_LOG_WIDGETS
	for(i = 0; i < indent; i++) fputc(' ', stderr);
	fprintf(stderr, "}\n");
#endif
	if(box->computed.style.clip){
		cairo_restore(cr);
	}
}

void bRenderBox(cairo_t *cr, bBox *box, double top, double left)
{
	resetLayout(box);

	box_compute_stretch_width(box, cr);
	box->computed.width = box_compute_width(box, cr,
			box->computed.style.min_width,
			box->computed.style.max_width,
			box->computed.style.min_width);
	box_compute_stretch_height(box, cr);
	box->computed.height = box_compute_height(box, cr,
			box->computed.style.min_height,
			box->computed.style.max_height,
			box->computed.style.min_height);
	renderBox_recursive(cr, box, 0, top, left);
}

static void freeCont(bContent *cont)
{
	switch(cont->type){
		case B_CONTENT_TYPE_TEXT:
			if(cont->lt){
				g_object_unref(cont->lt);
				cont->lt = NULL;
			}
			if(cont->font){
				pango_font_description_free(cont->font);
				cont->font = NULL;
			}
			if(cont->text) g_free(cont->text);
			break;
		case B_CONTENT_TYPE_IMAGE:
			if(cont->imgsurf) cairo_surface_destroy(cont->imgsurf);
			break;
		case B_CONTENT_TYPE_IMAGE_SVG:
			if(cont->rsvghandle){
				if(cont->rsvghandle->hd){
					g_object_unref(cont->rsvghandle->hd);
				}
				g_free(cont->rsvghandle);
			}
			break;
		case B_CONTENT_TYPE_EMPTY:
			break;
	}
}

int bGetLineXAtOffset(bContent *cont, int cursor, int *line, int *x)
{
	if(cont->type != B_CONTENT_TYPE_TEXT || !cont->lt) return -1;
	pango_layout_index_to_line_x(cont->lt, cursor, 0, line, x);
	return 0;
}

int bGetOffsetAtLineX(bContent *cont, int line, int x)
{
	int offset, trailing;
	if(cont->type != B_CONTENT_TYPE_TEXT || !cont->lt) return -1;

	pango_layout_line_x_to_index(
			pango_layout_get_line(cont->lt, line),
			x, &offset, &trailing);

	while(trailing > 0){
		offset = g_utf8_find_next_char(cont->text + offset, NULL) - cont->text;
		trailing--;
	}

	return offset;
}

int bGetLastLine(bContent *cont)
{
	if(cont->type != B_CONTENT_TYPE_TEXT || !cont->lt) return -1;
	return pango_layout_get_line_count(cont->lt) - 1;
}

int bSetText(bContent *cont, const char *txt)
{
	char *tmp;
	tmp = g_strdup(txt);
	if(!tmp) return 0;
	freeCont(cont);
	cont->type = B_CONTENT_TYPE_TEXT;
	cont->text = tmp;
	if (cont->self)
		bInvalidate(cont->self->owner);
	return 0;
}

static int stringEndsWith(const char *str, const char *end, size_t len)
{
	size_t endlen;
	endlen = strlen(end);
	if(len < endlen) return 0;
	while(endlen){
		if(str[len - endlen] != *end) return 0;
		endlen--;
		end++;
	}
	return 1;
}

int bSetImagePath(bContent *cont, const char *filename)
{
	cairo_surface_t *tmp;
	size_t filenamelen;
	filenamelen = strlen(filename);
	if (cont->self)
		bInvalidate(cont->self->owner);
	if(stringEndsWith(filename, ".svg", filenamelen)){
		RsvgHandle *hd;
		freeCont(cont);
		cont->type = B_CONTENT_TYPE_IMAGE_SVG;
		cont->rsvghandle = g_malloc(sizeof(bRsvgHandleWrapper));
		if(!cont->rsvghandle) return 0;
		hd = rsvg_handle_new_from_file(filename, NULL);
		if(!hd){
			g_free(cont->rsvghandle);
			return 0;
		}
		cont->rsvghandle->hd = hd;
		cont->svgrecolor = 0;
	}
	else if(stringEndsWith(filename, ".png", filenamelen)){
		tmp = cairo_image_surface_create_from_png(filename);
		if(!tmp) return 0;
		freeCont(cont);
		cont->type = B_CONTENT_TYPE_IMAGE;
		cont->imgsurf = tmp;
	}
	else return -1;
	cont->imgscale = 1;
	return 0;
}

struct icon_theme {
	struct icon_directory *dirs;
	struct icon_theme *next;
};
struct icon_directory {
	char *path;
	int minsize, maxsize, scalable;
	struct icon_directory *next;
};

static struct icon_theme *iconThemes;
static const char *suffixes[] = {
	"svg",
	"png",
	NULL
};

static struct icon_theme *loadIconThemes(const char *name)
{
	GKeyFile *kf;
	const char * const *datadirs;
	const char * const *datadir;
	char **directories;
	char *homedir, *filename = NULL, *inherits;
	struct icon_theme *thm;
	struct icon_directory *tmp = NULL, **dest;
	int isFallback = (strcmp(name, "hicolor") == 0);
	thm = g_malloc(sizeof(struct icon_theme));
	if(!thm) return NULL;
	kf = g_key_file_new();
	if(!kf){
		g_free(thm);
		return NULL;
	}
	homedir = getenv("HOME");
	if(homedir){
		filename = g_strdup_printf("%s/.icons/%s/index.theme",
			homedir, name);
		g_debug("Checking for icontheme %s...", filename);
		if(!g_key_file_load_from_file(kf, filename,
			G_KEY_FILE_NONE, NULL))
		{
			g_clear_pointer(&filename, g_free);
		}
	}
	datadirs = g_get_system_data_dirs();
	if(!filename){
		for(datadir = datadirs; *datadir; datadir++){
			filename = g_strdup_printf("%s/icons/%s/index.theme",
				*datadir, name);
			g_debug("Checking for icontheme %s...", filename);
			if(g_key_file_load_from_file(kf, filename,
				G_KEY_FILE_NONE, NULL))
			{
				break;
			}
			g_clear_pointer(&filename, g_free);
		}
	}
	if(!filename){
		filename = g_strdup_printf("%s/icons/%s/index.theme",
			g_get_user_data_dir(), name);
		g_debug("Checking for icontheme %s...", filename);
		if(!g_key_file_load_from_file(kf, filename,
			G_KEY_FILE_NONE, NULL))
		{
			g_free(filename);
			g_key_file_unref(kf);
			return NULL;
		}
	}
	/* Leave out the /index.theme at the end so that we can append directory
	 * names */
	filename[strlen(filename) - strlen("/index.theme")] = '\0';
	inherits = g_key_file_get_string(kf, "Icon Theme", "Inherits", NULL);
	if(inherits){
		thm->next = loadIconThemes(inherits);
		g_free(inherits);
	}
	else if(!isFallback){
		thm->next = loadIconThemes("hicolor");
	}
	else {
		thm->next = NULL;
	}
	g_key_file_set_list_separator(kf, ',');
	directories = g_key_file_get_string_list(kf, "Icon Theme",
			"Directories", NULL, NULL);
	thm->dirs = NULL;
	if(directories){
		struct icon_directory *last = NULL;
		char **directory;
		for(directory = directories; *directory; directory++){
			char *type;
			int size;

			tmp = g_malloc(sizeof(struct icon_directory));
			if(!tmp) break;
			tmp->next = NULL;
			tmp->path = g_strdup_printf("%s/%s", filename,
					*directory);
			type = g_key_file_get_string(kf,
					*directory, "Type", NULL);
			size = g_key_file_get_integer(kf,
					*directory, "Size", NULL);
			tmp->minsize = g_key_file_get_integer(kf,
					*directory, "MinSize", NULL);
			tmp->maxsize = g_key_file_get_integer(kf,
					*directory, "MaxSize", NULL);
			if(tmp->minsize == 0) tmp->minsize = size;
			if(tmp->maxsize == 0) tmp->maxsize = size;
			if(type && 0 == strcmp(type, "Threshold")){
				tmp->minsize = size - 2;
				tmp->maxsize = size + 2;
			}
			if(last) last->next = tmp;
			if(!thm->dirs) thm->dirs = tmp;
			if(type){
				tmp->scalable = (0 == strcmp(type, "Scalable"));
				g_free(type);
			}
			else {
				tmp->scalable = 0;
			}
			last = tmp;
		}
		g_strfreev(directories);
	}
	dest = tmp ? &(tmp->next) : &(thm->dirs);

	/* Search for icons at the top level of the pixmaps / icons directory */
	/* The sizes are set to INT_MAX since they are the least preferred
	 * icons */
	if(homedir){
		(*dest) = g_malloc(sizeof(struct icon_directory));
		if(*dest){
			(*dest)->next = NULL;
			(*dest)->path = g_strdup_printf("%s/.icons", homedir);
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			dest = &((*dest)->next);
		}
	}
	if(isFallback){
		for(datadir = datadirs; *datadir; datadir++){
			(*dest) = g_malloc(sizeof(struct icon_directory));
			if(!*dest) break;
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/icons", *datadir);
			dest = &((*dest)->next);
			(*dest) = g_malloc(sizeof(struct icon_directory));
			if(!*dest) break;
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/pixmaps", *datadir);
			dest = &((*dest)->next);
		}
		(*dest) = g_malloc(sizeof(struct icon_directory));
		if(*dest){
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/icons",
					g_get_user_data_dir());
			dest = &((*dest)->next);
		}
		(*dest) = g_malloc(sizeof(struct icon_directory));
		if(*dest){
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/pixmaps",
					g_get_user_data_dir());
			dest = &((*dest)->next);
		}
	}
	g_free(filename);
	g_key_file_unref(kf);
	return thm;
}

static char *getIconFilename(const char *name, int size)
{
	struct icon_theme *thm;
	struct icon_directory *dir;
	char *best_path = NULL;
	int best_sizediff = INT_MAX, best_scalable = 0;
	if(!iconThemes)
		iconThemes = loadIconThemes("Adwaita");
	for(thm = iconThemes; thm; thm = thm->next){
		/* g_debug("Looking for %s in a theme", name); */
		for(dir = thm->dirs; dir; dir = dir->next){
			char *path;
			const char **suffix = suffixes;
			size_t maxpath;
			int sizediff, fd;
			/* g_debug("Looking for %s in %s",
				name, dir->path); */
			if(size < dir->minsize){
				sizediff = dir->minsize - size;
			}
			else if(size > dir->maxsize){
				sizediff = size - dir->maxsize;
			}
			else {
				sizediff = 0;
			}
			if(sizediff < best_sizediff ||
				(!dir->scalable && best_scalable &&
				 sizediff == best_sizediff))
			{
				maxpath = strlen(dir->path) + 1 +
					strlen(name) +
					strlen(".symbolic.png"
						/* the longest suffix */) + 1;
				path = g_malloc(maxpath);
				do {
					snprintf(path, maxpath, "%s/%s.%s",
						dir->path, name, *suffix);
					suffix++;
					fd = open(path, O_RDONLY);
				} while(fd < 0 && *suffix);
				if(fd > 0) {
					close(fd);
					best_scalable = dir->scalable;
					best_sizediff = sizediff;
					best_path = path;
				}
				else {
					g_free(path);
				}
			}
		}
	}
	return best_path;
}

int bSetIcon(bContent *cont, const char *name, int size)
{
	double width, height;
	if (cont->self)
		bInvalidate(cont->self->owner);
	if (name[0] != '/') {
		char *filename = getIconFilename(name, size);
		if (!filename)
			return -1;
		g_debug("Icon file: %s", filename);
		if (bSetImagePath(cont, filename) < 0) {
			g_free(filename);
			return -1;
		}
		g_free(filename);
	} else {
		g_debug("Icon file: %s", name);
		if (bSetImagePath(cont, name) < 0)
			return 0;
	}
	if (cont->type == B_CONTENT_TYPE_IMAGE) {
		width = cairo_image_surface_get_width(cont->imgsurf);
		height = cairo_image_surface_get_height(cont->imgsurf);
		cont->imgscale =
			((double)size) / (width > height ? width : height);
	} else if (cont->type == B_CONTENT_TYPE_IMAGE_SVG) {
		if (stringEndsWith(name, "-symbolic", strlen(name))) {
			cont->svgrecolor = 1;
		}
		cont->svgwidth = cont->svgheight = size;
	}
	return 0;
}

void bClearContent(bContent *cont)
{
	freeCont(cont);
	cont->type = B_CONTENT_TYPE_EMPTY;
}

bContent *bCreateText(const char *text)
{
	bContent *cont = bCreateEmpty();
	if(bSetText(cont, text) < 0){
		freeCont(cont);
		g_free(cont);
		return NULL;
	}
	return cont;
}

bContent *bCreateIcon(const char *name, int size)
{
	bContent *cont = bCreateEmpty();
	if(bSetIcon(cont, name, size) < 0){
		freeCont(cont);
		g_free(cont);
		return NULL;
	}
	return cont;
}

bContent *bCreateImage(const char *path)
{
	bContent *cont = bCreateEmpty();
	if(bSetImagePath(cont, path) < 0){
		freeCont(cont);
		g_free(cont);
		return NULL;
	}
	return cont;
}

void bDestroyBoxChildren(bBox *box)
{
	bWidgetListItem *tmp, *next;

	for(tmp = box->children.first; tmp; tmp = next)
	{
		if(tmp->type == B_WIDGET_TYPE_BOX){
			tmp->box->self = NULL;
			bDestroyBoxRecursive(tmp->box);
		}
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){
			freeCont(tmp->cont);
			g_free(tmp->cont);
		}
		next = tmp->next;
		g_free(tmp);
	}
	box->children.first = box->children.last = NULL;
}

void bDestroyBoxRecursive(bBox *box)
{
	bDestroyBoxChildren(box);
	bTriggerEvent(box->destroy, NULL);
	bClearEvent(&box->destroy);
	bClearEvent(&box->focusin);
	bClearEvent(&box->focusout);
	bClearEvent(&box->click);
	bClearEvent(&box->skleft);
	bClearEvent(&box->skright);
	if(box->self){
		bInvalidate(box->self->owner);
		/* remove from parent */
		if(box->self->next)
			box->self->next->prev = box->self->prev;
		else
			box->self->owner->children.last = box->self->prev;
		if(box->self->prev)
			box->self->prev->next = box->self->next;
		else
			box->self->owner->children.first = box->self->next;
		g_free(box->self);
	}
	g_free(box);
}
