/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bananui-window"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <glib.h>
#include <xkbcommon/xkbcommon.h>
#include <bananui/window.h>
#include <wayland-client.h>
#include "text-input-client-protocol.h"
#include "xdg-shell-client-protocol.h"

struct registry_interfaces {
	struct zwp_text_input_manager_v3 *text_input;
	struct wl_compositor *comp;
	struct wl_seat *seat;
	struct wl_shm *shm;
	struct xdg_wm_base *wm_base;
};

typedef struct bsWaylandWindow {
	struct wl_display *disp;
	struct wl_surface *wlsurf;
	struct wl_buffer *buffer;
	struct wl_shm_pool *pool;
	cairo_surface_t *surf;
	unsigned char *pixels;
	int shmfd;
	struct xdg_toplevel *toplevel;
	struct xdg_surface *xdgsurf;
	struct wl_keyboard *keyboard;
	struct xkb_keymap *keymap;
	struct xkb_context *xkbctx;
	struct wl_callback *frame_callback;
	int in_frame_callback;
	struct registry_interfaces reg;
	int configured_surface, configured_toplevel;
} bWaylandWindow;

static void keyboard_handle_keymap(void *data, struct wl_keyboard *kb,
		uint32_t format, int32_t fd, uint32_t size)
{
	bWindow *wnd = data;
	bWaylandWindow *ww = wnd->private;
	char *buf;
	if(format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1){
		g_critical("Incompatible keymap format");
		return;
	}
	buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	if(buf == MAP_FAILED){
		g_critical("Failed to map keymap: %s",
				strerror(errno));
		return;
	}
	xkb_keymap_unref(ww->keymap);
	ww->keymap = xkb_keymap_new_from_buffer(ww->xkbctx,
		buf, size-1, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	if(!ww->keymap){
		munmap(buf, size);
		g_critical("Failed to load keymap");
		return;
	}
	munmap(buf, size);
	wnd->xkbstate = xkb_state_new(ww->keymap);
	if(!wnd->xkbstate){
		xkb_keymap_unref(ww->keymap);
		g_critical("Failed to create keyboard state");
		return;
	}
	g_debug("Loaded keymap on %p", wnd);
}
static void keyboard_handle_enter(void *data, struct wl_keyboard *kb,
		uint32_t serial, struct wl_surface *surface,
		struct wl_array *keys)
{
	bWindow *wnd = data;
	bWaylandWindow *ww = wnd->private;
	if(ww->wlsurf != surface) return;
	g_debug("Keyboard entered %p", wnd);
	wnd->focused = 1;
	bTriggerEventSafe(wnd->focusin, &wnd->destroy_guard, wnd);
}
static void keyboard_handle_leave(void *data, struct wl_keyboard *kb,
		uint32_t serial, struct wl_surface *surface)
{
	bWindow *wnd = data;
	bWaylandWindow *ww = wnd->private;
	if(ww->wlsurf != surface) return;
	g_debug("Keyboard left %p", wnd);
	wnd->focused = 0;
	bStopKeyRepeat(wnd);
	bTriggerEventSafe(wnd->focusout, &wnd->destroy_guard, wnd);
}
static void keyboard_handle_key(void *data, struct wl_keyboard *kb,
		uint32_t serial, uint32_t time, uint32_t key, uint32_t state)
{
	bWindow *wnd = data;
	if(!wnd->focused) return;
	g_debug("Key %s on %p", state == WL_KEYBOARD_KEY_STATE_PRESSED
			? "down" : "up", wnd);
	bWindowKey(wnd, key + 8, state == WL_KEYBOARD_KEY_STATE_PRESSED
			? XKB_KEY_DOWN : XKB_KEY_UP);
}
static void keyboard_handle_modifiers(void *data, struct wl_keyboard *kb,
		uint32_t serial, uint32_t depressed,
		uint32_t latched, uint32_t locked, uint32_t group)
{
	bWindow *wnd = data;
	xkb_state_update_mask(wnd->xkbstate, depressed, latched, locked,
		group, 0, 0);
}
static void keyboard_handle_repeat_info(void *data, struct wl_keyboard *kb,
		int32_t rate, int32_t delay)
{
	bWindow *wnd = data;
	wnd->key_repeat_rate = rate;
	wnd->key_repeat_delay = delay;
}

static const struct wl_keyboard_listener keyboard_listener = {
	.keymap = keyboard_handle_keymap,
	.enter = keyboard_handle_enter,
	.leave = keyboard_handle_leave,
	.key = keyboard_handle_key,
	.modifiers = keyboard_handle_modifiers,
	.repeat_info = keyboard_handle_repeat_info,
};

static void xdg_wm_base_handle_ping(void *data, struct xdg_wm_base *shell,
	uint32_t serial)
{
	xdg_wm_base_pong(shell, serial);
}
static void xdg_surface_handle_configure(void *data,
	struct xdg_surface *surface, uint32_t serial)
{
	bWindow *wnd = data;
	bWaylandWindow *ww = wnd->private;
	xdg_surface_ack_configure(surface, serial);
	ww->configured_surface = 1;
}
static void shm_randname(char *buf)
{
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	long r = ts.tv_nsec;
	for (int i = 0; i < 6; ++i) {
		buf[i] = 'A'+(r&15)+(r&16)*2;
		r >>= 5;
	}
}

static int create_shm_file(void)
{
	int retries = 100;
	do {
		char name[] = "/wl_shm-XXXXXX";
		shm_randname(name + sizeof(name) - 7);
		--retries;
		int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
		if (fd >= 0) {
			shm_unlink(name);
			return fd;
		}
	} while (retries > 0 && errno == EEXIST);
	g_critical("shm create failed: %s", strerror(errno));
	return -1;
}

static int allocate_shm(size_t size)
{
	int fd = create_shm_file();
	if (fd < 0)
		return -1;
	int ret;
	do {
		ret = ftruncate(fd, size);
	} while (ret < 0 && errno == EINTR);
	if (ret < 0) {
		close(fd);
		return -1;
	}
	return fd;
}
static void xdg_toplevel_handle_configure(void *data,
	struct xdg_toplevel *toplevel, int32_t width, int32_t height,
	struct wl_array *states)
{
	bWindow *wnd = data;
	bWaylandWindow *ww = wnd->private;
	if(ww->configured_toplevel) return;
	if(!height) height = 290;
	if(!width) width = 240;
	wnd->height = height;
	wnd->width = width;
	ww->configured_toplevel = 1;
	g_debug("Window size: %d x %d", width, height);
}
static void xdg_toplevel_handle_close(void *data, struct xdg_toplevel *toplevel)
{
	bWindow *wnd = data;
	wnd->closing = 1;
}

static const struct xdg_wm_base_listener wm_base_listener = {
	.ping = xdg_wm_base_handle_ping,
};
static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};
static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.configure = xdg_toplevel_handle_configure,
	.close = xdg_toplevel_handle_close,
};

static void registry_handle_global(void *data, struct wl_registry *reg,
	uint32_t name, const char *interface, uint32_t version)
{
	struct registry_interfaces *interfaces = data;
	if(strcmp(interface, "wl_compositor") == 0){
		interfaces->comp = wl_registry_bind(reg, name,
			&wl_compositor_interface, version > 4 ? 4 : version);
	}
	else if(strcmp(interface, "wl_seat") == 0){
		interfaces->seat = wl_registry_bind(reg, name,
			&wl_seat_interface, version);
	}
	else if(strcmp(interface, "xdg_wm_base") == 0){
		interfaces->wm_base = wl_registry_bind(reg, name,
			&xdg_wm_base_interface, 1);
		xdg_wm_base_add_listener(interfaces->wm_base,
			&wm_base_listener, NULL);
	}
	else if(strcmp(interface, "wl_shm") == 0){
		interfaces->shm = wl_registry_bind(reg, name,
			&wl_shm_interface, 1);
	}
	else if(strcmp(interface, "zwp_text_input_manager_v3") == 0){
		interfaces->text_input = wl_registry_bind(reg, name,
			&zwp_text_input_manager_v3_interface, version);
	}
}
static void registry_handle_global_remove(void *data, struct wl_registry *reg,
	uint32_t name)
{
}

static const struct wl_registry_listener listeners = {
	.global = registry_handle_global,
	.global_remove = registry_handle_global_remove,
};

static void handle_surface_frame(void *data, struct wl_callback *cb,
		uint32_t callback_data)
{
	bWindow *wnd = data;
	bWaylandWindow *ww = wnd->private;
	wl_callback_destroy(cb);
	ww->frame_callback = NULL;
	ww->in_frame_callback = 1;

	/* Clear window to make it transparent */
	cairo_save(wnd->renderer);
	cairo_set_operator(wnd->renderer, CAIRO_OPERATOR_CLEAR);
	cairo_set_source_rgba(wnd->renderer, 0, 0, 0, 0);
	cairo_paint(wnd->renderer);
	cairo_restore(wnd->renderer);

	/* Request re-render */
	if(bTriggerEventSafe(wnd->redraw, &wnd->destroy_guard, wnd) < 0)
		return;

	/* Commit wayland surface */
	wl_surface_attach(ww->wlsurf, ww->buffer, 0, 0);
	wl_surface_damage(ww->wlsurf, 0, 0, wnd->width, wnd->height);
	wl_surface_commit(ww->wlsurf);
	wl_display_flush(ww->disp);

	ww->in_frame_callback = 0;
}

static const struct wl_callback_listener frame_listener = {
	.done = handle_surface_frame,
};

static void wayland_wnd_destroy(bWindow *wnd)
{
	bWaylandWindow *ww = wnd->private;
	int stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32,
				wnd->width);
	if(ww->frame_callback) wl_callback_destroy(ww->frame_callback);
	if(ww->keyboard) wl_keyboard_destroy(ww->keyboard);
	if(wnd->renderer) cairo_destroy(wnd->renderer);
	if(ww->surf) cairo_surface_destroy(ww->surf);
	if(ww->buffer) wl_buffer_destroy(ww->buffer);
	if(ww->pool) wl_shm_pool_destroy(ww->pool);
	if(ww->pixels) munmap(ww->pixels, stride * wnd->height);
	if(ww->toplevel) xdg_toplevel_destroy(ww->toplevel);
	if(ww->xdgsurf) xdg_surface_destroy(ww->xdgsurf);
	if(ww->wlsurf) wl_surface_destroy(ww->wlsurf);
	if(ww->shmfd >= 0) close(ww->shmfd);
	if(ww->reg.text_input)
		zwp_text_input_manager_v3_destroy(ww->reg.text_input);
	if(ww->reg.seat) wl_seat_destroy(ww->reg.seat);
	if(ww->reg.shm) wl_shm_destroy(ww->reg.shm);
	if(ww->reg.wm_base) xdg_wm_base_destroy(ww->reg.wm_base);
	if(ww->disp) wl_display_flush(ww->disp);
	g_free(ww);
	wnd->private = NULL;
}

static int wayland_wnd_init(bWindow *wnd, const char *title,
		const char *app_id, int fullscreen, int w, int h)
{
	int stride;
	struct wl_registry *reg;
	bWaylandWindow *ww = g_malloc0(sizeof(bWaylandWindow));
	if(!ww){
		g_critical("Failed to allocate memory for window");
		return -1;
	}
	wnd->private = ww;

	ww->disp = bGetWaylandDisplay(wnd->disp);
	if(!ww->disp){
		g_critical("Display is not a Wayland display");
		return -1;
	}

	ww->xkbctx = xkb_context_new(0);
	if(!ww->xkbctx){
		g_critical("Failed to create XKB context");
		return -1;
	}

	reg = wl_display_get_registry(ww->disp);
	wl_registry_add_listener(reg, &listeners, &ww->reg);
	wl_display_roundtrip(ww->disp);
	wl_registry_destroy(reg);
	if(!ww->reg.comp){
		g_critical("No wl_compositor interface found.");
		return -1;
	}
	if(!ww->reg.wm_base){
		g_critical("No XDG shell (xdg_wm_base) found.");
		return -1;
	}
	if(!ww->reg.seat){
		g_critical("No seat available.");
		return -1;
	}
	if(!ww->reg.shm){
		g_warning("No wl_shm found.");
		return -1;
	}
	if(!ww->reg.text_input){
		g_warning("No text input method.");
	}

	ww->keyboard = wl_seat_get_keyboard(ww->reg.seat);
	if(ww->keyboard){
		wl_keyboard_add_listener(ww->keyboard,
			&keyboard_listener, wnd);
	}

	ww->wlsurf = wl_compositor_create_surface(ww->reg.comp);
	if(!ww->wlsurf) return -1;

	ww->xdgsurf =
		xdg_wm_base_get_xdg_surface(ww->reg.wm_base, ww->wlsurf);
	if(!ww->xdgsurf) return -1;
	xdg_surface_add_listener(ww->xdgsurf, &xdg_surface_listener, wnd);
	ww->toplevel = xdg_surface_get_toplevel(ww->xdgsurf);
	if(!ww->toplevel) return -1;
	xdg_toplevel_add_listener(ww->toplevel, &xdg_toplevel_listener, wnd);
	xdg_toplevel_set_title(ww->toplevel, title);
	xdg_toplevel_set_app_id(ww->toplevel, app_id);
	wl_surface_commit(ww->wlsurf);
	if(fullscreen) xdg_toplevel_set_fullscreen(ww->toplevel, NULL);

	while(!(ww->configured_toplevel && ww->configured_surface) &&
		wl_display_dispatch(ww->disp) >= 0);

	if(w > 0) wnd->width = w;
	else wnd->width += w;
	if(h > 0) wnd->height = h;
	else wnd->height += h;

	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32,
			wnd->width);
	ww->shmfd = allocate_shm(stride * wnd->height);
	if(ww->shmfd < 0) return -1;
	ww->pixels = mmap(NULL, stride * wnd->height,
		PROT_READ | PROT_WRITE,
		MAP_SHARED, ww->shmfd, 0);
	if(ww->pixels == MAP_FAILED){
		g_critical("mmap: %s", strerror(errno));
		ww->pixels = NULL;
		return -1;
	}
	ww->pool = wl_shm_create_pool(ww->reg.shm, ww->shmfd,
			stride * wnd->height);
	if(!ww->pool) return -1;
	ww->buffer = wl_shm_pool_create_buffer(ww->pool, 0,
			wnd->width, wnd->height, stride,
			WL_SHM_FORMAT_ARGB8888);
	if(!ww->buffer) return -1;
	ww->surf = cairo_image_surface_create_for_data(ww->pixels,
			CAIRO_FORMAT_ARGB32, wnd->width, wnd->height,
			stride);
	if(!ww->surf) return -1;
	wnd->renderer = cairo_create(ww->surf);
	if(!wnd->renderer) return -1;
	wl_surface_attach(ww->wlsurf, ww->buffer, 0, 0);
	wl_surface_damage(ww->wlsurf, 0, 0, wnd->width, wnd->height);
	wl_surface_commit(ww->wlsurf);
	return 0;
}

static void wayland_wnd_request_frame(bWindow *wnd)
{
	bWaylandWindow *ww = wnd->private;
	if(ww->frame_callback) return;
	ww->frame_callback = wl_surface_frame(ww->wlsurf);
	wl_callback_add_listener(ww->frame_callback, &frame_listener, wnd);
	if(!ww->in_frame_callback){
		wl_surface_commit(ww->wlsurf);
		wl_display_flush(ww->disp);
	}
}

static bInputMethod *wayland_wnd_get_input_method(bWindow *wnd)
{
	struct zwp_text_input_v3 *text_input;
	bWaylandWindow *ww = wnd->private;
	if(!ww->reg.text_input) return NULL;
	text_input = zwp_text_input_manager_v3_get_text_input(
				ww->reg.text_input,
				ww->reg.seat);
	return bCreateWaylandInputMethod(ww->disp, text_input);
}

static const bWindowOps wayland_wnd_ops = {
	.init = wayland_wnd_init,
	.destroy = wayland_wnd_destroy,
	.request_frame = wayland_wnd_request_frame,
	.get_input_method = wayland_wnd_get_input_method
};

static int dispatch_key(bWindow *wnd, enum xkb_key_direction dir,
		int is_repeat)
{
	bKeyEvent ev = {
		.wnd = wnd,
		.keycode = wnd->keycode,
		.is_repeat = is_repeat,
	};
	const xkb_keysym_t *keysyms;
	int i, ret, numsyms;
	numsyms = xkb_state_key_get_syms(wnd->xkbstate, wnd->keycode,
			&keysyms);
	for(i = 0; i < numsyms; i++){
		ev.seqnum = i;
		ev.sym = keysyms[i];
		if(dir == XKB_KEY_UP){
			ret = bTriggerEventSafe(wnd->keyup,
					&wnd->destroy_guard, &ev);
		}
		else if(dir == XKB_KEY_DOWN){
			ret = bTriggerEventSafe(wnd->keydown,
					&wnd->destroy_guard, &ev);
		}
		/* if window is destroyed */
		if(ret < 0) return 0;
	}
	if(dir == XKB_KEY_UP || !xkb_keymap_key_repeats(
				xkb_state_get_keymap(wnd->xkbstate),
				wnd->keycode))
	{
		return 0;
	}
	return 1;
}

void bStopKeyRepeat(bWindow *wnd)
{
	if(wnd->timeout_id){
		g_source_remove(wnd->timeout_id);
		wnd->timeout_id = 0;
	}
}

static gboolean key_repeat_repeat(gpointer data)
{
	bWindow *wnd = data;
	g_debug("Key repeat triggered on %p", wnd);
	if(dispatch_key(wnd, XKB_KEY_DOWN, 1)){
		return G_SOURCE_CONTINUE;
	}
	else {
		wnd->timeout_id = 0;
		return G_SOURCE_REMOVE;
	}
}

static gboolean key_repeat_first(gpointer data)
{
	bWindow *wnd = data;
	g_debug("First key repeat triggered on %p", wnd);
	if(dispatch_key(wnd, XKB_KEY_DOWN, 1)){
		wnd->timeout_id = g_timeout_add(wnd->key_repeat_rate,
				key_repeat_repeat,
				wnd);
	}
	else {
		wnd->timeout_id = 0;
	}
	return G_SOURCE_REMOVE;
}

void bWindowKey(bWindow *wnd, xkb_keycode_t kc, enum xkb_key_direction dir)
{
	wnd->keycode = kc;
	bStopKeyRepeat(wnd);
	if(dispatch_key(wnd, dir, 0)){
		wnd->timeout_id = g_timeout_add(wnd->key_repeat_delay,
				key_repeat_first,
				wnd);
	}
}

bWindow *bCreateWindow2(const char *title, const char *app_id,
		int fullscreen, int width, int height, bDisplay *disp,
		const bWindowOps *ops)
{
	bWindow *wnd = g_malloc0(sizeof(bWindow));
	wnd->disp = disp ?: bGetDefaultDisplay();

	wnd->ops = ops ?: &wayland_wnd_ops;
	if(wnd->ops->init(wnd, title, app_id, fullscreen, width, height) < 0){
		bDestroyWindow(wnd);
		return NULL;
	}
	g_debug("Create window %p", wnd);
	return wnd;
}

bWindow *bCreateWindow(const char *title, const char *app_id)
{
	return bCreateWindow2(title, app_id, 0, 0, 0, NULL, NULL);
}

bWindow *bCreateFullscreenWindow(const char *title, const char *app_id)
{
	return bCreateWindow2(title, app_id, 1, 0, 0, NULL, NULL);
}

bWindow *bCreateSizedWindow(const char *title, const char *app_id,
		int width, int height)
{
	return bCreateWindow2(title, app_id, 0, width, height, NULL, NULL);
}

void bDestroyWindow(bWindow *wnd)
{
	bEventHandler *destroy = wnd->destroy_guard;
	g_debug("Destroy window %p", wnd);
	if (wnd->private)
		wnd->ops->destroy(wnd);
	if (wnd->timeout_id)
		g_source_remove(wnd->timeout_id);
	bClearEvent(&wnd->keydown);
	bClearEvent(&wnd->keyup);
	bClearEvent(&wnd->fullscreenchange);
	bClearEvent(&wnd->focusin);
	bClearEvent(&wnd->focusout);
	bClearEvent(&wnd->redraw);
	g_free(wnd);
	bTriggerEvent(destroy, destroy);
}

void bRequestFrame(bWindow *wnd)
{
	wnd->ops->request_frame(wnd);
}

bInputMethod *bGetInputMethod(bWindow *wnd)
{
	return wnd->ops->get_input_method(wnd);
}
