/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <bananui/settings.h>

#define FALLBACK_SETTINGS_PATH "/etc/bananian-device.ini"

struct bsSettings {
	char *userpath;
	GKeyFile *devicefile;
	GKeyFile *userfile;
};

static char *getDeviceSettingsPath(GError **err)
{
	char *compatibles, *compatible, *path;
	size_t length;
	if(!g_file_get_contents("/proc/device-tree/compatible",
				&compatibles,
				&length,
				err))
	{
		return NULL;
	}
	compatible = g_strndup(compatibles, length);
	g_free(compatibles);
	path = g_strdup_printf("/usr/share/bananui/devices/%s.ini",
			compatible);
	g_free(compatible);
	return path;
}

bSettings *bCreateSettingsManager(void)
{
	GError *err = NULL;
	char *devicepath;
	bSettings *mgr;
	mgr = g_malloc0(sizeof(bSettings));
	if(!mgr) return NULL;
	mgr->userpath = g_strdup_printf("%s/bananui/settings.ini",
			g_get_user_config_dir());
	mgr->userfile = g_key_file_new();
	if(!mgr->userfile) goto free_path;
	if(!g_key_file_load_from_file(mgr->userfile, mgr->userpath,
			G_KEY_FILE_NONE, &err))
	{
#ifdef BANANUI_VERBOSE
		fprintf(stderr, "wbananui: load user settings failed: %s\n",
				err->message);
#endif
		g_clear_error(&err);
	}
	mgr->devicefile = g_key_file_new();
	if(!mgr->devicefile) goto free_userfile;
	devicepath = getDeviceSettingsPath(&err);
	if(!devicepath || !g_key_file_load_from_file(mgr->devicefile,
				devicepath, G_KEY_FILE_NONE, &err))
	{
		fprintf(stderr, "wbananui: load device settings %s: %s\n",
				devicepath, err->message);
		g_clear_error(&err);
		if(!g_key_file_load_from_file(mgr->devicefile,
				FALLBACK_SETTINGS_PATH,
				G_KEY_FILE_NONE, &err))
		{
			fprintf(stderr,
				"wbananui: settings fallback failed: %s\n",
				err->message);
			g_clear_error(&err);
		}
	}
	g_free(devicepath);
	return mgr;
free_userfile:
	g_key_file_free(mgr->userfile);
free_path:
	g_free(mgr->userpath);
	return NULL;
}

void bDestroySettingsManager(bSettings *mgr)
{
	if(mgr){
		g_key_file_free(mgr->devicefile);
		g_key_file_free(mgr->userfile);
		g_free(mgr->userpath);
	}
}

char *bGetSettingString(bSettings *mgr, const char *name, const char *defval)
{
	if(mgr){
		GError *err = NULL;
		char *ret;
		ret = g_key_file_get_string(mgr->devicefile, "Settings",
				name, &err);
		if(!err) return ret;
		g_clear_error(&err);
		ret = g_key_file_get_string(mgr->userfile, "Settings",
				name, &err);
		if(!err) return ret;
#ifdef BANANUI_VERBOSE
		fprintf(stderr, "wbananui: get setting %s failed: %s\n",
				name, err->message);
#endif
		g_error_free(err);
	}
	return g_strdup(defval);
}

int bGetSettingInt(bSettings *mgr, const char *name, int defval)
{
	if(mgr){
		GError *err = NULL;
		int ret;
		ret = g_key_file_get_integer(mgr->devicefile, "Settings",
				name, &err);
		if(!err) return ret;
		g_clear_error(&err);
		ret = g_key_file_get_integer(mgr->userfile, "Settings",
				name, &err);
		if(!err) return ret;
#ifdef BANANUI_VERBOSE
		fprintf(stderr, "wbananui: get integer setting %s failed: %s\n",
				name, err->message);
#endif
		g_error_free(err);
	}
	return defval;
}

int bSetSettingString(bSettings *mgr, const char *name, const char *val)
{
	if(mgr){
		GError *err = NULL;
		g_key_file_set_string(mgr->userfile, "Settings", name, val);
		g_key_file_save_to_file(mgr->userfile, mgr->userpath, &err);
		if(!err) return 0;
		fprintf(stderr, "wbananui: save after setting %s failed: %s\n",
				name, err->message);
		g_error_free(err);
	}
	return -1;
}

int bSetSettingInt(bSettings *mgr, const char *name, int val)
{
	if(mgr){
		GError *err = NULL;
		g_key_file_set_integer(mgr->userfile, "Settings", name, val);
		g_key_file_save_to_file(mgr->userfile, mgr->userpath, &err);
		if(!err) return 0;
		fprintf(stderr, "wbananui: save after setting %s failed: %s\n",
				name, err->message);
		g_error_free(err);
	}
	return -1;
}
