/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <bananui/softkey.h>

bSoftkeyPanel *bCreateSoftkeyPanel(void)
{
	bSoftkeyPanel *panel;
	bBox *boxes[3] = { 0 };
	panel = g_new0(bSoftkeyPanel, 1);
	panel->card = bBuildCard(bDefaultTheme(NULL), "softkeys", 3, boxes);
	if (!panel->card) {
		g_free(panel);
		return NULL;
	}
	if (boxes[0])
		bAddContent(boxes[0], panel->lcont = bCreateEmpty());
	if (boxes[1])
		bAddContent(boxes[1], panel->ccont = bCreateEmpty());
	if (boxes[2])
		bAddContent(boxes[2], panel->rcont = bCreateEmpty());
	return panel;
}

void bSetSoftkeyText(bSoftkeyPanel *sk,
	const char *l, const char *c, const char *r)
{
	if(l) bSetText(sk->lcont, l);
	if(c) bSetText(sk->ccont, c);
	if(r) bSetText(sk->rcont, r);
	bInvalidateWithChildren(sk->card->box);
}

void bDestroySoftkeyPanel(bSoftkeyPanel *sk)
{
	if(!sk) return;
	bDestroyCard(sk->card);
	g_free(sk);
}
