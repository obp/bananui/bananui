/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <bananui/widget.h>
#include <bananui/button.h>

static int focus_handler(void *param, void *data)
{
	bButtonWidget *btn = data;
	bBindStyle(btn->box, btn->focused_style_name);
	btn->focused = 1;
	return 1;
}

static int unfocus_handler(void *param, void *data)
{
	bButtonWidget *btn = data;
	bBindStyle(btn->box, btn->style_name);
	btn->focused = 0;
	return 1;
}

static int btn_destroy_handler(void *param, void *data)
{
	bButtonWidget *btn = data;
	g_free(btn->style_name);
	g_free(btn->focused_style_name);
	g_free(btn);
	return 1;
}

bButtonWidget *bCreateButtonWidget(const char *text,
				   const char *style_name,
				   bButtonFunc func)
{
	bButtonWidget *btn;
	bBox *boxes[3] = { 0 };
	btn = g_new0(bButtonWidget, 1);
	btn->func = func;
	btn->box = bBuildBox(bDefaultTheme(NULL), style_name, 3, boxes);
	if (!btn->box) {
		g_free(btn);
		return NULL;
	}
	btn->box->focusable = 1;
	btn->focused_style_name = g_strdup_printf("%s :focus", style_name);
	btn->style_name = g_strdup(style_name);
	if (boxes[0]) {
		bAddContent(boxes[0], btn->icon = bCreateEmpty());
		if(func == B_BUTTON_FUNC_RADIO){
			bSetIcon(btn->icon, "radio-symbolic", 24);
		}
		else if(func == B_BUTTON_FUNC_CHECKBOX){
			bSetIcon(btn->icon, "checkbox-symbolic", 24);
		}
	}
	if (boxes[1])
		bAddContent(boxes[1], btn->text = bCreateText(text));
	if (boxes[2])
		bAddContent(boxes[2], btn->subtitle = bCreateEmpty());
	bRegisterEventHandler(&btn->box->focusin, focus_handler, btn);
	bRegisterEventHandler(&btn->box->focusout, unfocus_handler, btn);
	bRegisterEventHandler(&btn->box->destroy, btn_destroy_handler, btn);
	return btn;
}

void bSetButtonText(bButtonWidget *btn, const char *text)
{
	if (btn->text)
		bSetText(btn->text, text);
}

void bSetButtonSubtitle(bButtonWidget *btn, const char *subtitle)
{
	if (btn->subtitle)
		bSetText(btn->subtitle, subtitle);
}

void bSetButtonIcon(bButtonWidget *btn, const char *icon, int size)
{
	if (btn->icon)
		bSetIcon(btn->icon, icon, size);
}

void bSetButtonState(bButtonWidget *btn, int state)
{
	if (btn->state == state)
		return;
	btn->state = state;
	if (!btn->icon)
		return;
	if(btn->func == B_BUTTON_FUNC_RADIO && state == 0)
		bSetIcon(btn->icon, "radio-symbolic", 24);
	else if(btn->func == B_BUTTON_FUNC_RADIO && state == 1)
		bSetIcon(btn->icon, "radio-checked-symbolic", 24);
	else if(btn->func == B_BUTTON_FUNC_CHECKBOX && state == 0)
		bSetIcon(btn->icon, "checkbox-symbolic", 24);
	else if(btn->func == B_BUTTON_FUNC_CHECKBOX && state == 1)
		bSetIcon(btn->icon, "checkbox-checked-symbolic", 24);
}
