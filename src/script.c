/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bananui-script"

#include <stdio.h>
#include <glib.h>
#include <bananui/script.h>
#include <bananui/widget.h>
#include <libigi/compiler.h>
#include <libigi/vm.h>

struct bsScript {
	char *path;
	FILE *stream;
	IgelProgram *prg;
	bStyleSpec *style;
	bBox *box, **refs;
	uint16_t n_refs;
	cairo_t *renderer;
	cairo_surface_t **resources;
	uint32_t n_resources, resources_size;
	unsigned int cairo_saves;
	double width, height;
};

#define PROPS \
	PROP_DOUBLE(min_width) \
	PROP_DOUBLE(max_width) \
	PROP_DOUBLE(min_height) \
	PROP_DOUBLE(max_height) \
	PROP_DOUBLE(rounding_radius) \
	PROP_DOUBLE(mgn_t) \
	PROP_DOUBLE(mgn_l) \
	PROP_DOUBLE(mgn_b) \
	PROP_DOUBLE(mgn_r) \
	PROP_DOUBLE(pad_t) \
	PROP_DOUBLE(pad_l) \
	PROP_DOUBLE(pad_b) \
	PROP_DOUBLE(pad_r) \
	PROP_INTEGER(font_size) \
	PROP_INTEGER(font_weight) \
	PROP_INTEGER(clip) \
	PROP_ENUM(alignment, B_ALIGN_, START) \
	PROP_ENUM(alignment, B_ALIGN_, CENTER) \
	PROP_ENUM(alignment, B_ALIGN_, END) \
	PROP_ENUM(content_alignment, B_ALIGN_, START) \
	PROP_ENUM(content_alignment, B_ALIGN_, CENTER) \
	PROP_ENUM(content_alignment, B_ALIGN_, END) \
	PROP_ENUM(flow_direction, B_, DOWN) \
	PROP_ENUM(flow_direction, B_, LTR) \
	PROP_ENUM(flow_direction, B_, UP) \
	PROP_ENUM(flow_direction, B_, RTL) \
	PROP_ENUM(font_style, B_FONT_, NORMAL) \
	PROP_ENUM(font_style, B_FONT_, OBLIQUE) \
	PROP_ENUM(font_style, B_FONT_, ITALIC) \
	PROP_ENUM(wrap_mode, B_WRAP_, ELLIPSIZE) \
	PROP_ENUM(wrap_mode, B_WRAP_, BREAK) \
	PROP_ENUM(wrap_mode, B_WRAP_, WORD) \
	PROP_ENUM(wrap_mode, B_WRAP_, ELLIPSIZE_START) \
	PROP_COLOR(background_color) \
	PROP_COLOR(warn_color) \
	PROP_COLOR(error_color) \
	PROP_COLOR(success_color) \
	PROP_COLOR(content_color) \
	PROP_STRING(font_name)

#define PROP_DOUBLE(_p) \
	&(IgelBuiltinFunction){ "R", "set " #_p " []", ' ' },

#define PROP_INTEGER(_p) \
	&(IgelBuiltinFunction){ "Z", "set " #_p " []", ' ' },

#define PROP_ENUM(_p, _ns, _v) \
	&(IgelBuiltinFunction){ "", "set " #_p " " #_v, ' ' },

#define PROP_COLOR(_p) \
	&(IgelBuiltinFunction){ "8888", "set " #_p " [] [] [] []", ' ' },

#define PROP_STRING(_p) \
	&(IgelBuiltinFunction){ "^", "set " #_p " []", ' ' },

static const IgelBuiltinFunction *funcs[] = {
	&(IgelBuiltinFunction){ "RR", "[] + []", 'R' },
	&(IgelBuiltinFunction){ "RR", "[] - []", 'R' },
	&(IgelBuiltinFunction){ "RR", "[] / []", 'R' },
	&(IgelBuiltinFunction){ "^", "box []", ' ' },
	&(IgelBuiltinFunction){ "", "endbox", ' ' },
	&(IgelBuiltinFunction){ "n", "put [] endbox", ' ' },
	&(IgelBuiltinFunction){ "^Z", "icon [] []", ' ' },
	&(IgelBuiltinFunction){ "^", "text []", ' ' },
	&(IgelBuiltinFunction){ "", "width", 'R' },
	&(IgelBuiltinFunction){ "", "height", 'R' },
	&(IgelBuiltinFunction){ "", "save", ' ' },
	&(IgelBuiltinFunction){ "", "restore", ' ' },
	&(IgelBuiltinFunction){ "R", "translate [] []", ' ' },
	&(IgelBuiltinFunction){ "R", "scale [] []", ' ' },
	&(IgelBuiltinFunction){ "R", "scale []", ' ' },
	&(IgelBuiltinFunction){ "R", "rotate []", ' ' },
	&(IgelBuiltinFunction){ "N", "use []", ' ' },
	&(IgelBuiltinFunction){ "", "paint", ' ' },
	&(IgelBuiltinFunction){ "", "line-width []", ' ' },
	&(IgelBuiltinFunction){ "", "stroke", ' ' },
	&(IgelBuiltinFunction){ "", "fill", ' ' },
	PROPS
};

#undef PROP_STRING
#undef PROP_COLOR
#undef PROP_ENUM
#undef PROP_INTEGER
#undef PROP_DOUBLE

#define N_FUNCS (sizeof(funcs) / sizeof(IgelBuiltinFunction *))

static void do_add(IgelVM *vm, size_t fp, void *data)
{
	double op1 = igvm_get_double(vm, fp + 8);
	double op2 = igvm_get_double(vm, fp + 16);
	igvm_put_double(vm, fp, op1 + op2);
}

static void do_subtract(IgelVM *vm, size_t fp, void *data)
{
	double op1 = igvm_get_double(vm, fp + 8);
	double op2 = igvm_get_double(vm, fp + 16);
	igvm_put_double(vm, fp, op1 - op2);
}

static void do_divide(IgelVM *vm, size_t fp, void *data)
{
	double op1 = igvm_get_double(vm, fp + 8);
	double op2 = igvm_get_double(vm, fp + 16);
	igvm_put_double(vm, fp, op1 / op2);
}

static void do_start_box(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	bBox *parent = script->box;
	const char *style = igvm_get_string(vm, fp + 8);
	if (script->box) {
		g_debug("Start box: %s", style);
		script->box = bCreateBox(style);
		bAddBox(parent, script->box);
	} else {
		g_warning("No box is being built, cannot start box");
	}
}

static void do_end_box(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->box && script->box->self) {
		g_debug("End box");
		script->box = script->box->self->owner;
	} else if (script->box) {
		g_warning("Cannot end top level box");
	} else {
		g_warning("No box is being built, cannot end box");
	}
}

static void do_put_box(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	uint16_t ref = igvm_get16(vm, fp + 8);
	if (script->box && ref < script->n_refs) {
		g_debug("Put box: %d", ref);
		script->refs[ref] = script->box;
		if (script->box->self)
			script->box = script->box->self->owner;
		else
			g_warning("Cannot end top level box");
	} else if (script->box) {
		g_warning("Put [%d] out of range", ref);
	} else {
		g_warning("No box is being built, cannot put box");
	}
}

static void do_icon(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	const char *name = igvm_get_string(vm, fp + 8);
	int32_t size = igvm_get32(vm, fp + 12);

	if (script->box) {
		g_debug("Icon: %s %d", name, size);
		if (!name)
			return;
		bAddContent(script->box, bCreateIcon(name, size));
	} else {
		g_warning("No box is being built, cannot put box");
	}
}

static void do_text(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	const char *text = igvm_get_string(vm, fp + 8);

	if (script->box) {
		g_debug("Text: %s", text);
		if (!text)
			return;
		bAddContent(script->box, bCreateText(text));
	} else {
		g_warning("No box is being built, cannot put box");
	}
}

static void do_get_width(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->renderer) {
		igvm_put_double(vm, fp, script->width);
	} else {
		igvm_put_double(vm, fp, 0.0);
		g_warning("Invalid get width call outside renderer");
	}
}

static void do_get_height(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->renderer) {
		igvm_put_double(vm, fp, script->height);
	} else {
		igvm_put_double(vm, fp, 0.0);
		g_warning("Invalid get height call outside renderer");
	}
}

static void do_save(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->renderer) {
		cairo_save(script->renderer);
		script->cairo_saves++;
	} else {
		g_warning("Invalid save call outside renderer");
	}
}

static void do_restore(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->renderer && script->cairo_saves > 0) {
		cairo_restore(script->renderer);
		script->cairo_saves--;
	} else if (script->renderer) {
		g_warning("[restore] called without preceding [save]");
	} else {
		g_warning("Invalid restore call outside renderer");
	}
}

static void do_translate(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	double x = igvm_get_double(vm, fp + 8);
	double y = igvm_get_double(vm, fp + 16);
	if (script->renderer)
		cairo_translate(script->renderer, x, y);
	else
		g_warning("Invalid translate call outside renderer");
}

static void do_scale_xy(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	double x = igvm_get_double(vm, fp + 8);
	double y = igvm_get_double(vm, fp + 16);
	if (script->renderer)
		cairo_scale(script->renderer, x, y);
	else
		g_warning("Invalid scale call outside renderer");
}

static void do_scale(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	double scale = igvm_get_double(vm, fp + 8);
	if (script->renderer)
		cairo_scale(script->renderer, scale, scale);
	else
		g_warning("Invalid scale call outside renderer");
}

static void do_rotate(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	double angle = igvm_get_double(vm, fp + 8);
	if (script->renderer)
		cairo_rotate(script->renderer, angle);
	else
		g_warning("Invalid rotate call outside renderer");
}

static void do_use_resource(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	uint32_t rid = igvm_get32(vm, fp + 8);
	if (script->renderer)
		cairo_set_source_surface(script->renderer,
					 script->resources[rid],
					 0, 0);
	else
		g_warning("Invalid resource call outside renderer");
}

static void do_paint(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->renderer)
		cairo_paint(script->renderer);
	else
		g_warning("Invalid paint call outside renderer");
}

static void do_set_line_width(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	double width = igvm_get_double(vm, fp + 8);
	if (script->renderer)
		cairo_set_line_width(script->renderer, width);
	else
		g_warning("Invalid set line width call outside renderer");
}

static void do_stroke(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->renderer)
		cairo_stroke(script->renderer);
	else
		g_warning("Invalid stroke call outside renderer");
}

static void do_fill(IgelVM *vm, size_t fp, void *data)
{
	bScript *script = data;
	if (script->renderer)
		cairo_fill(script->renderer);
	else
		g_warning("Invalid fill call outside renderer");
}

#define PROP_DOUBLE(_p) \
	static void set_ ## _p(IgelVM *vm, size_t fp, void *data) \
	{ \
		bScript *script = data; \
		double p = igvm_get_double(vm, fp + 8); \
		if (script->style) { \
			g_debug("Setting %s: %f", #_p, p); \
			script->style->items._p = p; \
			script->style->override_ ## _p = 1; \
		} else { \
			g_warning("Cannot set %s outside style definition", #_p); \
		} \
	}

#define PROP_INTEGER(_p) \
	static void set_ ## _p(IgelVM *vm, size_t fp, void *data) \
	{ \
		bScript *script = data; \
		int32_t p = igvm_get32(vm, fp + 8); \
		if (script->style) { \
			g_debug("Setting %s: %d", #_p, p); \
			script->style->items._p = p; \
			script->style->override_ ## _p = 1; \
		} else { \
			g_warning("Cannot set %s outside style definition", #_p); \
		} \
	}

#define PROP_ENUM(_p, _ns, _v) \
	static void set_ ## _p ## _ ## _v(IgelVM *vm, size_t fp, void *data) \
	{ \
		bScript *script = data; \
		if (script->style) { \
			g_debug("Setting %s: %s", #_p, #_v); \
			script->style->items._p = _ns ## _v; \
			script->style->override_ ## _p = 1; \
		} else { \
			g_warning("Cannot set %s outside style definition", #_p); \
		} \
	}

#define PROP_COLOR(_color) \
	static void set_ ## _color(IgelVM *vm, size_t fp, void *data) \
	{ \
		bScript *script = data; \
		uint8_t r = igvm_get8(vm, fp + 8); \
		uint8_t g = igvm_get8(vm, fp + 9); \
		uint8_t b = igvm_get8(vm, fp + 10); \
		uint8_t a = igvm_get8(vm, fp + 11); \
		if (script->style) { \
			g_debug("Setting %s: #%02X%02X%02X%02X", \
				#_color, r, g, b, a); \
			script->style->items._color[0] = r / 255.0; \
			script->style->items._color[1] = g / 255.0; \
			script->style->items._color[2] = b / 255.0; \
			script->style->items._color[3] = a / 255.0; \
			script->style->override_ ## _color = 1; \
		} else { \
			g_warning("Cannot set %s outside style definition", #_color); \
		} \
	}

#define PROP_STRING(_p) \
	static void set_ ## _p(IgelVM *vm, size_t fp, void *data) \
	{ \
		bScript *script = data; \
		const char *str = igvm_get_string(vm, fp + 8); \
		if (!str) \
			return; \
		if (script->style) { \
			g_debug("Setting %s: %s", #_p, str); \
			script->style->items._p = str; \
		} else { \
			g_warning("Cannot set %s outside style definition", #_p); \
		} \
	}

PROPS

#undef PROP_STRING
#undef PROP_COLOR
#undef PROP_ENUM
#undef PROP_INTEGER
#undef PROP_DOUBLE

#define PROP_DOUBLE(_p) set_ ## _p,
#define PROP_INTEGER(_p) set_ ## _p,
#define PROP_ENUM(_p, _ns, _v) set_ ## _p ## _ ## _v,
#define PROP_COLOR(_p) set_ ## _p,
#define PROP_STRING(_p) set_ ## _p,

static const IgelVMBuiltinFunction vm_funcs[N_FUNCS] = {
	do_add,
	do_subtract,
	do_divide,
	do_start_box,
	do_end_box,
	do_put_box,
	do_icon,
	do_text,
	do_get_width,
	do_get_height,
	do_save,
	do_restore,
	do_translate,
	do_scale_xy,
	do_scale,
	do_rotate,
	do_use_resource,
	do_paint,
	do_set_line_width,
	do_stroke,
	do_fill,
	PROPS
};

#undef PROP_STRING
#undef PROP_COLOR
#undef PROP_ENUM
#undef PROP_INTEGER
#undef PROP_DOUBLE

static uint32_t load_resource(bScript *script,
			      const char *name,
			      const char *file)
{
	cairo_surface_t *surface = cairo_image_surface_create_from_png(file);
	const char *error = NULL;
	switch (cairo_surface_status(surface)) {
	case CAIRO_STATUS_NO_MEMORY:
		error = "no memory";
		g_clear_pointer(&surface, cairo_surface_destroy);
		break;
	case CAIRO_STATUS_FILE_NOT_FOUND:
		error = "file not found";
		g_clear_pointer(&surface, cairo_surface_destroy);
		break;
	case CAIRO_STATUS_READ_ERROR:
		error = "read error";
		g_clear_pointer(&surface, cairo_surface_destroy);
		break;
	case CAIRO_STATUS_PNG_ERROR:
		error = "PNG error";
		g_clear_pointer(&surface, cairo_surface_destroy);
		break;
	default:
		break;
	}
	if (!surface) {
		g_warning("Failed to open %s as a PNG file: %s", file, error);
		return UINT32_MAX;
	}
	if (script->n_resources >= script->resources_size) {
		script->resources_size *= 2;
		script->resources = g_renew(cairo_surface_t*,
					    script->resources,
					    script->resources_size);
	}
	script->resources[script->n_resources] = surface;
	return script->n_resources++;
}

static FILE *fmemopen_printf(const char *fmt, ...)
{
	FILE *stream;
	va_list ap;
	int size;

	va_start(ap, fmt);
	size = vsnprintf(NULL, 0, fmt, ap);
	va_end(ap);

	if (size < 0)
		return NULL;

	stream = fmemopen(NULL, size + 1, "w+");

	va_start(ap, fmt);
	vfprintf(stream, fmt, ap);
	va_end(ap);

	rewind(stream);

	return stream;
}

static FILE *find_script(const char *name, void *userdata)
{
	bScript *script = userdata;
	const char * const *datadirs;
	FILE *stream;
	char *path;

	if (strncmp(name, "resource:", strlen("resource:")) == 0) {
		char **names;
		uint32_t rid;
		g_debug("Script %s is a resource", name);
		names = g_strsplit(name + strlen("resource:"), ":", 2);
		rid = load_resource(script, names[0], names[1]);
		if (rid == UINT32_MAX) {
			g_strfreev(names);
			return NULL;
		}
		stream = fmemopen_printf("[use %s] { [use [#\\N %u]] }",
					 names[0],
					 rid);
		g_strfreev(names);
		return stream;
	}

	path = g_strdup_printf("%s/bananui/scripts/%s",
				    g_get_user_data_dir(), name);
	g_debug("Looking for script at %s", path);
	stream = fopen(path, "r");

	if (!stream) {
		datadirs = g_get_system_data_dirs();
		for (; *datadirs; datadirs++) {
			g_free(path);
			path = g_strdup_printf("%s/bananui/scripts/%s",
					*datadirs, name);
			g_debug("Looking for script at %s", path);
			stream = fopen(path, "r");
			if (stream)
				break;
		}
	}

	if (!script->path)
		script->path = path;
	else
		g_free(path);
	return stream;
}

bScript *bLoadScript(const char *name)
{
	bScript *script = g_slice_new0(bScript);

	script->resources_size = 8;
	script->resources = g_new(cairo_surface_t*, script->resources_size);

	script->stream = find_script(name, script);
	if (!script->stream) {
		g_warning("Script %s not found", name);
		g_slice_free(bScript, script);
		return NULL;
	}

	script->prg = igc_compile(script->stream,
				  script->path,
				  find_script,
				  script,
				  funcs,
				  N_FUNCS);
	if (!script->prg) {
		g_free(script->path);
		fclose(script->stream);
		g_slice_free(bScript, script);
		return NULL;
	}

	return script;
}

int bExecuteScript(bScript *script, const char *entrypoint)
{
	uint16_t ep = igc_resolve_function(script->prg, entrypoint);
	if (ep == 0xffff) {
		g_debug("Entry point [%s] not found in %s",
			entrypoint, script->path);
		return -1;
	}
	g_debug("Executing [%s] in %s (marker %u)",
		entrypoint, script->path, ep);
	if (igvm_run_program(script->prg, ep, vm_funcs, N_FUNCS, script) < 0)
		return -1;
	return 0;
}

static void call_render(cairo_t *cr,
			double x, double y,
			double width, double height,
			void *rclass,
			void *userdata)
{
	bScript *script = userdata;
	uint16_t ep = GPOINTER_TO_UINT(rclass);
	script->renderer = cr;
	script->width = width;
	script->height = height;
	script->cairo_saves = 0;
	cairo_save(cr);
	cairo_translate(cr, x, y);
	if (igvm_run_program(script->prg, ep, vm_funcs, N_FUNCS, script) < 0)
		g_critical("Render script execution failed");
	if (script->cairo_saves != 0) {
		g_warning("[save] called without matching [restore]");
		while (script->cairo_saves) {
			cairo_restore(cr);
			script->cairo_saves--;
		}
	}
	cairo_restore(cr);
	script->renderer = NULL;
}

int bBuildStyle(bScript *script, bStyleSpec *style, const char *style_name)
{
	uint16_t render_ep;
	int ret;
	char *ep_name;
	if (script->style) {
		g_critical("bBuildStyle called while another style was being built");
		return -1;
	}
	script->style = style;
	memset(style, 0, sizeof(bStyleSpec));
	ep_name = g_strdup_printf("style %s", style_name);
	ret = bExecuteScript(script, ep_name);
	script->style = NULL;
	g_free(ep_name);
	ep_name = g_strdup_printf("render %s", style_name);
	render_ep = igc_resolve_function(script->prg, ep_name);
	if (render_ep != 0xffff) {
		style->items.render_func = call_render;
		style->items.render_class = GUINT_TO_POINTER(render_ep);
		style->items.render_userdata = script;
	}
	g_free(ep_name);
	return ret;
}

bBox *bBuildBox(bScript *script, const char *style_name,
		uint16_t n_refs, bBox **refs)
{
	bBox *box;
	char *ep_name;
	if (script->box) {
		g_critical("bBuildBox called while another box was being built");
		return NULL;
	}
	script->box = box = bCreateBox(style_name);
	script->refs = refs;
	script->n_refs = n_refs;
	ep_name = g_strdup_printf("build %s", style_name);
	if (bExecuteScript(script, ep_name) < 0) {
		g_debug("Failed to build '%s'", style_name);
		bDestroyBoxRecursive(box);
		script->refs = NULL;
		script->box = NULL;
		g_free(ep_name);
		return NULL;
	}
	if (script->box != box) {
		do {
			g_assert(script->box->self);
			script->box = script->box->self->owner;
		} while(script->box != box);
		g_warning("Missing [endbox] in [build %s]", style_name);
	}
	script->refs = NULL;
	script->box = NULL;
	g_free(ep_name);
	return box;
}
