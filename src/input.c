/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "bananui-input"

#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <bananui/widget.h>
#include <bananui/input.h>
#include <bananui/card.h>
#include "text-input-client-protocol.h"

static int inp_handle_update(void *param, void *data);

static void inp_report_state(bInputWidget *inp)
{
	if(!inp->im) return;
	bReportInputMethodState(inp->im, inp->mode, inp->flags,
			inp->text, inp->cursor, inp->cursor);
}

static void inp_delete_text(bInputWidget *inp, int new_cursor, unsigned int after)
{
	int i, textlen = strlen(inp->text);
	for(i = new_cursor; i < textlen; i++){
		inp->text[i] = inp->text[i + after + inp->cursor - new_cursor];
	}
	inp->cursor = new_cursor;
	inp->curx = -1; /* invalidate horizontal offset */
	inp_report_state(inp);
}

static void inp_update(bInputWidget *inp)
{
	int text_size = strlen(inp->text), preedit_size = 0;
	char *displayed_text;

	if(inp->preedit){
		preedit_size = strlen(inp->preedit);
	}
	displayed_text = g_try_malloc((preedit_size + text_size + 1) * sizeof(char));
	if(displayed_text){
		int i = text_size + 1;
		/* Insert preedit string into displayed text at cursor position */
		while(i > 0){
			i--;
			if(i >= inp->cursor){
				displayed_text[i + preedit_size] = inp->text[i];
			}
			else {
				displayed_text[i] = inp->text[i];
			}
		}
		if(inp->preedit){
			memcpy(displayed_text + inp->cursor,
				inp->preedit, preedit_size);
		}
		bSetText(inp->cont, displayed_text);
		inp->cont->cursor = inp->cursor + preedit_size;
		/* The application may prevent the redraw to save resources. */
		if(bTriggerEvent(inp->change, displayed_text)){
			if(inp->wnd) bRequestFrame(inp->wnd);
		}
		g_free(displayed_text);
	}
}

static void inp_update_cursor(bInputWidget *inp)
{
	inp_report_state(inp);
	if(inp->preedit){
		inp_update(inp);
	}
	else {
		inp->cont->cursor = inp->cursor;
		bRequestFrame(inp->wnd);
	}
}

static void inp_ime_enable(bInputWidget *inp)
{
	if(inp->active) return;
	g_debug("Enable IME");
	inp->active = inp->cont->cursor_visible = 1;
	bRequestFrame(inp->wnd);
	bEnableInputMethod(inp->im);
}

static void inp_ime_disable(bInputWidget *inp)
{
	if(!inp->active) return;
	g_debug("Disable IME");
	bDisableInputMethod(inp->im);
	g_clear_pointer(&inp->preedit, g_free);
	inp->active = inp->cont->cursor_visible = 0;
	inp_update(inp);
}

static int inp_handle_leave(void *param, void *data)
{
	bInputWidget *inp = data;
	g_assert(inp->wnd);
	inp_ime_disable(inp);
	return 1;
}

static int inp_handle_enter(void *param, void *data)
{
	bInputWidget *inp = data;
	g_assert(inp->wnd);
	inp_ime_enable(inp);
	return 1;
}

static int inp_navigate_prev(char *text, int cursor)
{
	char *prev = g_utf8_find_prev_char(text, text + cursor);
	return prev ? (prev - text) : -1;
}

static int inp_navigate_next(char *text, int cursor)
{
	return text[cursor] ? g_utf8_find_next_char(text + cursor, NULL) - text : -1;
}


/*
 * XXX: navigation behaves incorrectly when a preedit string is set.
 * does this need to be fixed or will the preedit string always be unset
 * when navigating?
 */
static int inp_navigate_up(bInputWidget *inp)
{
	int line, x;
	if(inp->cursor == 0) return -1;
	if(bGetLineXAtOffset(inp->cont, inp->cursor, &line, &x) < 0) return -1;
	if(line == 0) return 0;
	if(inp->curx < 0) inp->curx = x;
	return bGetOffsetAtLineX(inp->cont, line-1, inp->curx);
}

static int inp_navigate_down(bInputWidget *inp)
{
	int line, x;
	if((unsigned int)inp->cursor >= strlen(inp->text)) return -1;
	if(bGetLineXAtOffset(inp->cont, inp->cursor, &line, &x) < 0) return -1;
	if(line == bGetLastLine(inp->cont)) return strlen(inp->text);
	if(inp->curx < 0) inp->curx = x;
	return bGetOffsetAtLineX(inp->cont, line+1, inp->curx);
}

static void inp_insert_text(bInputWidget *inp, const char *new_text)
{
	int textlen = strlen(inp->text), i = textlen + 1;
	int insertlen = strlen(new_text);
	g_debug("- insert: %s\n", new_text);
	if (textlen + insertlen >= inp->bufsize) {
		char *newtext;
		while (textlen + insertlen >= inp->bufsize)
			inp->bufsize = inp->bufsize*2;
		newtext = g_try_realloc(inp->text,
					inp->bufsize * sizeof(char));
		if (newtext)
			inp->text = newtext;
		else
			return;
	}
	/* Insert insert string into text */
	while (i > inp->cursor) {
		i--;
		inp->text[i + insertlen] = inp->text[i];
	}
	memcpy(inp->text + inp->cursor, new_text, insertlen);
	inp->cursor += insertlen;
	inp->curx = -1; /* invalidate horizontal offset */
	inp_report_state(inp);
}

static int inp_handle_keydown(void *param, void *data)
{
	bInputWidget *inp = data;
	bKeyEvent *ev = param;
	int multiline = inp->flags &
		(B_INPUT_FLAG_MULTILINE | B_INPUT_FLAG_MULTILINE_WRAP);
	/* inp_navigate_* returns -1 when there is nowhere to navigate */
	if(ev->sym == XKB_KEY_BackSpace){
		int new_cursor = inp_navigate_prev(inp->text, inp->cursor);
		g_debug("Delete -> %d", new_cursor);
		if (new_cursor < 0)
			return 1;
		inp_delete_text(inp, new_cursor, 0);
		inp_update(inp);
		return 0;
	}
	else if(ev->sym == XKB_KEY_Left){
		int new_cursor = inp_navigate_prev(inp->text, inp->cursor);
		g_debug("Cursor left -> %d", new_cursor);
		if(new_cursor < 0){
			return 1;
		}
		inp->cursor = new_cursor;
		inp->curx = -1; /* invalidate horizontal offset */
		inp_update_cursor(inp);
		return 0;
	}
	else if(ev->sym == XKB_KEY_Right){
		int new_cursor = inp_navigate_next(inp->text, inp->cursor);
		g_debug("Cursor right -> %d", new_cursor);
		if(new_cursor < 0){
			return 1;
		}
		inp->cursor = new_cursor;
		inp->curx = -1; /* invalidate horizontal offset */
		inp_update_cursor(inp);
		return 0;
	}
	else if(multiline && ev->sym == XKB_KEY_Up){
		int new_cursor = inp_navigate_up(inp);
		g_debug("Cursor up -> %d", new_cursor);
		if(new_cursor < 0){
			return 1;
		}
		inp->cursor = new_cursor;
		inp_update_cursor(inp);
		return 0;
	}
	else if(multiline && ev->sym == XKB_KEY_Down){
		int new_cursor = inp_navigate_down(inp);
		g_debug("Cursor down -> %d", new_cursor);
		if(new_cursor < 0){
			return 1;
		}
		inp->cursor = new_cursor;
		inp_update_cursor(inp);
		return 0;
	}
	else if(multiline && ev->sym == XKB_KEY_Return){
		inp_insert_text(inp, "\n");
		inp_update(inp);
		return 0;
	}
	return 1;
}

static int inp_handle_keyup(void *param, void *data)
{
	return 1;
}

static int inp_handle_update(void *param, void *data)
{
	bInputWidget *inp = data;
	bInputMethodEvent *ev = param;
	g_assert(inp->wnd);

	g_debug("IME update");
	if (ev->delete_before || ev->delete_after) {
		int new_cursor = inp->cursor - ev->delete_before;
		if (new_cursor < 0)
			new_cursor = 0;
		inp_delete_text(inp, new_cursor, ev->delete_after);
	}
	if (ev->commit_string)
		inp_insert_text(inp, ev->commit_string);
	if (ev->preedit) {
		g_debug("- preedit: %s\n", ev->preedit);
		inp->preedit = g_strdup(ev->preedit);
	}
	inp_update(inp);
	return 1;
}

static int inp_focus_handler(void *param, void *data)
{
	bFocusEvent *ev = param;
	bInputWidget *inp = data;
	if(inp->wnd) return 1;
	inp->wnd = ev->wnd;
	inp->im = bGetInputMethod(inp->wnd);
	inp_report_state(inp);
	inp_ime_enable(inp);
	bBindStyle(inp->box, inp->focused_style_name);
	bRegisterEventHandler(&inp->im->update, inp_handle_update, inp);
	bRegisterEventHandler(&inp->im->leave, inp_handle_leave, inp);
	bRegisterEventHandler(&inp->im->enter, inp_handle_enter, inp);
	bRegisterEventHandler(&inp->wnd->keydown, inp_handle_keydown, inp);
	bRegisterEventHandler(&inp->wnd->keyup, inp_handle_keyup, inp);
	return 1;
}

static int inp_unfocus_handler(void *param, void *data)
{
	bInputWidget *inp = data;
	if(!inp->wnd) return 1;
	bUnregisterEventHandler(&inp->im->update, inp_handle_update, inp);
	bUnregisterEventHandler(&inp->im->leave, inp_handle_leave, inp);
	bUnregisterEventHandler(&inp->im->enter, inp_handle_enter, inp);
	bUnregisterEventHandler(&inp->wnd->keydown, inp_handle_keydown, inp);
	bUnregisterEventHandler(&inp->wnd->keyup, inp_handle_keyup, inp);
	bBindStyle(inp->box, inp->style_name);
	inp_ime_disable(inp);
	g_clear_pointer(&inp->im, bDestroyInputMethod);
	inp->wnd = NULL;
	return 1;
}

static int inp_destroy_handler(void *param, void *data)
{
	bInputWidget *inp = data;
	bClearEvent(&inp->change);
	g_free(inp->focused_style_name);
	g_free(inp->style_name);
	g_free(inp->text);
	g_free(inp);
	return 1;
}

bInputWidget *bCreateInputWidget(const char *style_name,
				 bInputMode mode,
				 bInputFlags flags)
{
	bInputWidget *inp;
	bBox *content = NULL;
	g_assert(mode != B_INPUT_MODE_PASSWORD || ("Not implemented" && 0));

	inp = g_new0(bInputWidget, 1);
	inp->flags = flags;
	inp->mode = mode;
	inp->curx = -1;
	inp->bufsize = 512;
	inp->box = bBuildBox(bDefaultTheme(NULL), style_name, 1, &content);
	if (!inp->box) {
		g_free(inp);
		return NULL;
	}
	if (!content) {
		g_critical("%s content location not specified in theme",
			   style_name);
		bDestroyBoxRecursive(inp->box);
		g_free(inp);
		return NULL;
	}
	inp->box->focusable = 1;
	inp->style_name = g_strdup(style_name);
	inp->focused_style_name = g_strdup_printf("%s :focus", style_name);

	bAddContent(content, inp->cont = bCreateText(""));

	inp->cont->cursor_hscroll = (flags & B_INPUT_FLAG_MULTILINE_WRAP) ? 0 : 1;

	bRegisterEventHandler(&inp->box->focusin, inp_focus_handler, inp);
	bRegisterEventHandler(&inp->box->focusout, inp_unfocus_handler, inp);
	bRegisterEventHandler(&inp->box->destroy, inp_destroy_handler, inp);

	inp->text = g_malloc(inp->bufsize * sizeof(char));
	if(!inp->text){
		bDestroyBoxRecursive(inp->box);
		g_free(inp);
		return NULL;
	}
	inp->text[0] = L'\0';

	return inp;
}

char *bGetInputText(bInputWidget *inp)
{
	return g_strdup(inp->cont->text);
}

int bSetInputText(bInputWidget *inp, const char *text)
{
	int newsize = strlen(text);
	if (!g_utf8_validate(text, -1, NULL)) {
		g_warning("Invalid UTF-8 string passed to text input");
		return -1;
	}
	if (newsize >= inp->bufsize) {
		char *newtext;
		while (newsize >= inp->bufsize)
			inp->bufsize *= 2;
		newtext = g_try_realloc(inp->text, inp->bufsize * sizeof(char));
		if (!newtext)
			return -1;
		inp->text = newtext;
	}
	strcpy(inp->text, text);
	if (inp->cursor > newsize) {
		inp->cursor = newsize;
		inp->curx = -1; /* invalidate horizontal offset */
	}
	inp_report_state(inp);
	inp_update(inp);
	return 0;
}
