/*
 * This file is part of bananui.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <bananui/menu.h>

bMenu *bCreateMenuView(bWindow *wnd)
{
	bMenu *menu;
	menu = g_malloc0(sizeof(bMenu));
	menu->wnd = wnd;
	return menu;
}

void bSetMenuTitle(bMenu *m, const char *title)
{
	if(m->current) bSetText(m->current->header_text, title);
}

static void destroyDisappearing(bMenu *m)
{
	if(m->disappearing){
		bDestroyCard(m->disappearing->card);
		if (m->disappearing->header)
			bDestroyCard(m->disappearing->header);
		bDestroySoftkeyPanel(m->disappearing->sk);
		g_free(m->disappearing);
		m->disappearing = NULL;
	}
}

int bPopMenu(bMenu *m, bMenuTransition transition)
{
	destroyDisappearing(m);
	m->disappearing = m->current;
	m->current = m->disappearing->prev;
	bUnfocusCard(m->wnd, m->disappearing->card);
	if (m->current && transition != 0){
		bFocusCard(m->wnd, m->current->card);
		if (transition & B_MENU_TRANSITION_SLIDE)
			bTranslateCard(m->disappearing->card, 240, 0, 0.25);
		if (transition & B_MENU_TRANSITION_FADE)
			bFadeCard(m->disappearing->card, 0, 0.25);
		if (m->disappearing->header)
			bAnimateCardWith(m->disappearing->header,
					m->disappearing->card);
		bAnimateCardWith(m->disappearing->sk->card,
				m->disappearing->card);
		bRequestFrame(m->wnd);
		return 1;
	}
	destroyDisappearing(m);
	return 0;
}

struct menu_click {
	bMenuEntry *e;
	bMenu *m;
};

static int handle_destroy(void *param, void *data)
{
	struct menu_click *mc = data;
	if(mc->e->button_handler) mc->e->button_handler(mc->m, NULL, mc->e->arg);
	g_free(mc);
	return 1;
}

static int handle_click(void *param, void *data)
{
	struct menu_click *mc = data;
	if(!mc->e->click_handler) return 1;
	return mc->e->click_handler(mc->m, mc->e->arg);
}

int _bOpenMenuPrivate(bMenu *m, bMenuDef *def, bMenuTransition transition)
{
	unsigned int i;
	bBox *headerbox = NULL;
	bMenuView *mv;

	mv = g_new0(bMenuView, 1);
	mv->card = bCreateCard("menu");
	mv->header = bBuildCard(bDefaultTheme(NULL),
				"menu header",
				1,
				&headerbox);
	if (headerbox)
		bAddContent(headerbox,
			    mv->header_text = bCreateText(def->title ?: ""));
	mv->sk = bCreateSoftkeyPanel();
	bSetSoftkeyText(mv->sk, "", "SELECT", "");

	for (i = 0; i < def->num_entries; i++) {
		struct menu_click *click;
		bButtonWidget *btn;
		bMenuEntry *ent = &def->entries[i];

		click = g_new0(struct menu_click, 1);
		click->e = ent;
		click->m = m;

		btn = bCreateButtonWidget(ent->name, ent->style, ent->func);
		if (ent->icon) {
			bSetIcon(btn->icon, ent->icon,
					ent->iconsize ? ent->iconsize : 24);
		}
		
		bRegisterEventHandler(&btn->box->click, handle_click, click);
		bRegisterEventHandler(&btn->box->destroy, handle_destroy, click);
		bAddBox(mv->card->box, btn->box);
		if (ent->button_handler)
			ent->button_handler(m, btn, ent->arg);
	}

	if (m->current)
		bUnfocusCard(m->wnd, m->current->card);
	mv->prev = m->prev = m->current;
	m->current = mv;
	def->init(m);
	if (transition != 0) {
		if (transition & B_MENU_TRANSITION_SLIDE) {
			bTranslateCard(mv->card, 240, 0, 0);
			bTranslateCard(mv->card, 0, 0, 0.25);
		}
		if (transition & B_MENU_TRANSITION_FADE) {
			bFadeCard(mv->card, 0, 0);
			bFadeCard(mv->card, 1, 0.25);
		}
		if (mv->header)
			bAnimateCardWith(mv->header, mv->card);
		bAnimateCardWith(mv->sk->card, mv->card);
	}
	bFocusCard(m->wnd, mv->card);
	bRequestFrame(m->wnd);
	return 0;
}

void bRenderMenu(bMenu *m)
{
	if(!m->current) return;
	/*
	 * Render the cards first so that they don't show through fading
	 * soft keys or headers
	 */
	if(m->prev)
		bRenderCard(m->wnd, m->prev->card);
	bRenderCard(m->wnd, m->current->card);
	if(m->disappearing)
		bRenderCard(m->wnd, m->disappearing->card);

	if(m->prev){
		if (m->prev->header)
			bRenderCard(m->wnd, m->prev->header);
		bRenderCard(m->wnd, m->prev->sk->card);
		if(bAnimationComplete(m->current->card)){
			m->prev = NULL;
		}
	}
	if (m->current->header)
		bRenderCard(m->wnd, m->current->header);
	bRenderCard(m->wnd, m->current->sk->card);
	if(m->disappearing){
		if (m->disappearing->header)
			bRenderCard(m->wnd, m->disappearing->header);
		bRenderCard(m->wnd, m->disappearing->sk->card);
		if(bAnimationComplete(m->disappearing->card)){
			destroyDisappearing(m);
		}
	}
}
